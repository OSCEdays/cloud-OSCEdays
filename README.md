Custom theming for the OSCEdays owncloud instance at cloud.oscedays.org
Currently custom filetypes in themes/OSCEdays/core/img/filetypes are not being applied..?

in order to use this theme, the following line must be added to the array section of owncloud/config/config.php:
 'theme' => 'OSCEdays'