


<!DOCTYPE html>
<html lang="en" class="">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    
    
    <title>core/helper.php at master · owncloud/core · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png">
    <meta property="fb:app_id" content="1401488693436528">

      <meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="owncloud/core" name="twitter:title" /><meta content="The ownCloud core" name="twitter:description" /><meta content="https://avatars2.githubusercontent.com/u/1645051?v=3&amp;s=400" name="twitter:image:src" />
      <meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="https://avatars2.githubusercontent.com/u/1645051?v=3&amp;s=400" property="og:image" /><meta content="owncloud/core" property="og:title" /><meta content="https://github.com/owncloud/core" property="og:url" /><meta content="The ownCloud core" property="og:description" />
      <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">
    <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">
    <link rel="assets" href="https://assets-cdn.github.com/">
    
    <meta name="pjax-timeout" content="1000">
    

    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="selected-link" value="repo_source" data-pjax-transient>
      <meta name="google-analytics" content="UA-3769691-2">

    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" /><meta content="github" name="octolytics-app-id" /><meta content="55B20CED:4D8A:FE17E58:5530C1D1" name="octolytics-dimension-request_id" />
    
    <meta content="Rails, view, blob#show" name="analytics-event" />
    <meta class="js-ga-set" name="dimension1" content="Logged Out">
    <meta class="js-ga-set" name="dimension2" content="Header v3">
    <meta name="is-dotcom" content="true">
    <meta name="hostname" content="github.com">
    <meta name="user-login" content="">

    
    <link rel="icon" type="image/x-icon" href="https://assets-cdn.github.com/favicon.ico">


    <meta content="authenticity_token" name="csrf-param" />
<meta content="msfPdSMlyRl2XVv9m7cN4C9Mz8K5e6nAGFz2WJhzR3064Vc6WLVL9NJ2yjV1SXxEo37Be0vwjbcjkQi4NhsM6g==" name="csrf-token" />

    <link href="https://assets-cdn.github.com/assets/github-99d0b872ee54fd3afae4675a7592394fa9d65696f8ad7a751b79704bc999f40a.css" media="all" rel="stylesheet" />
    <link href="https://assets-cdn.github.com/assets/github2-e6c2d39e3c50ad99c491e3d19fe91e598eb13a1b75ec03b1831e8d710cde2f04.css" media="all" rel="stylesheet" />
    
    


    <meta http-equiv="x-pjax-version" content="4a4eec5c32a35d72f5cb696ad3d59cda">

      
  <meta name="description" content="The ownCloud core">
  <meta name="go-import" content="github.com/owncloud/core git https://github.com/owncloud/core.git">

  <meta content="1645051" name="octolytics-dimension-user_id" /><meta content="owncloud" name="octolytics-dimension-user_login" /><meta content="5550552" name="octolytics-dimension-repository_id" /><meta content="owncloud/core" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="5550552" name="octolytics-dimension-repository_network_root_id" /><meta content="owncloud/core" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/owncloud/core/commits/master.atom" rel="alternate" title="Recent Commits to core:master" type="application/atom+xml">

  </head>


  <body class="logged_out  env-production linux vis-public page-blob">
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>
    <div class="wrapper">
      
      
      


        
        <div class="header header-logged-out" role="banner">
  <div class="container clearfix">

    <a class="header-logo-wordmark" href="https://github.com/" data-ga-click="(Logged out) Header, go to homepage, icon:logo-wordmark">
      <span class="mega-octicon octicon-logo-github"></span>
    </a>

    <div class="header-actions" role="navigation">
        <a class="btn btn-primary" href="/join" data-ga-click="(Logged out) Header, clicked Sign up, text:sign-up">Sign up</a>
      <a class="btn" href="/login?return_to=%2Fowncloud%2Fcore%2Fblob%2Fmaster%2Flib%2Fprivate%2Fhelper.php" data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in">Sign in</a>
    </div>

    <div class="site-search repo-scope js-site-search" role="search">
      <form accept-charset="UTF-8" action="/owncloud/core/search" class="js-site-search-form" data-global-search-url="/search" data-repo-search-url="/owncloud/core/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
  <input type="text"
    class="js-site-search-field is-clearable"
    data-hotkey="s"
    name="q"
    placeholder="Search"
    data-global-scope-placeholder="Search GitHub"
    data-repo-scope-placeholder="Search"
    tabindex="1"
    autocapitalize="off">
  <div class="scope-badge">This repository</div>
</form>
    </div>

      <ul class="header-nav left" role="navigation">
          <li class="header-nav-item">
            <a class="header-nav-link" href="/explore" data-ga-click="(Logged out) Header, go to explore, text:explore">Explore</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="/features" data-ga-click="(Logged out) Header, go to features, text:features">Features</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="https://enterprise.github.com/" data-ga-click="(Logged out) Header, go to enterprise, text:enterprise">Enterprise</a>
          </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="/blog" data-ga-click="(Logged out) Header, go to blog, text:blog">Blog</a>
          </li>
      </ul>

  </div>
</div>



      <div id="start-of-content" class="accessibility-aid"></div>
          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    <div id="js-flash-container">
      
    </div>
    <div class="pagehead repohead instapaper_ignore readability-menu">
      <div class="container">
        
<ul class="pagehead-actions">

  <li>
      <a href="/login?return_to=%2Fowncloud%2Fcore"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to watch a repository" rel="nofollow">
    <span class="octicon octicon-eye"></span>
    Watch
  </a>
  <a class="social-count" href="/owncloud/core/watchers">
    408
  </a>

  </li>

  <li>
      <a href="/login?return_to=%2Fowncloud%2Fcore"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <span class="octicon octicon-star"></span>
    Star
  </a>

    <a class="social-count js-social-count" href="/owncloud/core/stargazers">
      3,239
    </a>

  </li>

    <li>
      <a href="/login?return_to=%2Fowncloud%2Fcore"
        class="btn btn-sm btn-with-count tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <span class="octicon octicon-repo-forked"></span>
        Fork
      </a>
      <a href="/owncloud/core/network" class="social-count">
        1,036
      </a>
    </li>
</ul>

        <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
          <span class="mega-octicon octicon-repo"></span>
          <span class="author"><a href="/owncloud" class="url fn" itemprop="url" rel="author"><span itemprop="title">owncloud</span></a></span><!--
       --><span class="path-divider">/</span><!--
       --><strong><a href="/owncloud/core" class="js-current-repository" data-pjax="#js-repo-pjax-container">core</a></strong>

          <span class="page-context-loader">
            <img alt="" height="16" src="https://assets-cdn.github.com/assets/spinners/octocat-spinner-32-e513294efa576953719e4e2de888dd9cf929b7d62ed8d05f25e731d02452ab6c.gif" width="16" />
          </span>

        </h1>
      </div><!-- /.container -->
    </div><!-- /.repohead -->

    <div class="container">
      <div class="repository-with-sidebar repo-container new-discussion-timeline  ">
        <div class="repository-sidebar clearfix">
            
<nav class="sunken-menu repo-nav js-repo-nav js-sidenav-container-pjax js-octicon-loaders"
     role="navigation"
     data-pjax="#js-repo-pjax-container"
     data-issue-count-url="/owncloud/core/issues/counts">
  <ul class="sunken-menu-group">
    <li class="tooltipped tooltipped-w" aria-label="Code">
      <a href="/owncloud/core" aria-label="Code" class="selected js-selected-navigation-item sunken-menu-item" data-hotkey="g c" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /owncloud/core">
        <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
        <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/assets/spinners/octocat-spinner-32-e513294efa576953719e4e2de888dd9cf929b7d62ed8d05f25e731d02452ab6c.gif" width="16" />
</a>    </li>

      <li class="tooltipped tooltipped-w" aria-label="Issues">
        <a href="/owncloud/core/issues" aria-label="Issues" class="js-selected-navigation-item sunken-menu-item" data-hotkey="g i" data-selected-links="repo_issues repo_labels repo_milestones /owncloud/core/issues">
          <span class="octicon octicon-issue-opened"></span> <span class="full-word">Issues</span>
          <span class="js-issue-replace-counter"></span>
          <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/assets/spinners/octocat-spinner-32-e513294efa576953719e4e2de888dd9cf929b7d62ed8d05f25e731d02452ab6c.gif" width="16" />
</a>      </li>

    <li class="tooltipped tooltipped-w" aria-label="Pull requests">
      <a href="/owncloud/core/pulls" aria-label="Pull requests" class="js-selected-navigation-item sunken-menu-item" data-hotkey="g p" data-selected-links="repo_pulls /owncloud/core/pulls">
          <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull requests</span>
          <span class="js-pull-replace-counter"></span>
          <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/assets/spinners/octocat-spinner-32-e513294efa576953719e4e2de888dd9cf929b7d62ed8d05f25e731d02452ab6c.gif" width="16" />
</a>    </li>

      <li class="tooltipped tooltipped-w" aria-label="Wiki">
        <a href="/owncloud/core/wiki" aria-label="Wiki" class="js-selected-navigation-item sunken-menu-item" data-hotkey="g w" data-selected-links="repo_wiki /owncloud/core/wiki">
          <span class="octicon octicon-book"></span> <span class="full-word">Wiki</span>
          <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/assets/spinners/octocat-spinner-32-e513294efa576953719e4e2de888dd9cf929b7d62ed8d05f25e731d02452ab6c.gif" width="16" />
</a>      </li>
  </ul>
  <div class="sunken-menu-separator"></div>
  <ul class="sunken-menu-group">

    <li class="tooltipped tooltipped-w" aria-label="Pulse">
      <a href="/owncloud/core/pulse" aria-label="Pulse" class="js-selected-navigation-item sunken-menu-item" data-selected-links="pulse /owncloud/core/pulse">
        <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
        <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/assets/spinners/octocat-spinner-32-e513294efa576953719e4e2de888dd9cf929b7d62ed8d05f25e731d02452ab6c.gif" width="16" />
</a>    </li>

    <li class="tooltipped tooltipped-w" aria-label="Graphs">
      <a href="/owncloud/core/graphs" aria-label="Graphs" class="js-selected-navigation-item sunken-menu-item" data-selected-links="repo_graphs repo_contributors /owncloud/core/graphs">
        <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
        <img alt="" class="mini-loader" height="16" src="https://assets-cdn.github.com/assets/spinners/octocat-spinner-32-e513294efa576953719e4e2de888dd9cf929b7d62ed8d05f25e731d02452ab6c.gif" width="16" />
</a>    </li>
  </ul>


</nav>

              <div class="only-with-full-nav">
                  
<div class="clone-url open"
  data-protocol-type="http"
  data-url="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone">
  <h3><span class="text-emphasized">HTTPS</span> clone URL</h3>
  <div class="input-group js-zeroclipboard-container">
    <input type="text" class="input-mini input-monospace js-url-field js-zeroclipboard-target"
           value="https://github.com/owncloud/core.git" readonly="readonly">
    <span class="input-group-button">
      <button aria-label="Copy to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" data-copy-hint="Copy to clipboard" type="button"><span class="octicon octicon-clippy"></span></button>
    </span>
  </div>
</div>

  
<div class="clone-url "
  data-protocol-type="subversion"
  data-url="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone">
  <h3><span class="text-emphasized">Subversion</span> checkout URL</h3>
  <div class="input-group js-zeroclipboard-container">
    <input type="text" class="input-mini input-monospace js-url-field js-zeroclipboard-target"
           value="https://github.com/owncloud/core" readonly="readonly">
    <span class="input-group-button">
      <button aria-label="Copy to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" data-copy-hint="Copy to clipboard" type="button"><span class="octicon octicon-clippy"></span></button>
    </span>
  </div>
</div>



<p class="clone-options">You can clone with
  <a href="#" class="js-clone-selector" data-protocol="http">HTTPS</a> or <a href="#" class="js-clone-selector" data-protocol="subversion">Subversion</a>.
  <a href="https://help.github.com/articles/which-remote-url-should-i-use" class="help tooltipped tooltipped-n" aria-label="Get help on which URL is right for you.">
    <span class="octicon octicon-question"></span>
  </a>
</p>




                <a href="/owncloud/core/archive/master.zip"
                   class="btn btn-sm sidebar-button"
                   aria-label="Download the contents of owncloud/core as a zip file"
                   title="Download the contents of owncloud/core as a zip file"
                   rel="nofollow">
                  <span class="octicon octicon-cloud-download"></span>
                  Download ZIP
                </a>
              </div>
        </div><!-- /.repository-sidebar -->

        <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>
          

<a href="/owncloud/core/blob/f32d97750c33942db53a56d1deceacb2ed3e779b/lib/private/helper.php" class="hidden js-permalink-shortcut" data-hotkey="y">Permalink</a>

<!-- blob contrib key: blob_contributors:v21:c1961af8ce0f2ff93ce7d7864ca02e68 -->

<div class="file-navigation js-zeroclipboard-container">
  
<div class="select-menu js-menu-container js-select-menu left">
  <span class="btn btn-sm select-menu-button js-menu-target css-truncate" data-hotkey="w"
    data-master-branch="master"
    data-ref="master"
    title="master"
    role="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <span class="octicon octicon-git-branch"></span>
    <i>branch:</i>
    <span class="js-select-button css-truncate-target">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-x js-menu-close" role="button" aria-label="Close"></span>
      </div>

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" data-filter-placeholder="Filter branches/tags" class="js-select-menu-tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" data-filter-placeholder="Find a tag…" class="js-select-menu-tab">Tags</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/3rdparty-replace/lib/private/helper.php"
               data-name="3rdparty-replace"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="3rdparty-replace">
                3rdparty-replace
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/4965-bringbackfileuploadspinner/lib/private/helper.php"
               data-name="4965-bringbackfileuploadspinner"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="4965-bringbackfileuploadspinner">
                4965-bringbackfileuploadspinner
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ac_tests/lib/private/helper.php"
               data-name="ac_tests"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ac_tests">
                ac_tests
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/add-dropdown-filesize-unit/lib/private/helper.php"
               data-name="add-dropdown-filesize-unit"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="add-dropdown-filesize-unit">
                add-dropdown-filesize-unit
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/add-limit-parameter-to-getSharewith/lib/private/helper.php"
               data-name="add-limit-parameter-to-getSharewith"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="add-limit-parameter-to-getSharewith">
                add-limit-parameter-to-getSharewith
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/add-login-name/lib/private/helper.php"
               data-name="add-login-name"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="add-login-name">
                add-login-name
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ajaxfilelist-ie8/lib/private/helper.php"
               data-name="ajaxfilelist-ie8"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ajaxfilelist-ie8">
                ajaxfilelist-ie8
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ajaxfilelist-ie8-alternative/lib/private/helper.php"
               data-name="ajaxfilelist-ie8-alternative"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ajaxfilelist-ie8-alternative">
                ajaxfilelist-ie8-alternative
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/app-cleanup/lib/private/helper.php"
               data-name="app-cleanup"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="app-cleanup">
                app-cleanup
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/app-upgrade-order/lib/private/helper.php"
               data-name="app-upgrade-order"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="app-upgrade-order">
                app-upgrade-order
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/appexception-for-ci/lib/private/helper.php"
               data-name="appexception-for-ci"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="appexception-for-ci">
                appexception-for-ci
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/auth-refactor/lib/private/helper.php"
               data-name="auth-refactor"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="auth-refactor">
                auth-refactor
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/autotest-docker-postgres/lib/private/helper.php"
               data-name="autotest-docker-postgres"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="autotest-docker-postgres">
                autotest-docker-postgres
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/autotest-external-swift/lib/private/helper.php"
               data-name="autotest-external-swift"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="autotest-external-swift">
                autotest-external-swift
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/avatar_loadfromdata/lib/private/helper.php"
               data-name="avatar_loadfromdata"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="avatar_loadfromdata">
                avatar_loadfromdata
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/aws-composer/lib/private/helper.php"
               data-name="aws-composer"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="aws-composer">
                aws-composer
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/backport-14293-public-download-activities/lib/private/helper.php"
               data-name="backport-14293-public-download-activities"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="backport-14293-public-download-activities">
                backport-14293-public-download-activities
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/backport-15362-purge-app-cache-cli-enable/lib/private/helper.php"
               data-name="backport-15362-purge-app-cache-cli-enable"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="backport-15362-purge-app-cache-cli-enable">
                backport-15362-purge-app-cache-cli-enable
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/backport-15566/lib/private/helper.php"
               data-name="backport-15566"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="backport-15566">
                backport-15566
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/backport-api-authentication/lib/private/helper.php"
               data-name="backport-api-authentication"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="backport-api-authentication">
                backport-api-authentication
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/blank_page_with_error_message/lib/private/helper.php"
               data-name="blank_page_with_error_message"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="blank_page_with_error_message">
                blank_page_with_error_message
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/block-legacy-clients/lib/private/helper.php"
               data-name="block-legacy-clients"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="block-legacy-clients">
                block-legacy-clients
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/bulk-share-insert/lib/private/helper.php"
               data-name="bulk-share-insert"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="bulk-share-insert">
                bulk-share-insert
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/cache-move-single-query/lib/private/helper.php"
               data-name="cache-move-single-query"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="cache-move-single-query">
                cache-move-single-query
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/cache-storage-status/lib/private/helper.php"
               data-name="cache-storage-status"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="cache-storage-status">
                cache-storage-status
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/cache_userfolder/lib/private/helper.php"
               data-name="cache_userfolder"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="cache_userfolder">
                cache_userfolder
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/clean-up-ocs-code/lib/private/helper.php"
               data-name="clean-up-ocs-code"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="clean-up-ocs-code">
                clean-up-ocs-code
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/clockwork/lib/private/helper.php"
               data-name="clockwork"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="clockwork">
                clockwork
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/closecursor-cases/lib/private/helper.php"
               data-name="closecursor-cases"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="closecursor-cases">
                closecursor-cases
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/consistency-check-command/lib/private/helper.php"
               data-name="consistency-check-command"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="consistency-check-command">
                consistency-check-command
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/create-share-target-reuse/lib/private/helper.php"
               data-name="create-share-target-reuse"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="create-share-target-reuse">
                create-share-target-reuse
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/dav-put-storage-2/lib/private/helper.php"
               data-name="dav-put-storage-2"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="dav-put-storage-2">
                dav-put-storage-2
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/dav-put-storage-ci/lib/private/helper.php"
               data-name="dav-put-storage-ci"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="dav-put-storage-ci">
                dav-put-storage-ci
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/dav-stream/lib/private/helper.php"
               data-name="dav-stream"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="dav-stream">
                dav-stream
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/dav-tags-experiment/lib/private/helper.php"
               data-name="dav-tags-experiment"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="dav-tags-experiment">
                dav-tags-experiment
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/dbprefix-test-master/lib/private/helper.php"
               data-name="dbprefix-test-master"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="dbprefix-test-master">
                dbprefix-test-master
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/decrypt-tool/lib/private/helper.php"
               data-name="decrypt-tool"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="decrypt-tool">
                decrypt-tool
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/default-timezone/lib/private/helper.php"
               data-name="default-timezone"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="default-timezone">
                default-timezone
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/defaultandroid/lib/private/helper.php"
               data-name="defaultandroid"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="defaultandroid">
                defaultandroid
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/delayed-resource-loader/lib/private/helper.php"
               data-name="delayed-resource-loader"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="delayed-resource-loader">
                delayed-resource-loader
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/delete-storage/lib/private/helper.php"
               data-name="delete-storage"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="delete-storage">
                delete-storage
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/delete-undo-master/lib/private/helper.php"
               data-name="delete-undo-master"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="delete-undo-master">
                delete-undo-master
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/deprecate-controller/lib/private/helper.php"
               data-name="deprecate-controller"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="deprecate-controller">
                deprecate-controller
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/deprecate-potential-dangerous-methods/lib/private/helper.php"
               data-name="deprecate-potential-dangerous-methods"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="deprecate-potential-dangerous-methods">
                deprecate-potential-dangerous-methods
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/direct-download-smb/lib/private/helper.php"
               data-name="direct-download-smb"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="direct-download-smb">
                direct-download-smb
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/docker-oracle-nc-sleep/lib/private/helper.php"
               data-name="docker-oracle-nc-sleep"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="docker-oracle-nc-sleep">
                docker-oracle-nc-sleep
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/doctrine_in_setup/lib/private/helper.php"
               data-name="doctrine_in_setup"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="doctrine_in_setup">
                doctrine_in_setup
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/dont_hide_exceptions/lib/private/helper.php"
               data-name="dont_hide_exceptions"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="dont_hide_exceptions">
                dont_hide_exceptions
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/dont_send_errors_as_html_if_request_looks_like_a_json_request/lib/private/helper.php"
               data-name="dont_send_errors_as_html_if_request_looks_like_a_json_request"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="dont_send_errors_as_html_if_request_looks_like_a_json_request">
                dont_send_errors_as_html_if_request_looks_like_a_json_request
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/dont_send_errors_as_html_if_request_looks_like_a_json_request_master/lib/private/helper.php"
               data-name="dont_send_errors_as_html_if_request_looks_like_a_json_request_master"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="dont_send_errors_as_html_if_request_looks_like_a_json_request_master">
                dont_send_errors_as_html_if_request_looks_like_a_json_request_master
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/dotci/lib/private/helper.php"
               data-name="dotci"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="dotci">
                dotci
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/dropdowns/lib/private/helper.php"
               data-name="dropdowns"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="dropdowns">
                dropdowns
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/drophere/lib/private/helper.php"
               data-name="drophere"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="drophere">
                drophere
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/enc2_debug/lib/private/helper.php"
               data-name="enc2_debug"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="enc2_debug">
                enc2_debug
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/enc2_performance_improvement/lib/private/helper.php"
               data-name="enc2_performance_improvement"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="enc2_performance_improvement">
                enc2_performance_improvement
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/enc_fix_rename/lib/private/helper.php"
               data-name="enc_fix_rename"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="enc_fix_rename">
                enc_fix_rename
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/enc_fix_upload_shared_folder/lib/private/helper.php"
               data-name="enc_fix_upload_shared_folder"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="enc_fix_upload_shared_folder">
                enc_fix_upload_shared_folder
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/enc_isolated_fixes/lib/private/helper.php"
               data-name="enc_isolated_fixes"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="enc_isolated_fixes">
                enc_isolated_fixes
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/enc_ng/lib/private/helper.php"
               data-name="enc_ng"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="enc_ng">
                enc_ng
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/enc-pregfix-trashbin/lib/private/helper.php"
               data-name="enc-pregfix-trashbin"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="enc-pregfix-trashbin">
                enc-pregfix-trashbin
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/enc_reset_private_key_password/lib/private/helper.php"
               data-name="enc_reset_private_key_password"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="enc_reset_private_key_password">
                enc_reset_private_key_password
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/enc_stream_copy/lib/private/helper.php"
               data-name="enc_stream_copy"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="enc_stream_copy">
                enc_stream_copy
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/encryption_no_auto_logout/lib/private/helper.php"
               data-name="encryption_no_auto_logout"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="encryption_no_auto_logout">
                encryption_no_auto_logout
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/encryption-reuse/lib/private/helper.php"
               data-name="encryption-reuse"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="encryption-reuse">
                encryption-reuse
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/enforce-password-strength/lib/private/helper.php"
               data-name="enforce-password-strength"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="enforce-password-strength">
                enforce-password-strength
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/errorhandler-object/lib/private/helper.php"
               data-name="errorhandler-object"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="errorhandler-object">
                errorhandler-object
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/etag-endpoint/lib/private/helper.php"
               data-name="etag-endpoint"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="etag-endpoint">
                etag-endpoint
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/expireusergroupshareviaapi/lib/private/helper.php"
               data-name="expireusergroupshareviaapi"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="expireusergroupshareviaapi">
                expireusergroupshareviaapi
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/expose-cache-interface-on-storage/lib/private/helper.php"
               data-name="expose-cache-interface-on-storage"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="expose-cache-interface-on-storage">
                expose-cache-interface-on-storage
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ext-ocp/lib/private/helper.php"
               data-name="ext-ocp"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ext-ocp">
                ext-ocp
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ext-swiftcaching/lib/private/helper.php"
               data-name="ext-swiftcaching"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ext-swiftcaching">
                ext-swiftcaching
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/extended-attributes/lib/private/helper.php"
               data-name="extended-attributes"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="extended-attributes">
                extended-attributes
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/external-backend-ondemand/lib/private/helper.php"
               data-name="external-backend-ondemand"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="external-backend-ondemand">
                external-backend-ondemand
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/extstorage-configtestfix/lib/private/helper.php"
               data-name="extstorage-configtestfix"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="extstorage-configtestfix">
                extstorage-configtestfix
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/extstorage-dropbox-hasupdated/lib/private/helper.php"
               data-name="extstorage-dropbox-hasupdated"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="extstorage-dropbox-hasupdated">
                extstorage-dropbox-hasupdated
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/extstorage-smb-debug/lib/private/helper.php"
               data-name="extstorage-smb-debug"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="extstorage-smb-debug">
                extstorage-smb-debug
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/extstorage-webdavstreaming/lib/private/helper.php"
               data-name="extstorage-webdavstreaming"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="extstorage-webdavstreaming">
                extstorage-webdavstreaming
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/file-chunking-next-generation/lib/private/helper.php"
               data-name="file-chunking-next-generation"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="file-chunking-next-generation">
                file-chunking-next-generation
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fileapi/lib/private/helper.php"
               data-name="fileapi"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fileapi">
                fileapi
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/files-ajaxload-infscroll-selectionwip/lib/private/helper.php"
               data-name="files-ajaxload-infscroll-selectionwip"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="files-ajaxload-infscroll-selectionwip">
                files-ajaxload-infscroll-selectionwip
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/files-createfileentry/lib/private/helper.php"
               data-name="files-createfileentry"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="files-createfileentry">
                files-createfileentry
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/files-external-ajax/lib/private/helper.php"
               data-name="files-external-ajax"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="files-external-ajax">
                files-external-ajax
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/files-external-duplicatemimetypebug/lib/private/helper.php"
               data-name="files-external-duplicatemimetypebug"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="files-external-duplicatemimetypebug">
                files-external-duplicatemimetypebug
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/files_external_scan_all_files/lib/private/helper.php"
               data-name="files_external_scan_all_files"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="files_external_scan_all_files">
                files_external_scan_all_files
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/files-fileinfoformatter/lib/private/helper.php"
               data-name="files-fileinfoformatter"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="files-fileinfoformatter">
                files-fileinfoformatter
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/files-handlebarsforrows/lib/private/helper.php"
               data-name="files-handlebarsforrows"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="files-handlebarsforrows">
                files-handlebarsforrows
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/filesystem-factory/lib/private/helper.php"
               data-name="filesystem-factory"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="filesystem-factory">
                filesystem-factory
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix_3597/lib/private/helper.php"
               data-name="fix_3597"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix_3597">
                fix_3597
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-7871/lib/private/helper.php"
               data-name="fix-7871"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-7871">
                fix-7871
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-10825/lib/private/helper.php"
               data-name="fix-10825"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-10825">
                fix-10825
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-11448-with-block-ui/lib/private/helper.php"
               data-name="fix-11448-with-block-ui"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-11448-with-block-ui">
                fix-11448-with-block-ui
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-12190-2-stable7/lib/private/helper.php"
               data-name="fix-12190-2-stable7"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-12190-2-stable7">
                fix-12190-2-stable7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-15631-master/lib/private/helper.php"
               data-name="fix-15631-master"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-15631-master">
                fix-15631-master
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-duplicate-date-functions/lib/private/helper.php"
               data-name="fix-duplicate-date-functions"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-duplicate-date-functions">
                fix-duplicate-date-functions
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-group-list-and-count-primary-group-only/lib/private/helper.php"
               data-name="fix-group-list-and-count-primary-group-only"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-group-list-and-count-primary-group-only">
                fix-group-list-and-count-primary-group-only
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix_group_search/lib/private/helper.php"
               data-name="fix_group_search"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix_group_search">
                fix_group_search
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-htaccess-frank/lib/private/helper.php"
               data-name="fix-htaccess-frank"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-htaccess-frank">
                fix-htaccess-frank
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-input-padding/lib/private/helper.php"
               data-name="fix-input-padding"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-input-padding">
                fix-input-padding
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix_preview_versions_revert_issue/lib/private/helper.php"
               data-name="fix_preview_versions_revert_issue"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix_preview_versions_revert_issue">
                fix_preview_versions_revert_issue
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-smb-oc-admin-stable7/lib/private/helper.php"
               data-name="fix-smb-oc-admin-stable7"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-smb-oc-admin-stable7">
                fix-smb-oc-admin-stable7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fix-smb-oc-admin-stable8/lib/private/helper.php"
               data-name="fix-smb-oc-admin-stable8"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fix-smb-oc-admin-stable8">
                fix-smb-oc-admin-stable8
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/flysystem/lib/private/helper.php"
               data-name="flysystem"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="flysystem">
                flysystem
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/fs-statcachestoragewrapper/lib/private/helper.php"
               data-name="fs-statcachestoragewrapper"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="fs-statcachestoragewrapper">
                fs-statcachestoragewrapper
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/git-push---set-upstream-origin-shared-etag-propagate-jenkins/lib/private/helper.php"
               data-name="git-push---set-upstream-origin-shared-etag-propagate-jenkins"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="git-push---set-upstream-origin-shared-etag-propagate-jenkins">
                git-push---set-upstream-origin-shared-etag-propagate-jenkins
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/hidden-files/lib/private/helper.php"
               data-name="hidden-files"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="hidden-files">
                hidden-files
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/incrementuser/lib/private/helper.php"
               data-name="incrementuser"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="incrementuser">
                incrementuser
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/inputs/lib/private/helper.php"
               data-name="inputs"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="inputs">
                inputs
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/internal_email_template/lib/private/helper.php"
               data-name="internal_email_template"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="internal_email_template">
                internal_email_template
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/intialize-ssl-enforcing-before-starting-session/lib/private/helper.php"
               data-name="intialize-ssl-enforcing-before-starting-session"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="intialize-ssl-enforcing-before-starting-session">
                intialize-ssl-enforcing-before-starting-session
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/issue/11951-activity-sharing-email/lib/private/helper.php"
               data-name="issue/11951-activity-sharing-email"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="issue/11951-activity-sharing-email">
                issue/11951-activity-sharing-email
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/issue/15589/lib/private/helper.php"
               data-name="issue/15589"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="issue/15589">
                issue/15589
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/issue/15634-empty-txt-previews/lib/private/helper.php"
               data-name="issue/15634-empty-txt-previews"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="issue/15634-empty-txt-previews">
                issue/15634-empty-txt-previews
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/issue/15667-occ-encryption/lib/private/helper.php"
               data-name="issue/15667-occ-encryption"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="issue/15667-occ-encryption">
                issue/15667-occ-encryption
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/jailed-webserver-access-master/lib/private/helper.php"
               data-name="jailed-webserver-access-master"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="jailed-webserver-access-master">
                jailed-webserver-access-master
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/jcf-fix-cache-update/lib/private/helper.php"
               data-name="jcf-fix-cache-update"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="jcf-fix-cache-update">
                jcf-fix-cache-update
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/keep-webui-in-sync/lib/private/helper.php"
               data-name="keep-webui-in-sync"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="keep-webui-in-sync">
                keep-webui-in-sync
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/lazyimageload/lib/private/helper.php"
               data-name="lazyimageload"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="lazyimageload">
                lazyimageload
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/lazyloadgroups/lib/private/helper.php"
               data-name="lazyloadgroups"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="lazyloadgroups">
                lazyloadgroups
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/lazyloadgroups-select2/lib/private/helper.php"
               data-name="lazyloadgroups-select2"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="lazyloadgroups-select2">
                lazyloadgroups-select2
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ldap-batch-read-attrs/lib/private/helper.php"
               data-name="ldap-batch-read-attrs"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ldap-batch-read-attrs">
                ldap-batch-read-attrs
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ldap-changes/lib/private/helper.php"
               data-name="ldap-changes"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ldap-changes">
                ldap-changes
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ldap_group_count/lib/private/helper.php"
               data-name="ldap_group_count"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ldap_group_count">
                ldap_group_count
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ldap-group-optimizations/lib/private/helper.php"
               data-name="ldap-group-optimizations"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ldap-group-optimizations">
                ldap-group-optimizations
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ldap-log/lib/private/helper.php"
               data-name="ldap-log"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ldap-log">
                ldap-log
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ldap-log-stable6/lib/private/helper.php"
               data-name="ldap-log-stable6"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ldap-log-stable6">
                ldap-log-stable6
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ldap-persuade-bind/lib/private/helper.php"
               data-name="ldap-persuade-bind"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ldap-persuade-bind">
                ldap-persuade-bind
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ldap-style/lib/private/helper.php"
               data-name="ldap-style"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ldap-style">
                ldap-style
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ldap_wizard_determine_groups/lib/private/helper.php"
               data-name="ldap_wizard_determine_groups"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ldap_wizard_determine_groups">
                ldap_wizard_determine_groups
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/log-external-deletes/lib/private/helper.php"
               data-name="log-external-deletes"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="log-external-deletes">
                log-external-deletes
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open selected"
               href="/owncloud/core/blob/master/lib/private/helper.php"
               data-name="master"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="master">
                master
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/master_restore/lib/private/helper.php"
               data-name="master_restore"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="master_restore">
                master_restore
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/memcache_assets/lib/private/helper.php"
               data-name="memcache_assets"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="memcache_assets">
                memcache_assets
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/migrate-certificate-stuff/lib/private/helper.php"
               data-name="migrate-certificate-stuff"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="migrate-certificate-stuff">
                migrate-certificate-stuff
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/migrate-incognito-mode-to-iusersession/lib/private/helper.php"
               data-name="migrate-incognito-mode-to-iusersession"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="migrate-incognito-mode-to-iusersession">
                migrate-incognito-mode-to-iusersession
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/mtime-propagate-increase/lib/private/helper.php"
               data-name="mtime-propagate-increase"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="mtime-propagate-increase">
                mtime-propagate-increase
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/mysql-collation/lib/private/helper.php"
               data-name="mysql-collation"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="mysql-collation">
                mysql-collation
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/newbranch/lib/private/helper.php"
               data-name="newbranch"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="newbranch">
                newbranch
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/newfileinvalidcharsfix/lib/private/helper.php"
               data-name="newfileinvalidcharsfix"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="newfileinvalidcharsfix">
                newfileinvalidcharsfix
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/no-eval/lib/private/helper.php"
               data-name="no-eval"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="no-eval">
                no-eval
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/nodeapi-searchapiuseview/lib/private/helper.php"
               data-name="nodeapi-searchapiuseview"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="nodeapi-searchapiuseview">
                nodeapi-searchapiuseview
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/oc-core-migration-test/lib/private/helper.php"
               data-name="oc-core-migration-test"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="oc-core-migration-test">
                oc-core-migration-test
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/onserviceavailable/lib/private/helper.php"
               data-name="onserviceavailable"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="onserviceavailable">
                onserviceavailable
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/optimize-appconfig/lib/private/helper.php"
               data-name="optimize-appconfig"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="optimize-appconfig">
                optimize-appconfig
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/pl-completed/lib/private/helper.php"
               data-name="pl-completed"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="pl-completed">
                pl-completed
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/poc-doctrine-migrations/lib/private/helper.php"
               data-name="poc-doctrine-migrations"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="poc-doctrine-migrations">
                poc-doctrine-migrations
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/preventcreatingpartfiles/lib/private/helper.php"
               data-name="preventcreatingpartfiles"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="preventcreatingpartfiles">
                preventcreatingpartfiles
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/preview-exifthumbnail/lib/private/helper.php"
               data-name="preview-exifthumbnail"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="preview-exifthumbnail">
                preview-exifthumbnail
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/public-calendar-api/lib/private/helper.php"
               data-name="public-calendar-api"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="public-calendar-api">
                public-calendar-api
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/quota-betterinsufficientstoragedetection/lib/private/helper.php"
               data-name="quota-betterinsufficientstoragedetection"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="quota-betterinsufficientstoragedetection">
                quota-betterinsufficientstoragedetection
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/quota-insufficientstorageforfilesfix/lib/private/helper.php"
               data-name="quota-insufficientstorageforfilesfix"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="quota-insufficientstorageforfilesfix">
                quota-insufficientstorageforfilesfix
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/quota-reuse-size/lib/private/helper.php"
               data-name="quota-reuse-size"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="quota-reuse-size">
                quota-reuse-size
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/quota-softquota/lib/private/helper.php"
               data-name="quota-softquota"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="quota-softquota">
                quota-softquota
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/refactorLoginFor2FA/lib/private/helper.php"
               data-name="refactorLoginFor2FA"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="refactorLoginFor2FA">
                refactorLoginFor2FA
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/refresh-mime-cache/lib/private/helper.php"
               data-name="refresh-mime-cache"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="refresh-mime-cache">
                refresh-mime-cache
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/remove-getetag-properties/lib/private/helper.php"
               data-name="remove-getetag-properties"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="remove-getetag-properties">
                remove-getetag-properties
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/remove-ocp-app-register/lib/private/helper.php"
               data-name="remove-ocp-app-register"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="remove-ocp-app-register">
                remove-ocp-app-register
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/renamemountpointconflicts/lib/private/helper.php"
               data-name="renamemountpointconflicts"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="renamemountpointconflicts">
                renamemountpointconflicts
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/repair-bogusleadingslash/lib/private/helper.php"
               data-name="repair-bogusleadingslash"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="repair-bogusleadingslash">
                repair-bogusleadingslash
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/repair-legacystorageid-setupfs/lib/private/helper.php"
               data-name="repair-legacystorageid-setupfs"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="repair-legacystorageid-setupfs">
                repair-legacystorageid-setupfs
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/replace-phpass/lib/private/helper.php"
               data-name="replace-phpass"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="replace-phpass">
                replace-phpass
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/response-setContentLengthHeader-stable7/lib/private/helper.php"
               data-name="response-setContentLengthHeader-stable7"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="response-setContentLengthHeader-stable7">
                response-setContentLengthHeader-stable7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/response-setContentLengthHeader-stable8/lib/private/helper.php"
               data-name="response-setContentLengthHeader-stable8"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="response-setContentLengthHeader-stable8">
                response-setContentLengthHeader-stable8
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/revert-10647-implementing_momentjs/lib/private/helper.php"
               data-name="revert-10647-implementing_momentjs"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="revert-10647-implementing_momentjs">
                revert-10647-implementing_momentjs
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/revert-15201-webdav-ng-bugfix/lib/private/helper.php"
               data-name="revert-15201-webdav-ng-bugfix"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="revert-15201-webdav-ng-bugfix">
                revert-15201-webdav-ng-bugfix
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/revert-15666-revert-15572-stable8-shares-properlyformatmountpoint/lib/private/helper.php"
               data-name="revert-15666-revert-15572-stable8-shares-properlyformatmountpoint"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="revert-15666-revert-15572-stable8-shares-properlyformatmountpoint">
                revert-15666-revert-15572-stable8-shares-properlyformatmountpoint
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/revert-flock-master/lib/private/helper.php"
               data-name="revert-flock-master"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="revert-flock-master">
                revert-flock-master
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/s2s_notification_queue_alternative/lib/private/helper.php"
               data-name="s2s_notification_queue_alternative"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="s2s_notification_queue_alternative">
                s2s_notification_queue_alternative
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/schema_testing/lib/private/helper.php"
               data-name="schema_testing"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="schema_testing">
                schema_testing
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/searchcommon-overlappingstorages/lib/private/helper.php"
               data-name="searchcommon-overlappingstorages"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="searchcommon-overlappingstorages">
                searchcommon-overlappingstorages
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/server2server-sharing-ng-jenkins/lib/private/helper.php"
               data-name="server2server-sharing-ng-jenkins"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="server2server-sharing-ng-jenkins">
                server2server-sharing-ng-jenkins
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/sftp_exceptions/lib/private/helper.php"
               data-name="sftp_exceptions"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="sftp_exceptions">
                sftp_exceptions
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/share-dropdownspinner/lib/private/helper.php"
               data-name="share-dropdownspinner"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="share-dropdownspinner">
                share-dropdownspinner
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/share-saveexpirationdatewithpassword/lib/private/helper.php"
               data-name="share-saveexpirationdatewithpassword"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="share-saveexpirationdatewithpassword">
                share-saveexpirationdatewithpassword
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/shared-cache-cache/lib/private/helper.php"
               data-name="shared-cache-cache"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="shared-cache-cache">
                shared-cache-cache
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/shared-etag-propagate/lib/private/helper.php"
               data-name="shared-etag-propagate"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="shared-etag-propagate">
                shared-etag-propagate
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/shared-etag-propagate-ci/lib/private/helper.php"
               data-name="shared-etag-propagate-ci"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="shared-etag-propagate-ci">
                shared-etag-propagate-ci
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/shared-storage-use-wrappers/lib/private/helper.php"
               data-name="shared-storage-use-wrappers"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="shared-storage-use-wrappers">
                shared-storage-use-wrappers
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/shared-storage-use-wrappers-jenkins/lib/private/helper.php"
               data-name="shared-storage-use-wrappers-jenkins"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="shared-storage-use-wrappers-jenkins">
                shared-storage-use-wrappers-jenkins
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/sharing_cleanup_step2/lib/private/helper.php"
               data-name="sharing_cleanup_step2"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="sharing_cleanup_step2">
                sharing_cleanup_step2
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/sharing_mail_notification/lib/private/helper.php"
               data-name="sharing_mail_notification"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="sharing_mail_notification">
                sharing_mail_notification
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/sharing-searchbymimefix/lib/private/helper.php"
               data-name="sharing-searchbymimefix"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="sharing-searchbymimefix">
                sharing-searchbymimefix
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/sharing_set_default_expire_date/lib/private/helper.php"
               data-name="sharing_set_default_expire_date"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="sharing_set_default_expire_date">
                sharing_set_default_expire_date
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/sharing_update_delete_behaviour/lib/private/helper.php"
               data-name="sharing_update_delete_behaviour"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="sharing_update_delete_behaviour">
                sharing_update_delete_behaviour
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/show-enc-in-files/lib/private/helper.php"
               data-name="show-enc-in-files"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="show-enc-in-files">
                show-enc-in-files
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/smashbox-debug-temp/lib/private/helper.php"
               data-name="smashbox-debug-temp"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="smashbox-debug-temp">
                smashbox-debug-temp
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/splunkintegration/lib/private/helper.php"
               data-name="splunkintegration"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="splunkintegration">
                splunkintegration
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/spread_some_closeCursor/lib/private/helper.php"
               data-name="spread_some_closeCursor"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="spread_some_closeCursor">
                spread_some_closeCursor
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/sqlite-busy-timeout/lib/private/helper.php"
               data-name="sqlite-busy-timeout"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="sqlite-busy-timeout">
                sqlite-busy-timeout
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stabe5/lib/private/helper.php"
               data-name="stabe5"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stabe5">
                stabe5
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable/lib/private/helper.php"
               data-name="stable"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable">
                stable
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable4/lib/private/helper.php"
               data-name="stable4"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable4">
                stable4
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable5/lib/private/helper.php"
               data-name="stable5"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable5">
                stable5
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable5-disableversionsinpublicmode/lib/private/helper.php"
               data-name="stable5-disableversionsinpublicmode"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable5-disableversionsinpublicmode">
                stable5-disableversionsinpublicmode
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable5-homecachequotafix/lib/private/helper.php"
               data-name="stable5-homecachequotafix"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable5-homecachequotafix">
                stable5-homecachequotafix
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable6/lib/private/helper.php"
               data-name="stable6"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable6">
                stable6
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable6-dav-disablerangerequestwhennotsupported/lib/private/helper.php"
               data-name="stable6-dav-disablerangerequestwhennotsupported"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable6-dav-disablerangerequestwhennotsupported">
                stable6-dav-disablerangerequestwhennotsupported
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable6-filesunittests/lib/private/helper.php"
               data-name="stable6-filesunittests"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable6-filesunittests">
                stable6-filesunittests
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable7/lib/private/helper.php"
               data-name="stable7"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable7">
                stable7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable7-closecursorforcalculatefoldersize/lib/private/helper.php"
               data-name="stable7-closecursorforcalculatefoldersize"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable7-closecursorforcalculatefoldersize">
                stable7-closecursorforcalculatefoldersize
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable7-dav-disablerangerequestwhennotsupported/lib/private/helper.php"
               data-name="stable7-dav-disablerangerequestwhennotsupported"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable7-dav-disablerangerequestwhennotsupported">
                stable7-dav-disablerangerequestwhennotsupported
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable7-fix-ext-owncloud/lib/private/helper.php"
               data-name="stable7-fix-ext-owncloud"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable7-fix-ext-owncloud">
                stable7-fix-ext-owncloud
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable7-log-external-deletes/lib/private/helper.php"
               data-name="stable7-log-external-deletes"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable7-log-external-deletes">
                stable7-log-external-deletes
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable7-preferencesclosecursor/lib/private/helper.php"
               data-name="stable7-preferencesclosecursor"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable7-preferencesclosecursor">
                stable7-preferencesclosecursor
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable8/lib/private/helper.php"
               data-name="stable8"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable8">
                stable8
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable8-backport-15353/lib/private/helper.php"
               data-name="stable8-backport-15353"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable8-backport-15353">
                stable8-backport-15353
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable8-davclient-catchmoreexceptions/lib/private/helper.php"
               data-name="stable8-davclient-catchmoreexceptions"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable8-davclient-catchmoreexceptions">
                stable8-davclient-catchmoreexceptions
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable8-fix-ext-owncloud/lib/private/helper.php"
               data-name="stable8-fix-ext-owncloud"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable8-fix-ext-owncloud">
                stable8-fix-ext-owncloud
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable8l10nbport/lib/private/helper.php"
               data-name="stable8l10nbport"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable8l10nbport">
                stable8l10nbport
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable8-share-donotreturntrashedfiles/lib/private/helper.php"
               data-name="stable8-share-donotreturntrashedfiles"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable8-share-donotreturntrashedfiles">
                stable8-share-donotreturntrashedfiles
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/stable45/lib/private/helper.php"
               data-name="stable45"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="stable45">
                stable45
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/storage-wrapper-check/lib/private/helper.php"
               data-name="storage-wrapper-check"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="storage-wrapper-check">
                storage-wrapper-check
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/storagewarning-readonly/lib/private/helper.php"
               data-name="storagewarning-readonly"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="storagewarning-readonly">
                storagewarning-readonly
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/store-users-for-webdav-auth/lib/private/helper.php"
               data-name="store-users-for-webdav-auth"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="store-users-for-webdav-auth">
                store-users-for-webdav-auth
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/test-db-column-type-change/lib/private/helper.php"
               data-name="test-db-column-type-change"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="test-db-column-type-change">
                test-db-column-type-change
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/test-ext/lib/private/helper.php"
               data-name="test-ext"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="test-ext">
                test-ext
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/test_ldap_2nddispname/lib/private/helper.php"
               data-name="test_ldap_2nddispname"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="test_ldap_2nddispname">
                test_ldap_2nddispname
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/test_ldap_2nddispname-stable7/lib/private/helper.php"
               data-name="test_ldap_2nddispname-stable7"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="test_ldap_2nddispname-stable7">
                test_ldap_2nddispname-stable7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/test_lookup/lib/private/helper.php"
               data-name="test_lookup"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="test_lookup">
                test_lookup
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/tests-reportinpage/lib/private/helper.php"
               data-name="tests-reportinpage"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="tests-reportinpage">
                tests-reportinpage
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/thumbnail_cache_improvements_sharing/lib/private/helper.php"
               data-name="thumbnail_cache_improvements_sharing"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="thumbnail_cache_improvements_sharing">
                thumbnail_cache_improvements_sharing
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/uid-not-loginname/lib/private/helper.php"
               data-name="uid-not-loginname"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="uid-not-loginname">
                uid-not-loginname
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/unique_user_ids/lib/private/helper.php"
               data-name="unique_user_ids"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="unique_user_ids">
                unique_user_ids
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/unit-tests-on-encryption-storage-wrapper/lib/private/helper.php"
               data-name="unit-tests-on-encryption-storage-wrapper"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="unit-tests-on-encryption-storage-wrapper">
                unit-tests-on-encryption-storage-wrapper
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/unshare_if_no_user_is_logged_in/lib/private/helper.php"
               data-name="unshare_if_no_user_is_logged_in"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="unshare_if_no_user_is_logged_in">
                unshare_if_no_user_is_logged_in
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/update-license-headers-wBJ4HiWhIPzaN5FgAZnuIF7MGDiyAxRu/lib/private/helper.php"
               data-name="update-license-headers-wBJ4HiWhIPzaN5FgAZnuIF7MGDiyAxRu"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="update-license-headers-wBJ4HiWhIPzaN5FgAZnuIF7MGDiyAxRu">
                update-license-headers-wBJ4HiWhIPzaN5FgAZnuIF7MGDiyAxRu
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/update-mtime-on-file_put_contents/lib/private/helper.php"
               data-name="update-mtime-on-file_put_contents"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="update-mtime-on-file_put_contents">
                update-mtime-on-file_put_contents
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/update-sabre2.1-chunkingfix/lib/private/helper.php"
               data-name="update-sabre2.1-chunkingfix"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="update-sabre2.1-chunkingfix">
                update-sabre2.1-chunkingfix
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/use-iterator-directory/lib/private/helper.php"
               data-name="use-iterator-directory"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="use-iterator-directory">
                use-iterator-directory
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/use-jsminplus/lib/private/helper.php"
               data-name="use-jsminplus"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="use-jsminplus">
                use-jsminplus
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/use-phpini-wrapper/lib/private/helper.php"
               data-name="use-phpini-wrapper"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="use-phpini-wrapper">
                use-phpini-wrapper
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/use-pretty-urls/lib/private/helper.php"
               data-name="use-pretty-urls"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="use-pretty-urls">
                use-pretty-urls
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/use_relative_path_to_create_images/lib/private/helper.php"
               data-name="use_relative_path_to_create_images"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="use_relative_path_to_create_images">
                use_relative_path_to_create_images
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/ux-s2s-ldap/lib/private/helper.php"
               data-name="ux-s2s-ldap"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="ux-s2s-ldap">
                ux-s2s-ldap
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/versions-cleanup/lib/private/helper.php"
               data-name="versions-cleanup"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="versions-cleanup">
                versions-cleanup
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/versions-rollbackkeepfileid/lib/private/helper.php"
               data-name="versions-rollbackkeepfileid"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="versions-rollbackkeepfileid">
                versions-rollbackkeepfileid
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/web-test-plugin-for-mirall/lib/private/helper.php"
               data-name="web-test-plugin-for-mirall"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="web-test-plugin-for-mirall">
                web-test-plugin-for-mirall
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/webdav-fileinfo/lib/private/helper.php"
               data-name="webdav-fileinfo"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="webdav-fileinfo">
                webdav-fileinfo
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/owncloud/core/blob/webdav_rfc5323_search/lib/private/helper.php"
               data-name="webdav_rfc5323_search"
               data-skip-pjax="true"
               rel="nofollow">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <span class="select-menu-item-text css-truncate-target" title="webdav_rfc5323_search">
                webdav_rfc5323_search
              </span>
            </a>
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.1.0alpha1/lib/private/helper.php"
                 data-name="v8.1.0alpha1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.1.0alpha1">v8.1.0alpha1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.3RC2/lib/private/helper.php"
                 data-name="v8.0.3RC2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.3RC2">v8.0.3RC2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.3RC1/lib/private/helper.php"
                 data-name="v8.0.3RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.3RC1">v8.0.3RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.2/lib/private/helper.php"
                 data-name="v8.0.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.2">v8.0.2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.1RC1/lib/private/helper.php"
                 data-name="v8.0.1RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.1RC1">v8.0.1RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.1/lib/private/helper.php"
                 data-name="v8.0.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.1">v8.0.1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.0beta2/lib/private/helper.php"
                 data-name="v8.0.0beta2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.0beta2">v8.0.0beta2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.0beta1/lib/private/helper.php"
                 data-name="v8.0.0beta1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.0beta1">v8.0.0beta1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.0alpha2/lib/private/helper.php"
                 data-name="v8.0.0alpha2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.0alpha2">v8.0.0alpha2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.0alpha1/lib/private/helper.php"
                 data-name="v8.0.0alpha1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.0alpha1">v8.0.0alpha1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.0RC2/lib/private/helper.php"
                 data-name="v8.0.0RC2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.0RC2">v8.0.0RC2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.0RC1/lib/private/helper.php"
                 data-name="v8.0.0RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.0RC1">v8.0.0RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v8.0.0/lib/private/helper.php"
                 data-name="v8.0.0"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v8.0.0">v8.0.0</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.5/lib/private/helper.php"
                 data-name="v7.0.5"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.5">v7.0.5</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.4RC2/lib/private/helper.php"
                 data-name="v7.0.4RC2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.4RC2">v7.0.4RC2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.4RC1/lib/private/helper.php"
                 data-name="v7.0.4RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.4RC1">v7.0.4RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.4/lib/private/helper.php"
                 data-name="v7.0.4"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.4">v7.0.4</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.3alpha1/lib/private/helper.php"
                 data-name="v7.0.3alpha1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.3alpha1">v7.0.3alpha1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.3RC3/lib/private/helper.php"
                 data-name="v7.0.3RC3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.3RC3">v7.0.3RC3</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.3RC2/lib/private/helper.php"
                 data-name="v7.0.3RC2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.3RC2">v7.0.3RC2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.3RC1/lib/private/helper.php"
                 data-name="v7.0.3RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.3RC1">v7.0.3RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.3/lib/private/helper.php"
                 data-name="v7.0.3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.3">v7.0.3</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.2RC1/lib/private/helper.php"
                 data-name="v7.0.2RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.2RC1">v7.0.2RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.2/lib/private/helper.php"
                 data-name="v7.0.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.2">v7.0.2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.1RC1/lib/private/helper.php"
                 data-name="v7.0.1RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.1RC1">v7.0.1RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.1/lib/private/helper.php"
                 data-name="v7.0.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.1">v7.0.1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.0beta1/lib/private/helper.php"
                 data-name="v7.0.0beta1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.0beta1">v7.0.0beta1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.0alpha2/lib/private/helper.php"
                 data-name="v7.0.0alpha2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.0alpha2">v7.0.0alpha2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.0RC3/lib/private/helper.php"
                 data-name="v7.0.0RC3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.0RC3">v7.0.0RC3</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.0RC2/lib/private/helper.php"
                 data-name="v7.0.0RC2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.0RC2">v7.0.0RC2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.0RC1/lib/private/helper.php"
                 data-name="v7.0.0RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.0RC1">v7.0.0RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v7.0.0/lib/private/helper.php"
                 data-name="v7.0.0"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v7.0.0">v7.0.0</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.7/lib/private/helper.php"
                 data-name="v6.0.7"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.7">v6.0.7</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.6RC1/lib/private/helper.php"
                 data-name="v6.0.6RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.6RC1">v6.0.6RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.6/lib/private/helper.php"
                 data-name="v6.0.6"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.6">v6.0.6</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.5RC1/lib/private/helper.php"
                 data-name="v6.0.5RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.5RC1">v6.0.5RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.5/lib/private/helper.php"
                 data-name="v6.0.5"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.5">v6.0.5</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.4beta1/lib/private/helper.php"
                 data-name="v6.0.4beta1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.4beta1">v6.0.4beta1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.4/lib/private/helper.php"
                 data-name="v6.0.4"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.4">v6.0.4</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.3RC1/lib/private/helper.php"
                 data-name="v6.0.3RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.3RC1">v6.0.3RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.3/lib/private/helper.php"
                 data-name="v6.0.3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.3">v6.0.3</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.2RC1/lib/private/helper.php"
                 data-name="v6.0.2RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.2RC1">v6.0.2RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.2/lib/private/helper.php"
                 data-name="v6.0.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.2">v6.0.2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.1RC1/lib/private/helper.php"
                 data-name="v6.0.1RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.1RC1">v6.0.1RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.1/lib/private/helper.php"
                 data-name="v6.0.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.1">v6.0.1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0beta5/lib/private/helper.php"
                 data-name="v6.0.0beta5"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0beta5">v6.0.0beta5</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0beta4/lib/private/helper.php"
                 data-name="v6.0.0beta4"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0beta4">v6.0.0beta4</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0beta3/lib/private/helper.php"
                 data-name="v6.0.0beta3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0beta3">v6.0.0beta3</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0beta2/lib/private/helper.php"
                 data-name="v6.0.0beta2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0beta2">v6.0.0beta2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0alpha2/lib/private/helper.php"
                 data-name="v6.0.0alpha2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0alpha2">v6.0.0alpha2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0a/lib/private/helper.php"
                 data-name="v6.0.0a"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0a">v6.0.0a</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0RC4/lib/private/helper.php"
                 data-name="v6.0.0RC4"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0RC4">v6.0.0RC4</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0RC3/lib/private/helper.php"
                 data-name="v6.0.0RC3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0RC3">v6.0.0RC3</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0RC2/lib/private/helper.php"
                 data-name="v6.0.0RC2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0RC2">v6.0.0RC2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0RC1/lib/private/helper.php"
                 data-name="v6.0.0RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0RC1">v6.0.0RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v6.0.0/lib/private/helper.php"
                 data-name="v6.0.0"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v6.0.0">v6.0.0</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.19/lib/private/helper.php"
                 data-name="v5.0.19"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.19">v5.0.19</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.17beta1/lib/private/helper.php"
                 data-name="v5.0.17beta1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.17beta1">v5.0.17beta1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.17/lib/private/helper.php"
                 data-name="v5.0.17"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.17">v5.0.17</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.16RC1/lib/private/helper.php"
                 data-name="v5.0.16RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.16RC1">v5.0.16RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.16/lib/private/helper.php"
                 data-name="v5.0.16"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.16">v5.0.16</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.15RC1/lib/private/helper.php"
                 data-name="v5.0.15RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.15RC1">v5.0.15RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.15/lib/private/helper.php"
                 data-name="v5.0.15"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.15">v5.0.15</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.14a/lib/private/helper.php"
                 data-name="v5.0.14a"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.14a">v5.0.14a</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.14/lib/private/helper.php"
                 data-name="v5.0.14"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.14">v5.0.14</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.13/lib/private/helper.php"
                 data-name="v5.0.13"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.13">v5.0.13</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.12/lib/private/helper.php"
                 data-name="v5.0.12"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.12">v5.0.12</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.11/lib/private/helper.php"
                 data-name="v5.0.11"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.11">v5.0.11</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.10/lib/private/helper.php"
                 data-name="v5.0.10"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.10">v5.0.10</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.9/lib/private/helper.php"
                 data-name="v5.0.9"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.9">v5.0.9</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.8/lib/private/helper.php"
                 data-name="v5.0.8"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.8">v5.0.8</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.7/lib/private/helper.php"
                 data-name="v5.0.7"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.7">v5.0.7</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.6/lib/private/helper.php"
                 data-name="v5.0.6"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.6">v5.0.6</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.5RC1/lib/private/helper.php"
                 data-name="v5.0.5RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.5RC1">v5.0.5RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.5/lib/private/helper.php"
                 data-name="v5.0.5"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.5">v5.0.5</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.4RC1/lib/private/helper.php"
                 data-name="v5.0.4RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.4RC1">v5.0.4RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.4/lib/private/helper.php"
                 data-name="v5.0.4"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.4">v5.0.4</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.3/lib/private/helper.php"
                 data-name="v5.0.3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.3">v5.0.3</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.2/lib/private/helper.php"
                 data-name="v5.0.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.2">v5.0.2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.1/lib/private/helper.php"
                 data-name="v5.0.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.1">v5.0.1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.0beta2/lib/private/helper.php"
                 data-name="v5.0.0beta2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.0beta2">v5.0.0beta2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.0beta1/lib/private/helper.php"
                 data-name="v5.0.0beta1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.0beta1">v5.0.0beta1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.0alpha1/lib/private/helper.php"
                 data-name="v5.0.0alpha1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.0alpha1">v5.0.0alpha1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.0RC3/lib/private/helper.php"
                 data-name="v5.0.0RC3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.0RC3">v5.0.0RC3</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.0RC2/lib/private/helper.php"
                 data-name="v5.0.0RC2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.0RC2">v5.0.0RC2</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.0RC1/lib/private/helper.php"
                 data-name="v5.0.0RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.0RC1">v5.0.0RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v5.0.0/lib/private/helper.php"
                 data-name="v5.0.0"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v5.0.0">v5.0.0</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.13/lib/private/helper.php"
                 data-name="v4.5.13"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.13">v4.5.13</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.12/lib/private/helper.php"
                 data-name="v4.5.12"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.12">v4.5.12</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.11/lib/private/helper.php"
                 data-name="v4.5.11"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.11">v4.5.11</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.10RC1/lib/private/helper.php"
                 data-name="v4.5.10RC1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.10RC1">v4.5.10RC1</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.10/lib/private/helper.php"
                 data-name="v4.5.10"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.10">v4.5.10</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.9/lib/private/helper.php"
                 data-name="v4.5.9"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.9">v4.5.9</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.8/lib/private/helper.php"
                 data-name="v4.5.8"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.8">v4.5.8</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.7/lib/private/helper.php"
                 data-name="v4.5.7"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.7">v4.5.7</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.6/lib/private/helper.php"
                 data-name="v4.5.6"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.6">v4.5.6</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.5/lib/private/helper.php"
                 data-name="v4.5.5"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.5">v4.5.5</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.4/lib/private/helper.php"
                 data-name="v4.5.4"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.4">v4.5.4</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.3/lib/private/helper.php"
                 data-name="v4.5.3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.3">v4.5.3</a>
            </div>
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/owncloud/core/tree/v4.5.2/lib/private/helper.php"
                 data-name="v4.5.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text css-truncate-target"
                 title="v4.5.2">v4.5.2</a>
            </div>
        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div>

    </div>
  </div>
</div>

  <div class="btn-group right">
    <a href="/owncloud/core/find/master"
          class="js-show-file-finder btn btn-sm empty-icon tooltipped tooltipped-s"
          data-pjax
          data-hotkey="t"
          aria-label="Quickly jump between files">
      <span class="octicon octicon-list-unordered"></span>
    </a>
    <button aria-label="Copy file path to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" data-copy-hint="Copy file path to clipboard" type="button"><span class="octicon octicon-clippy"></span></button>
  </div>

  <div class="breadcrumb js-zeroclipboard-target">
    <span class='repo-root js-repo-root'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/owncloud/core" class="" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">core</span></a></span></span><span class="separator">/</span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/owncloud/core/tree/master/lib" class="" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">lib</span></a></span><span class="separator">/</span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/owncloud/core/tree/master/lib/private" class="" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">private</span></a></span><span class="separator">/</span><strong class="final-path">helper.php</strong>
  </div>
</div>


  <div class="commit file-history-tease">
    <div class="file-history-tease-header">
        <img alt="@oparoz" class="avatar" data-user="323220" height="24" src="https://avatars2.githubusercontent.com/u/323220?v=3&amp;s=48" width="24" />
        <span class="author"><a href="/oparoz" rel="contributor">oparoz</a></span>
        <time datetime="2015-04-14T12:06:58Z" is="relative-time">Apr 14, 2015</time>
        <div class="commit-title">
            <a href="/owncloud/core/commit/bb0c09e994869b216b8163d262ffc7db9d439221" class="message" data-pjax="true" title="Adding a final fallback for findBinaryPath">Adding a final fallback for findBinaryPath</a>
        </div>
    </div>

    <div class="participation">
      <p class="quickstat">
        <a href="#blob_contributors_box" rel="facebox">
          <strong>24</strong>
           contributors
        </a>
      </p>
          <a class="avatar-link tooltipped tooltipped-s" aria-label="DeepDiver1975" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=DeepDiver1975"><img alt="@DeepDiver1975" class="avatar" data-user="1005065" height="20" src="https://avatars2.githubusercontent.com/u/1005065?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="PVince81" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=PVince81"><img alt="@PVince81" class="avatar" data-user="277525" height="20" src="https://avatars0.githubusercontent.com/u/277525?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="icewind1991" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=icewind1991"><img alt="@icewind1991" class="avatar" data-user="1283854" height="20" src="https://avatars1.githubusercontent.com/u/1283854?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="oparoz" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=oparoz"><img alt="@oparoz" class="avatar" data-user="323220" height="20" src="https://avatars0.githubusercontent.com/u/323220?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="LukasReschke" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=LukasReschke"><img alt="@LukasReschke" class="avatar" data-user="878997" height="20" src="https://avatars3.githubusercontent.com/u/878997?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="tanghus" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=tanghus"><img alt="@tanghus" class="avatar" data-user="533222" height="20" src="https://avatars1.githubusercontent.com/u/533222?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="MorrisJobke" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=MorrisJobke"><img alt="@MorrisJobke" class="avatar" data-user="245432" height="20" src="https://avatars2.githubusercontent.com/u/245432?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="jancborchardt" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=jancborchardt"><img alt="@jancborchardt" class="avatar" data-user="925062" height="20" src="https://avatars3.githubusercontent.com/u/925062?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="Xenopathic" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=Xenopathic"><img alt="@Xenopathic" class="avatar" data-user="2016878" height="20" src="https://avatars2.githubusercontent.com/u/2016878?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="rullzer" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=rullzer"><img alt="@rullzer" class="avatar" data-user="45821" height="20" src="https://avatars3.githubusercontent.com/u/45821?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="pellaeon" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=pellaeon"><img alt="@pellaeon" class="avatar" data-user="916913" height="20" src="https://avatars2.githubusercontent.com/u/916913?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="bantu" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=bantu"><img alt="@bantu" class="avatar" data-user="189375" height="20" src="https://avatars1.githubusercontent.com/u/189375?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="Kondou-ger" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=Kondou-ger"><img alt="@Kondou-ger" class="avatar" data-user="1410802" height="20" src="https://avatars0.githubusercontent.com/u/1410802?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="bartv2" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=bartv2"><img alt="@bartv2" class="avatar" data-user="2227877" height="20" src="https://avatars0.githubusercontent.com/u/2227877?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="owncloud-bot" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=owncloud-bot"><img alt="@owncloud-bot" class="avatar" data-user="2709266" height="20" src="https://avatars1.githubusercontent.com/u/2709266?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="speijnik" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=speijnik"><img alt="@speijnik" class="avatar" data-user="150516" height="20" src="https://avatars2.githubusercontent.com/u/150516?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="nickvergessen" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=nickvergessen"><img alt="@nickvergessen" class="avatar" data-user="213943" height="20" src="https://avatars1.githubusercontent.com/u/213943?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="flyser" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=flyser"><img alt="@flyser" class="avatar" data-user="1144183" height="20" src="https://avatars2.githubusercontent.com/u/1144183?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="th3fallen" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=th3fallen"><img alt="@th3fallen" class="avatar" data-user="291859" height="20" src="https://avatars2.githubusercontent.com/u/291859?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="VicDeo" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=VicDeo"><img alt="@VicDeo" class="avatar" data-user="991300" height="20" src="https://avatars2.githubusercontent.com/u/991300?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="blizzz" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=blizzz"><img alt="@blizzz" class="avatar" data-user="2184312" height="20" src="https://avatars1.githubusercontent.com/u/2184312?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="simonkoennecke" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=simonkoennecke"><img alt="@simonkoennecke" class="avatar" data-user="873349" height="20" src="https://avatars1.githubusercontent.com/u/873349?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="NormalRa" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=NormalRa"><img alt="@NormalRa" class="avatar" data-user="2751480" height="20" src="https://avatars0.githubusercontent.com/u/2751480?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="ringmaster" href="/owncloud/core/commits/70480423ffa6e7d469141c3022e53ea151856bd0/lib/private/helper.php?author=ringmaster"><img alt="@ringmaster" class="avatar" data-user="127738" height="20" src="https://avatars3.githubusercontent.com/u/127738?v=3&amp;s=40" width="20" /> </a>


    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list">
          <li class="facebox-user-list-item">
            <img alt="@DeepDiver1975" data-user="1005065" height="24" src="https://avatars0.githubusercontent.com/u/1005065?v=3&amp;s=48" width="24" />
            <a href="/DeepDiver1975">DeepDiver1975</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@PVince81" data-user="277525" height="24" src="https://avatars2.githubusercontent.com/u/277525?v=3&amp;s=48" width="24" />
            <a href="/PVince81">PVince81</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@icewind1991" data-user="1283854" height="24" src="https://avatars3.githubusercontent.com/u/1283854?v=3&amp;s=48" width="24" />
            <a href="/icewind1991">icewind1991</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@oparoz" data-user="323220" height="24" src="https://avatars2.githubusercontent.com/u/323220?v=3&amp;s=48" width="24" />
            <a href="/oparoz">oparoz</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@LukasReschke" data-user="878997" height="24" src="https://avatars1.githubusercontent.com/u/878997?v=3&amp;s=48" width="24" />
            <a href="/LukasReschke">LukasReschke</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@tanghus" data-user="533222" height="24" src="https://avatars3.githubusercontent.com/u/533222?v=3&amp;s=48" width="24" />
            <a href="/tanghus">tanghus</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@MorrisJobke" data-user="245432" height="24" src="https://avatars0.githubusercontent.com/u/245432?v=3&amp;s=48" width="24" />
            <a href="/MorrisJobke">MorrisJobke</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@jancborchardt" data-user="925062" height="24" src="https://avatars1.githubusercontent.com/u/925062?v=3&amp;s=48" width="24" />
            <a href="/jancborchardt">jancborchardt</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@Xenopathic" data-user="2016878" height="24" src="https://avatars0.githubusercontent.com/u/2016878?v=3&amp;s=48" width="24" />
            <a href="/Xenopathic">Xenopathic</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@rullzer" data-user="45821" height="24" src="https://avatars1.githubusercontent.com/u/45821?v=3&amp;s=48" width="24" />
            <a href="/rullzer">rullzer</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@pellaeon" data-user="916913" height="24" src="https://avatars0.githubusercontent.com/u/916913?v=3&amp;s=48" width="24" />
            <a href="/pellaeon">pellaeon</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@bantu" data-user="189375" height="24" src="https://avatars3.githubusercontent.com/u/189375?v=3&amp;s=48" width="24" />
            <a href="/bantu">bantu</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@Kondou-ger" data-user="1410802" height="24" src="https://avatars2.githubusercontent.com/u/1410802?v=3&amp;s=48" width="24" />
            <a href="/Kondou-ger">Kondou-ger</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@bartv2" data-user="2227877" height="24" src="https://avatars2.githubusercontent.com/u/2227877?v=3&amp;s=48" width="24" />
            <a href="/bartv2">bartv2</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@owncloud-bot" data-user="2709266" height="24" src="https://avatars3.githubusercontent.com/u/2709266?v=3&amp;s=48" width="24" />
            <a href="/owncloud-bot">owncloud-bot</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@speijnik" data-user="150516" height="24" src="https://avatars0.githubusercontent.com/u/150516?v=3&amp;s=48" width="24" />
            <a href="/speijnik">speijnik</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@nickvergessen" data-user="213943" height="24" src="https://avatars3.githubusercontent.com/u/213943?v=3&amp;s=48" width="24" />
            <a href="/nickvergessen">nickvergessen</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@flyser" data-user="1144183" height="24" src="https://avatars0.githubusercontent.com/u/1144183?v=3&amp;s=48" width="24" />
            <a href="/flyser">flyser</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@th3fallen" data-user="291859" height="24" src="https://avatars0.githubusercontent.com/u/291859?v=3&amp;s=48" width="24" />
            <a href="/th3fallen">th3fallen</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@VicDeo" data-user="991300" height="24" src="https://avatars0.githubusercontent.com/u/991300?v=3&amp;s=48" width="24" />
            <a href="/VicDeo">VicDeo</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@blizzz" data-user="2184312" height="24" src="https://avatars3.githubusercontent.com/u/2184312?v=3&amp;s=48" width="24" />
            <a href="/blizzz">blizzz</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@simonkoennecke" data-user="873349" height="24" src="https://avatars3.githubusercontent.com/u/873349?v=3&amp;s=48" width="24" />
            <a href="/simonkoennecke">simonkoennecke</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@NormalRa" data-user="2751480" height="24" src="https://avatars2.githubusercontent.com/u/2751480?v=3&amp;s=48" width="24" />
            <a href="/NormalRa">NormalRa</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@ringmaster" data-user="127738" height="24" src="https://avatars1.githubusercontent.com/u/127738?v=3&amp;s=48" width="24" />
            <a href="/ringmaster">ringmaster</a>
          </li>
      </ul>
    </div>
  </div>

<div class="file">
  <div class="file-header">
    <div class="file-actions">

      <div class="btn-group">
        <a href="/owncloud/core/raw/master/lib/private/helper.php" class="btn btn-sm " id="raw-url">Raw</a>
          <a href="/owncloud/core/blame/master/lib/private/helper.php" class="btn btn-sm js-update-url-with-hash">Blame</a>
        <a href="/owncloud/core/commits/master/lib/private/helper.php" class="btn btn-sm " rel="nofollow">History</a>
      </div>


          <button type="button" class="octicon-btn disabled tooltipped tooltipped-n" aria-label="You must be signed in to make or propose changes">
            <span class="octicon octicon-pencil"></span>
          </button>

        <button type="button" class="octicon-btn octicon-btn-danger disabled tooltipped tooltipped-n" aria-label="You must be signed in to make or propose changes">
          <span class="octicon octicon-trashcan"></span>
        </button>
    </div>

    <div class="file-info">
        1048 lines (963 sloc)
        <span class="file-info-divider"></span>
      32.988 kb
    </div>
  </div>
  
  <div class="blob-wrapper data type-php">
      <table class="highlight tab-size-8 js-file-line-container">
      <tr>
        <td id="L1" class="blob-num js-line-number" data-line-number="1"></td>
        <td id="LC1" class="blob-code js-file-line"><span class="pl-pse">&lt;?php</span><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L2" class="blob-num js-line-number" data-line-number="2"></td>
        <td id="LC2" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L3" class="blob-num js-line-number" data-line-number="3"></td>
        <td id="LC3" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Arthur Schiwon &lt;blizzz@owncloud.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L4" class="blob-num js-line-number" data-line-number="4"></td>
        <td id="LC4" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Bart Visscher &lt;bartv@thisnet.nl&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L5" class="blob-num js-line-number" data-line-number="5"></td>
        <td id="LC5" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Björn Schießle &lt;schiessle@owncloud.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L6" class="blob-num js-line-number" data-line-number="6"></td>
        <td id="LC6" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Christopher Schäpers &lt;kondou@ts.unde.re&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L7" class="blob-num js-line-number" data-line-number="7"></td>
        <td id="LC7" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Fabian Henze &lt;flyser42@gmx.de&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L8" class="blob-num js-line-number" data-line-number="8"></td>
        <td id="LC8" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Felix Moeller &lt;mail@felixmoeller.de&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L9" class="blob-num js-line-number" data-line-number="9"></td>
        <td id="LC9" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> François Kubler &lt;francois@kubler.org&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L10" class="blob-num js-line-number" data-line-number="10"></td>
        <td id="LC10" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Frank Karlitschek &lt;frank@owncloud.org&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L11" class="blob-num js-line-number" data-line-number="11"></td>
        <td id="LC11" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Georg Ehrke &lt;georg@owncloud.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L12" class="blob-num js-line-number" data-line-number="12"></td>
        <td id="LC12" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Jakob Sack &lt;mail@jakobsack.de&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L13" class="blob-num js-line-number" data-line-number="13"></td>
        <td id="LC13" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Jan-Christoph Borchardt &lt;hey@jancborchardt.net&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L14" class="blob-num js-line-number" data-line-number="14"></td>
        <td id="LC14" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Joas Schilling &lt;nickvergessen@owncloud.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L15" class="blob-num js-line-number" data-line-number="15"></td>
        <td id="LC15" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Jörn Friedrich Dreyer &lt;jfd@butonic.de&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L16" class="blob-num js-line-number" data-line-number="16"></td>
        <td id="LC16" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Lukas Reschke &lt;lukas@owncloud.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L17" class="blob-num js-line-number" data-line-number="17"></td>
        <td id="LC17" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Michael Gapczynski &lt;GapczynskiM@gmail.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L18" class="blob-num js-line-number" data-line-number="18"></td>
        <td id="LC18" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Morris Jobke &lt;hey@morrisjobke.de&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L19" class="blob-num js-line-number" data-line-number="19"></td>
        <td id="LC19" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Olivier Paroz &lt;owncloud@interfasys.ch&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L20" class="blob-num js-line-number" data-line-number="20"></td>
        <td id="LC20" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Owen Winkler &lt;a_github@midnightcircus.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L21" class="blob-num js-line-number" data-line-number="21"></td>
        <td id="LC21" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Pellaeon Lin &lt;nfsmwlin@gmail.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L22" class="blob-num js-line-number" data-line-number="22"></td>
        <td id="LC22" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Robin Appelman &lt;icewind@owncloud.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L23" class="blob-num js-line-number" data-line-number="23"></td>
        <td id="LC23" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Robin McCorkell &lt;rmccorkell@karoshi.org.uk&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L24" class="blob-num js-line-number" data-line-number="24"></td>
        <td id="LC24" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Roeland Jago Douma &lt;roeland@famdouma.nl&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L25" class="blob-num js-line-number" data-line-number="25"></td>
        <td id="LC25" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Simon Könnecke &lt;simonkoennecke@gmail.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L26" class="blob-num js-line-number" data-line-number="26"></td>
        <td id="LC26" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Thomas Müller &lt;thomas.mueller@tmit.eu&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L27" class="blob-num js-line-number" data-line-number="27"></td>
        <td id="LC27" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Thomas Tanghus &lt;thomas@tanghus.net&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L28" class="blob-num js-line-number" data-line-number="28"></td>
        <td id="LC28" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Valerio Ponte &lt;valerio.ponte@gmail.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L29" class="blob-num js-line-number" data-line-number="29"></td>
        <td id="LC29" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@author</span> Vincent Petry &lt;pvince81@owncloud.com&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L30" class="blob-num js-line-number" data-line-number="30"></td>
        <td id="LC30" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> *</span></span></td>
      </tr>
      <tr>
        <td id="L31" class="blob-num js-line-number" data-line-number="31"></td>
        <td id="LC31" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@copyright</span> Copyright (c) 2015, ownCloud, Inc.</span></span></td>
      </tr>
      <tr>
        <td id="L32" class="blob-num js-line-number" data-line-number="32"></td>
        <td id="LC32" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * <span class="pl-k">@license</span> AGPL-3.0</span></span></td>
      </tr>
      <tr>
        <td id="L33" class="blob-num js-line-number" data-line-number="33"></td>
        <td id="LC33" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> *</span></span></td>
      </tr>
      <tr>
        <td id="L34" class="blob-num js-line-number" data-line-number="34"></td>
        <td id="LC34" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * This code is free software: you can redistribute it and/or modify</span></span></td>
      </tr>
      <tr>
        <td id="L35" class="blob-num js-line-number" data-line-number="35"></td>
        <td id="LC35" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * it under the terms of the GNU Affero General Public License, version 3,</span></span></td>
      </tr>
      <tr>
        <td id="L36" class="blob-num js-line-number" data-line-number="36"></td>
        <td id="LC36" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * as published by the Free Software Foundation.</span></span></td>
      </tr>
      <tr>
        <td id="L37" class="blob-num js-line-number" data-line-number="37"></td>
        <td id="LC37" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> *</span></span></td>
      </tr>
      <tr>
        <td id="L38" class="blob-num js-line-number" data-line-number="38"></td>
        <td id="LC38" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * This program is distributed in the hope that it will be useful,</span></span></td>
      </tr>
      <tr>
        <td id="L39" class="blob-num js-line-number" data-line-number="39"></td>
        <td id="LC39" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * but WITHOUT ANY WARRANTY; without even the implied warranty of</span></span></td>
      </tr>
      <tr>
        <td id="L40" class="blob-num js-line-number" data-line-number="40"></td>
        <td id="LC40" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the</span></span></td>
      </tr>
      <tr>
        <td id="L41" class="blob-num js-line-number" data-line-number="41"></td>
        <td id="LC41" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * GNU Affero General Public License for more details.</span></span></td>
      </tr>
      <tr>
        <td id="L42" class="blob-num js-line-number" data-line-number="42"></td>
        <td id="LC42" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> *</span></span></td>
      </tr>
      <tr>
        <td id="L43" class="blob-num js-line-number" data-line-number="43"></td>
        <td id="LC43" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * You should have received a copy of the GNU Affero General Public License, version 3,</span></span></td>
      </tr>
      <tr>
        <td id="L44" class="blob-num js-line-number" data-line-number="44"></td>
        <td id="LC44" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * along with this program.  If not, see &lt;http://www.gnu.org/licenses/&gt;</span></span></td>
      </tr>
      <tr>
        <td id="L45" class="blob-num js-line-number" data-line-number="45"></td>
        <td id="LC45" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> *</span></span></td>
      </tr>
      <tr>
        <td id="L46" class="blob-num js-line-number" data-line-number="46"></td>
        <td id="LC46" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> */</span></span></td>
      </tr>
      <tr>
        <td id="L47" class="blob-num js-line-number" data-line-number="47"></td>
        <td id="LC47" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-k">use</span> <span class="pl-c1">Symfony\Component\Process\ExecutableFinder</span>;</span></td>
      </tr>
      <tr>
        <td id="L48" class="blob-num js-line-number" data-line-number="48"></td>
        <td id="LC48" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L49" class="blob-num js-line-number" data-line-number="49"></td>
        <td id="LC49" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L50" class="blob-num js-line-number" data-line-number="50"></td>
        <td id="LC50" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> * Collection of useful functions</span></span></td>
      </tr>
      <tr>
        <td id="L51" class="blob-num js-line-number" data-line-number="51"></td>
        <td id="LC51" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c"> */</span></span></td>
      </tr>
      <tr>
        <td id="L52" class="blob-num js-line-number" data-line-number="52"></td>
        <td id="LC52" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-k">class</span> <span class="pl-en">OC_Helper</span> {</span></td>
      </tr>
      <tr>
        <td id="L53" class="blob-num js-line-number" data-line-number="53"></td>
        <td id="LC53" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">private</span> <span class="pl-k">static</span> <span class="pl-smi">$mimetypeIcons</span> <span class="pl-k">=</span> <span class="pl-c1">array</span>();</span></td>
      </tr>
      <tr>
        <td id="L54" class="blob-num js-line-number" data-line-number="54"></td>
        <td id="LC54" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">private</span> <span class="pl-k">static</span> <span class="pl-smi">$mimetypeDetector</span>;</span></td>
      </tr>
      <tr>
        <td id="L55" class="blob-num js-line-number" data-line-number="55"></td>
        <td id="LC55" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">private</span> <span class="pl-k">static</span> <span class="pl-smi">$templateManager</span>;</span></td>
      </tr>
      <tr>
        <td id="L56" class="blob-num js-line-number" data-line-number="56"></td>
        <td id="LC56" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/** @var string[] */</span></span></td>
      </tr>
      <tr>
        <td id="L57" class="blob-num js-line-number" data-line-number="57"></td>
        <td id="LC57" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">private</span> <span class="pl-k">static</span> <span class="pl-smi">$mimeTypeAlias</span> <span class="pl-k">=</span> <span class="pl-c1">array</span>(</span></td>
      </tr>
      <tr>
        <td id="L58" class="blob-num js-line-number" data-line-number="58"></td>
        <td id="LC58" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/octet-stream<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>file<span class="pl-pds">&#39;</span></span>, <span class="pl-c">// use file icon as fallback</span></span></td>
      </tr>
      <tr>
        <td id="L59" class="blob-num js-line-number" data-line-number="59"></td>
        <td id="LC59" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L60" class="blob-num js-line-number" data-line-number="60"></td>
        <td id="LC60" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/illustrator<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>image/vector<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L61" class="blob-num js-line-number" data-line-number="61"></td>
        <td id="LC61" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/postscript<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>image/vector<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L62" class="blob-num js-line-number" data-line-number="62"></td>
        <td id="LC62" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>image/svg+xml<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>image/vector<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L63" class="blob-num js-line-number" data-line-number="63"></td>
        <td id="LC63" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L64" class="blob-num js-line-number" data-line-number="64"></td>
        <td id="LC64" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/coreldraw<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>image<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L65" class="blob-num js-line-number" data-line-number="65"></td>
        <td id="LC65" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-gimp<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>image<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L66" class="blob-num js-line-number" data-line-number="66"></td>
        <td id="LC66" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-photoshop<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>image<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L67" class="blob-num js-line-number" data-line-number="67"></td>
        <td id="LC67" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-dcraw<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>image<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L68" class="blob-num js-line-number" data-line-number="68"></td>
        <td id="LC68" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L69" class="blob-num js-line-number" data-line-number="69"></td>
        <td id="LC69" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/font-sfnt<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>font<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L70" class="blob-num js-line-number" data-line-number="70"></td>
        <td id="LC70" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-font<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>font<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L71" class="blob-num js-line-number" data-line-number="71"></td>
        <td id="LC71" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/font-woff<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>font<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L72" class="blob-num js-line-number" data-line-number="72"></td>
        <td id="LC72" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-fontobject<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>font<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L73" class="blob-num js-line-number" data-line-number="73"></td>
        <td id="LC73" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L74" class="blob-num js-line-number" data-line-number="74"></td>
        <td id="LC74" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/json<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>text/code<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L75" class="blob-num js-line-number" data-line-number="75"></td>
        <td id="LC75" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-perl<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>text/code<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L76" class="blob-num js-line-number" data-line-number="76"></td>
        <td id="LC76" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-php<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>text/code<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L77" class="blob-num js-line-number" data-line-number="77"></td>
        <td id="LC77" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>text/x-shellscript<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>text/code<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L78" class="blob-num js-line-number" data-line-number="78"></td>
        <td id="LC78" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/yaml<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>text/code<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L79" class="blob-num js-line-number" data-line-number="79"></td>
        <td id="LC79" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/xml<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>text/html<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L80" class="blob-num js-line-number" data-line-number="80"></td>
        <td id="LC80" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>text/css<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>text/code<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L81" class="blob-num js-line-number" data-line-number="81"></td>
        <td id="LC81" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-tex<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>text<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L82" class="blob-num js-line-number" data-line-number="82"></td>
        <td id="LC82" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L83" class="blob-num js-line-number" data-line-number="83"></td>
        <td id="LC83" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-compressed<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>package/x-generic<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L84" class="blob-num js-line-number" data-line-number="84"></td>
        <td id="LC84" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-7z-compressed<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>package/x-generic<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L85" class="blob-num js-line-number" data-line-number="85"></td>
        <td id="LC85" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-deb<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>package/x-generic<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L86" class="blob-num js-line-number" data-line-number="86"></td>
        <td id="LC86" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-gzip<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>package/x-generic<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L87" class="blob-num js-line-number" data-line-number="87"></td>
        <td id="LC87" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-rar-compressed<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>package/x-generic<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L88" class="blob-num js-line-number" data-line-number="88"></td>
        <td id="LC88" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/x-tar<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>package/x-generic<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L89" class="blob-num js-line-number" data-line-number="89"></td>
        <td id="LC89" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.android.package-archive<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>package/x-generic<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L90" class="blob-num js-line-number" data-line-number="90"></td>
        <td id="LC90" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/zip<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>package/x-generic<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L91" class="blob-num js-line-number" data-line-number="91"></td>
        <td id="LC91" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L92" class="blob-num js-line-number" data-line-number="92"></td>
        <td id="LC92" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/msword<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/document<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L93" class="blob-num js-line-number" data-line-number="93"></td>
        <td id="LC93" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.openxmlformats-officedocument.wordprocessingml.document<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/document<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L94" class="blob-num js-line-number" data-line-number="94"></td>
        <td id="LC94" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.openxmlformats-officedocument.wordprocessingml.template<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/document<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L95" class="blob-num js-line-number" data-line-number="95"></td>
        <td id="LC95" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-word.document.macroEnabled.12<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/document<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L96" class="blob-num js-line-number" data-line-number="96"></td>
        <td id="LC96" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-word.template.macroEnabled.12<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/document<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L97" class="blob-num js-line-number" data-line-number="97"></td>
        <td id="LC97" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.oasis.opendocument.text<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/document<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L98" class="blob-num js-line-number" data-line-number="98"></td>
        <td id="LC98" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.oasis.opendocument.text-template<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/document<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L99" class="blob-num js-line-number" data-line-number="99"></td>
        <td id="LC99" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.oasis.opendocument.text-web<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/document<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L100" class="blob-num js-line-number" data-line-number="100"></td>
        <td id="LC100" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.oasis.opendocument.text-master<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/document<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L101" class="blob-num js-line-number" data-line-number="101"></td>
        <td id="LC101" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L102" class="blob-num js-line-number" data-line-number="102"></td>
        <td id="LC102" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/mspowerpoint<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L103" class="blob-num js-line-number" data-line-number="103"></td>
        <td id="LC103" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-powerpoint<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L104" class="blob-num js-line-number" data-line-number="104"></td>
        <td id="LC104" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.openxmlformats-officedocument.presentationml.presentation<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L105" class="blob-num js-line-number" data-line-number="105"></td>
        <td id="LC105" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.openxmlformats-officedocument.presentationml.template<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L106" class="blob-num js-line-number" data-line-number="106"></td>
        <td id="LC106" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.openxmlformats-officedocument.presentationml.slideshow<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L107" class="blob-num js-line-number" data-line-number="107"></td>
        <td id="LC107" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-powerpoint.addin.macroEnabled.12<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L108" class="blob-num js-line-number" data-line-number="108"></td>
        <td id="LC108" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-powerpoint.presentation.macroEnabled.12<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L109" class="blob-num js-line-number" data-line-number="109"></td>
        <td id="LC109" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-powerpoint.template.macroEnabled.12<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L110" class="blob-num js-line-number" data-line-number="110"></td>
        <td id="LC110" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-powerpoint.slideshow.macroEnabled.12<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L111" class="blob-num js-line-number" data-line-number="111"></td>
        <td id="LC111" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.oasis.opendocument.presentation<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L112" class="blob-num js-line-number" data-line-number="112"></td>
        <td id="LC112" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.oasis.opendocument.presentation-template<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/presentation<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L113" class="blob-num js-line-number" data-line-number="113"></td>
        <td id="LC113" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L114" class="blob-num js-line-number" data-line-number="114"></td>
        <td id="LC114" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/msexcel<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L115" class="blob-num js-line-number" data-line-number="115"></td>
        <td id="LC115" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-excel<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L116" class="blob-num js-line-number" data-line-number="116"></td>
        <td id="LC116" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.openxmlformats-officedocument.spreadsheetml.sheet<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L117" class="blob-num js-line-number" data-line-number="117"></td>
        <td id="LC117" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.openxmlformats-officedocument.spreadsheetml.template<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L118" class="blob-num js-line-number" data-line-number="118"></td>
        <td id="LC118" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-excel.sheet.macroEnabled.12<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L119" class="blob-num js-line-number" data-line-number="119"></td>
        <td id="LC119" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-excel.template.macroEnabled.12<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L120" class="blob-num js-line-number" data-line-number="120"></td>
        <td id="LC120" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-excel.addin.macroEnabled.12<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L121" class="blob-num js-line-number" data-line-number="121"></td>
        <td id="LC121" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.ms-excel.sheet.binary.macroEnabled.12<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L122" class="blob-num js-line-number" data-line-number="122"></td>
        <td id="LC122" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.oasis.opendocument.spreadsheet<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L123" class="blob-num js-line-number" data-line-number="123"></td>
        <td id="LC123" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/vnd.oasis.opendocument.spreadsheet-template<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L124" class="blob-num js-line-number" data-line-number="124"></td>
        <td id="LC124" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>text/csv<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>x-office/spreadsheet<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L125" class="blob-num js-line-number" data-line-number="125"></td>
        <td id="LC125" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L126" class="blob-num js-line-number" data-line-number="126"></td>
        <td id="LC126" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-s"><span class="pl-pds">&#39;</span>application/msaccess<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-s"><span class="pl-pds">&#39;</span>database<span class="pl-pds">&#39;</span></span>,</span></td>
      </tr>
      <tr>
        <td id="L127" class="blob-num js-line-number" data-line-number="127"></td>
        <td id="LC127" class="blob-code js-file-line"><span class="pl-s1">	);</span></td>
      </tr>
      <tr>
        <td id="L128" class="blob-num js-line-number" data-line-number="128"></td>
        <td id="LC128" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L129" class="blob-num js-line-number" data-line-number="129"></td>
        <td id="LC129" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L130" class="blob-num js-line-number" data-line-number="130"></td>
        <td id="LC130" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Creates an url using a defined route</span></span></td>
      </tr>
      <tr>
        <td id="L131" class="blob-num js-line-number" data-line-number="131"></td>
        <td id="LC131" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $route</span></span></td>
      </tr>
      <tr>
        <td id="L132" class="blob-num js-line-number" data-line-number="132"></td>
        <td id="LC132" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> array $parameters</span></span></td>
      </tr>
      <tr>
        <td id="L133" class="blob-num js-line-number" data-line-number="133"></td>
        <td id="LC133" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span></span></span></td>
      </tr>
      <tr>
        <td id="L134" class="blob-num js-line-number" data-line-number="134"></td>
        <td id="LC134" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@internal</span> param array $args with param=&gt;value, will be appended to the returned url</span></span></td>
      </tr>
      <tr>
        <td id="L135" class="blob-num js-line-number" data-line-number="135"></td>
        <td id="LC135" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the url</span></span></td>
      </tr>
      <tr>
        <td id="L136" class="blob-num js-line-number" data-line-number="136"></td>
        <td id="LC136" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@deprecated</span> Use \OC::$server-&gt;getURLGenerator()-&gt;linkToRoute($route, $parameters)</span></span></td>
      </tr>
      <tr>
        <td id="L137" class="blob-num js-line-number" data-line-number="137"></td>
        <td id="LC137" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L138" class="blob-num js-line-number" data-line-number="138"></td>
        <td id="LC138" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns a url to the given app and file.</span></span></td>
      </tr>
      <tr>
        <td id="L139" class="blob-num js-line-number" data-line-number="139"></td>
        <td id="LC139" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L140" class="blob-num js-line-number" data-line-number="140"></td>
        <td id="LC140" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">linkToRoute</span>(<span class="pl-smi">$route</span>, <span class="pl-smi">$parameters</span> <span class="pl-k">=</span> <span class="pl-c1">array</span>()) {</span></td>
      </tr>
      <tr>
        <td id="L141" class="blob-num js-line-number" data-line-number="141"></td>
        <td id="LC141" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getURLGenerator()<span class="pl-k">-&gt;</span>linkToRoute(<span class="pl-smi">$route</span>, <span class="pl-smi">$parameters</span>);</span></td>
      </tr>
      <tr>
        <td id="L142" class="blob-num js-line-number" data-line-number="142"></td>
        <td id="LC142" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L143" class="blob-num js-line-number" data-line-number="143"></td>
        <td id="LC143" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L144" class="blob-num js-line-number" data-line-number="144"></td>
        <td id="LC144" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L145" class="blob-num js-line-number" data-line-number="145"></td>
        <td id="LC145" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Creates an url</span></span></td>
      </tr>
      <tr>
        <td id="L146" class="blob-num js-line-number" data-line-number="146"></td>
        <td id="LC146" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $app app</span></span></td>
      </tr>
      <tr>
        <td id="L147" class="blob-num js-line-number" data-line-number="147"></td>
        <td id="LC147" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $file file</span></span></td>
      </tr>
      <tr>
        <td id="L148" class="blob-num js-line-number" data-line-number="148"></td>
        <td id="LC148" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> array $args array with param=&gt;value, will be appended to the returned url</span></span></td>
      </tr>
      <tr>
        <td id="L149" class="blob-num js-line-number" data-line-number="149"></td>
        <td id="LC149" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *    The value of $args will be urlencoded</span></span></td>
      </tr>
      <tr>
        <td id="L150" class="blob-num js-line-number" data-line-number="150"></td>
        <td id="LC150" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the url</span></span></td>
      </tr>
      <tr>
        <td id="L151" class="blob-num js-line-number" data-line-number="151"></td>
        <td id="LC151" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@deprecated</span> Use \OC::$server-&gt;getURLGenerator()-&gt;linkTo($app, $file, $args)</span></span></td>
      </tr>
      <tr>
        <td id="L152" class="blob-num js-line-number" data-line-number="152"></td>
        <td id="LC152" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L153" class="blob-num js-line-number" data-line-number="153"></td>
        <td id="LC153" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns a url to the given app and file.</span></span></td>
      </tr>
      <tr>
        <td id="L154" class="blob-num js-line-number" data-line-number="154"></td>
        <td id="LC154" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L155" class="blob-num js-line-number" data-line-number="155"></td>
        <td id="LC155" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">linkTo</span>( <span class="pl-smi">$app</span>, <span class="pl-smi">$file</span>, <span class="pl-smi">$args</span> <span class="pl-k">=</span> <span class="pl-c1">array</span>() ) {</span></td>
      </tr>
      <tr>
        <td id="L156" class="blob-num js-line-number" data-line-number="156"></td>
        <td id="LC156" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getURLGenerator()<span class="pl-k">-&gt;</span>linkTo(<span class="pl-smi">$app</span>, <span class="pl-smi">$file</span>, <span class="pl-smi">$args</span>);</span></td>
      </tr>
      <tr>
        <td id="L157" class="blob-num js-line-number" data-line-number="157"></td>
        <td id="LC157" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L158" class="blob-num js-line-number" data-line-number="158"></td>
        <td id="LC158" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L159" class="blob-num js-line-number" data-line-number="159"></td>
        <td id="LC159" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L160" class="blob-num js-line-number" data-line-number="160"></td>
        <td id="LC160" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $key</span></span></td>
      </tr>
      <tr>
        <td id="L161" class="blob-num js-line-number" data-line-number="161"></td>
        <td id="LC161" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string url to the online documentation</span></span></td>
      </tr>
      <tr>
        <td id="L162" class="blob-num js-line-number" data-line-number="162"></td>
        <td id="LC162" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@deprecated</span> Use \OC::$server-&gt;getURLGenerator()-&gt;linkToDocs($key)</span></span></td>
      </tr>
      <tr>
        <td id="L163" class="blob-num js-line-number" data-line-number="163"></td>
        <td id="LC163" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L164" class="blob-num js-line-number" data-line-number="164"></td>
        <td id="LC164" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">linkToDocs</span>(<span class="pl-smi">$key</span>) {</span></td>
      </tr>
      <tr>
        <td id="L165" class="blob-num js-line-number" data-line-number="165"></td>
        <td id="LC165" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getURLGenerator()<span class="pl-k">-&gt;</span>linkToDocs(<span class="pl-smi">$key</span>);</span></td>
      </tr>
      <tr>
        <td id="L166" class="blob-num js-line-number" data-line-number="166"></td>
        <td id="LC166" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L167" class="blob-num js-line-number" data-line-number="167"></td>
        <td id="LC167" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L168" class="blob-num js-line-number" data-line-number="168"></td>
        <td id="LC168" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L169" class="blob-num js-line-number" data-line-number="169"></td>
        <td id="LC169" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Creates an absolute url</span></span></td>
      </tr>
      <tr>
        <td id="L170" class="blob-num js-line-number" data-line-number="170"></td>
        <td id="LC170" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $app app</span></span></td>
      </tr>
      <tr>
        <td id="L171" class="blob-num js-line-number" data-line-number="171"></td>
        <td id="LC171" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $file file</span></span></td>
      </tr>
      <tr>
        <td id="L172" class="blob-num js-line-number" data-line-number="172"></td>
        <td id="LC172" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> array $args array with param=&gt;value, will be appended to the returned url</span></span></td>
      </tr>
      <tr>
        <td id="L173" class="blob-num js-line-number" data-line-number="173"></td>
        <td id="LC173" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *    The value of $args will be urlencoded</span></span></td>
      </tr>
      <tr>
        <td id="L174" class="blob-num js-line-number" data-line-number="174"></td>
        <td id="LC174" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the url</span></span></td>
      </tr>
      <tr>
        <td id="L175" class="blob-num js-line-number" data-line-number="175"></td>
        <td id="LC175" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L176" class="blob-num js-line-number" data-line-number="176"></td>
        <td id="LC176" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns a absolute url to the given app and file.</span></span></td>
      </tr>
      <tr>
        <td id="L177" class="blob-num js-line-number" data-line-number="177"></td>
        <td id="LC177" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L178" class="blob-num js-line-number" data-line-number="178"></td>
        <td id="LC178" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">linkToAbsolute</span>(<span class="pl-smi">$app</span>, <span class="pl-smi">$file</span>, <span class="pl-smi">$args</span> <span class="pl-k">=</span> <span class="pl-c1">array</span>()) {</span></td>
      </tr>
      <tr>
        <td id="L179" class="blob-num js-line-number" data-line-number="179"></td>
        <td id="LC179" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getURLGenerator()<span class="pl-k">-&gt;</span>getAbsoluteURL(</span></td>
      </tr>
      <tr>
        <td id="L180" class="blob-num js-line-number" data-line-number="180"></td>
        <td id="LC180" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span>linkTo(<span class="pl-smi">$app</span>, <span class="pl-smi">$file</span>, <span class="pl-smi">$args</span>)</span></td>
      </tr>
      <tr>
        <td id="L181" class="blob-num js-line-number" data-line-number="181"></td>
        <td id="LC181" class="blob-code js-file-line"><span class="pl-s1">		);</span></td>
      </tr>
      <tr>
        <td id="L182" class="blob-num js-line-number" data-line-number="182"></td>
        <td id="LC182" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L183" class="blob-num js-line-number" data-line-number="183"></td>
        <td id="LC183" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L184" class="blob-num js-line-number" data-line-number="184"></td>
        <td id="LC184" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L185" class="blob-num js-line-number" data-line-number="185"></td>
        <td id="LC185" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Makes an $url absolute</span></span></td>
      </tr>
      <tr>
        <td id="L186" class="blob-num js-line-number" data-line-number="186"></td>
        <td id="LC186" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $url the url</span></span></td>
      </tr>
      <tr>
        <td id="L187" class="blob-num js-line-number" data-line-number="187"></td>
        <td id="LC187" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the absolute url</span></span></td>
      </tr>
      <tr>
        <td id="L188" class="blob-num js-line-number" data-line-number="188"></td>
        <td id="LC188" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@deprecated</span> Use \OC::$server-&gt;getURLGenerator()-&gt;getAbsoluteURL($url)</span></span></td>
      </tr>
      <tr>
        <td id="L189" class="blob-num js-line-number" data-line-number="189"></td>
        <td id="LC189" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L190" class="blob-num js-line-number" data-line-number="190"></td>
        <td id="LC190" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns a absolute url to the given app and file.</span></span></td>
      </tr>
      <tr>
        <td id="L191" class="blob-num js-line-number" data-line-number="191"></td>
        <td id="LC191" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L192" class="blob-num js-line-number" data-line-number="192"></td>
        <td id="LC192" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">makeURLAbsolute</span>(<span class="pl-smi">$url</span>) {</span></td>
      </tr>
      <tr>
        <td id="L193" class="blob-num js-line-number" data-line-number="193"></td>
        <td id="LC193" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getURLGenerator()<span class="pl-k">-&gt;</span>getAbsoluteURL(<span class="pl-smi">$url</span>);</span></td>
      </tr>
      <tr>
        <td id="L194" class="blob-num js-line-number" data-line-number="194"></td>
        <td id="LC194" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L195" class="blob-num js-line-number" data-line-number="195"></td>
        <td id="LC195" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L196" class="blob-num js-line-number" data-line-number="196"></td>
        <td id="LC196" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L197" class="blob-num js-line-number" data-line-number="197"></td>
        <td id="LC197" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Creates an url for remote use</span></span></td>
      </tr>
      <tr>
        <td id="L198" class="blob-num js-line-number" data-line-number="198"></td>
        <td id="LC198" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $service id</span></span></td>
      </tr>
      <tr>
        <td id="L199" class="blob-num js-line-number" data-line-number="199"></td>
        <td id="LC199" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the url</span></span></td>
      </tr>
      <tr>
        <td id="L200" class="blob-num js-line-number" data-line-number="200"></td>
        <td id="LC200" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L201" class="blob-num js-line-number" data-line-number="201"></td>
        <td id="LC201" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns a url to the given service.</span></span></td>
      </tr>
      <tr>
        <td id="L202" class="blob-num js-line-number" data-line-number="202"></td>
        <td id="LC202" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L203" class="blob-num js-line-number" data-line-number="203"></td>
        <td id="LC203" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">linkToRemoteBase</span>(<span class="pl-smi">$service</span>) {</span></td>
      </tr>
      <tr>
        <td id="L204" class="blob-num js-line-number" data-line-number="204"></td>
        <td id="LC204" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span>linkTo(<span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-pds">&#39;</span></span>, <span class="pl-s"><span class="pl-pds">&#39;</span>remote.php<span class="pl-pds">&#39;</span></span>) <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$service</span>;</span></td>
      </tr>
      <tr>
        <td id="L205" class="blob-num js-line-number" data-line-number="205"></td>
        <td id="LC205" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L206" class="blob-num js-line-number" data-line-number="206"></td>
        <td id="LC206" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L207" class="blob-num js-line-number" data-line-number="207"></td>
        <td id="LC207" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L208" class="blob-num js-line-number" data-line-number="208"></td>
        <td id="LC208" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Creates an absolute url for remote use</span></span></td>
      </tr>
      <tr>
        <td id="L209" class="blob-num js-line-number" data-line-number="209"></td>
        <td id="LC209" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $service id</span></span></td>
      </tr>
      <tr>
        <td id="L210" class="blob-num js-line-number" data-line-number="210"></td>
        <td id="LC210" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> bool $add_slash</span></span></td>
      </tr>
      <tr>
        <td id="L211" class="blob-num js-line-number" data-line-number="211"></td>
        <td id="LC211" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the url</span></span></td>
      </tr>
      <tr>
        <td id="L212" class="blob-num js-line-number" data-line-number="212"></td>
        <td id="LC212" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L213" class="blob-num js-line-number" data-line-number="213"></td>
        <td id="LC213" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns a absolute url to the given service.</span></span></td>
      </tr>
      <tr>
        <td id="L214" class="blob-num js-line-number" data-line-number="214"></td>
        <td id="LC214" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L215" class="blob-num js-line-number" data-line-number="215"></td>
        <td id="LC215" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">linkToRemote</span>(<span class="pl-smi">$service</span>, <span class="pl-smi">$add_slash</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>) {</span></td>
      </tr>
      <tr>
        <td id="L216" class="blob-num js-line-number" data-line-number="216"></td>
        <td id="LC216" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getURLGenerator()<span class="pl-k">-&gt;</span>getAbsoluteURL(</span></td>
      </tr>
      <tr>
        <td id="L217" class="blob-num js-line-number" data-line-number="217"></td>
        <td id="LC217" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span>linkToRemoteBase(<span class="pl-smi">$service</span>)</span></td>
      </tr>
      <tr>
        <td id="L218" class="blob-num js-line-number" data-line-number="218"></td>
        <td id="LC218" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">.</span> ((<span class="pl-smi">$add_slash</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">$service</span>[<span class="pl-c1">strlen</span>(<span class="pl-smi">$service</span>) <span class="pl-k">-</span> <span class="pl-c1">1</span>] <span class="pl-k">!</span><span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/<span class="pl-pds">&#39;</span></span>) ? <span class="pl-s"><span class="pl-pds">&#39;</span>/<span class="pl-pds">&#39;</span></span> : <span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-pds">&#39;</span></span>)</span></td>
      </tr>
      <tr>
        <td id="L219" class="blob-num js-line-number" data-line-number="219"></td>
        <td id="LC219" class="blob-code js-file-line"><span class="pl-s1">		);</span></td>
      </tr>
      <tr>
        <td id="L220" class="blob-num js-line-number" data-line-number="220"></td>
        <td id="LC220" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L221" class="blob-num js-line-number" data-line-number="221"></td>
        <td id="LC221" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L222" class="blob-num js-line-number" data-line-number="222"></td>
        <td id="LC222" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L223" class="blob-num js-line-number" data-line-number="223"></td>
        <td id="LC223" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Creates an absolute url for public use</span></span></td>
      </tr>
      <tr>
        <td id="L224" class="blob-num js-line-number" data-line-number="224"></td>
        <td id="LC224" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $service id</span></span></td>
      </tr>
      <tr>
        <td id="L225" class="blob-num js-line-number" data-line-number="225"></td>
        <td id="LC225" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> bool $add_slash</span></span></td>
      </tr>
      <tr>
        <td id="L226" class="blob-num js-line-number" data-line-number="226"></td>
        <td id="LC226" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the url</span></span></td>
      </tr>
      <tr>
        <td id="L227" class="blob-num js-line-number" data-line-number="227"></td>
        <td id="LC227" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L228" class="blob-num js-line-number" data-line-number="228"></td>
        <td id="LC228" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns a absolute url to the given service.</span></span></td>
      </tr>
      <tr>
        <td id="L229" class="blob-num js-line-number" data-line-number="229"></td>
        <td id="LC229" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L230" class="blob-num js-line-number" data-line-number="230"></td>
        <td id="LC230" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">linkToPublic</span>(<span class="pl-smi">$service</span>, <span class="pl-smi">$add_slash</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>) {</span></td>
      </tr>
      <tr>
        <td id="L231" class="blob-num js-line-number" data-line-number="231"></td>
        <td id="LC231" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$service</span> <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">&#39;</span>files<span class="pl-pds">&#39;</span></span>) {</span></td>
      </tr>
      <tr>
        <td id="L232" class="blob-num js-line-number" data-line-number="232"></td>
        <td id="LC232" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$url</span> <span class="pl-k">=</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getURLGenerator()<span class="pl-k">-&gt;</span>getAbsoluteURL(<span class="pl-s"><span class="pl-pds">&#39;</span>/s<span class="pl-pds">&#39;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L233" class="blob-num js-line-number" data-line-number="233"></td>
        <td id="LC233" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L234" class="blob-num js-line-number" data-line-number="234"></td>
        <td id="LC234" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$url</span> <span class="pl-k">=</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getURLGenerator()<span class="pl-k">-&gt;</span>getAbsoluteURL(<span class="pl-k">self</span><span class="pl-k">::</span>linkTo(<span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-pds">&#39;</span></span>, <span class="pl-s"><span class="pl-pds">&#39;</span>public.php<span class="pl-pds">&#39;</span></span>)<span class="pl-k">.</span><span class="pl-s"><span class="pl-pds">&#39;</span>?service=<span class="pl-pds">&#39;</span></span><span class="pl-k">.</span><span class="pl-smi">$service</span>);</span></td>
      </tr>
      <tr>
        <td id="L235" class="blob-num js-line-number" data-line-number="235"></td>
        <td id="LC235" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L236" class="blob-num js-line-number" data-line-number="236"></td>
        <td id="LC236" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-smi">$url</span> <span class="pl-k">.</span> ((<span class="pl-smi">$add_slash</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">$service</span>[<span class="pl-c1">strlen</span>(<span class="pl-smi">$service</span>) <span class="pl-k">-</span> <span class="pl-c1">1</span>] <span class="pl-k">!</span><span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/<span class="pl-pds">&#39;</span></span>) ? <span class="pl-s"><span class="pl-pds">&#39;</span>/<span class="pl-pds">&#39;</span></span> : <span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-pds">&#39;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L237" class="blob-num js-line-number" data-line-number="237"></td>
        <td id="LC237" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L238" class="blob-num js-line-number" data-line-number="238"></td>
        <td id="LC238" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L239" class="blob-num js-line-number" data-line-number="239"></td>
        <td id="LC239" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L240" class="blob-num js-line-number" data-line-number="240"></td>
        <td id="LC240" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Creates path to an image</span></span></td>
      </tr>
      <tr>
        <td id="L241" class="blob-num js-line-number" data-line-number="241"></td>
        <td id="LC241" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $app app</span></span></td>
      </tr>
      <tr>
        <td id="L242" class="blob-num js-line-number" data-line-number="242"></td>
        <td id="LC242" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $image image name</span></span></td>
      </tr>
      <tr>
        <td id="L243" class="blob-num js-line-number" data-line-number="243"></td>
        <td id="LC243" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the url</span></span></td>
      </tr>
      <tr>
        <td id="L244" class="blob-num js-line-number" data-line-number="244"></td>
        <td id="LC244" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@deprecated</span> Use \OC::$server-&gt;getURLGenerator()-&gt;imagePath($app, $image)</span></span></td>
      </tr>
      <tr>
        <td id="L245" class="blob-num js-line-number" data-line-number="245"></td>
        <td id="LC245" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L246" class="blob-num js-line-number" data-line-number="246"></td>
        <td id="LC246" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns the path to the image.</span></span></td>
      </tr>
      <tr>
        <td id="L247" class="blob-num js-line-number" data-line-number="247"></td>
        <td id="LC247" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L248" class="blob-num js-line-number" data-line-number="248"></td>
        <td id="LC248" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">imagePath</span>(<span class="pl-smi">$app</span>, <span class="pl-smi">$image</span>) {</span></td>
      </tr>
      <tr>
        <td id="L249" class="blob-num js-line-number" data-line-number="249"></td>
        <td id="LC249" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getURLGenerator()<span class="pl-k">-&gt;</span>imagePath(<span class="pl-smi">$app</span>, <span class="pl-smi">$image</span>);</span></td>
      </tr>
      <tr>
        <td id="L250" class="blob-num js-line-number" data-line-number="250"></td>
        <td id="LC250" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L251" class="blob-num js-line-number" data-line-number="251"></td>
        <td id="LC251" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L252" class="blob-num js-line-number" data-line-number="252"></td>
        <td id="LC252" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L253" class="blob-num js-line-number" data-line-number="253"></td>
        <td id="LC253" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * get path to icon of file type</span></span></td>
      </tr>
      <tr>
        <td id="L254" class="blob-num js-line-number" data-line-number="254"></td>
        <td id="LC254" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $mimetype mimetype</span></span></td>
      </tr>
      <tr>
        <td id="L255" class="blob-num js-line-number" data-line-number="255"></td>
        <td id="LC255" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the url</span></span></td>
      </tr>
      <tr>
        <td id="L256" class="blob-num js-line-number" data-line-number="256"></td>
        <td id="LC256" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L257" class="blob-num js-line-number" data-line-number="257"></td>
        <td id="LC257" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns the path to the image of this file type.</span></span></td>
      </tr>
      <tr>
        <td id="L258" class="blob-num js-line-number" data-line-number="258"></td>
        <td id="LC258" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L259" class="blob-num js-line-number" data-line-number="259"></td>
        <td id="LC259" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">mimetypeIcon</span>(<span class="pl-smi">$mimetype</span>) {</span></td>
      </tr>
      <tr>
        <td id="L260" class="blob-num js-line-number" data-line-number="260"></td>
        <td id="LC260" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L261" class="blob-num js-line-number" data-line-number="261"></td>
        <td id="LC261" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">isset</span>(<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimeTypeAlias</span>[<span class="pl-smi">$mimetype</span>])) {</span></td>
      </tr>
      <tr>
        <td id="L262" class="blob-num js-line-number" data-line-number="262"></td>
        <td id="LC262" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$mimetype</span> <span class="pl-k">=</span> <span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimeTypeAlias</span>[<span class="pl-smi">$mimetype</span>];</span></td>
      </tr>
      <tr>
        <td id="L263" class="blob-num js-line-number" data-line-number="263"></td>
        <td id="LC263" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L264" class="blob-num js-line-number" data-line-number="264"></td>
        <td id="LC264" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">isset</span>(<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeIcons</span>[<span class="pl-smi">$mimetype</span>])) {</span></td>
      </tr>
      <tr>
        <td id="L265" class="blob-num js-line-number" data-line-number="265"></td>
        <td id="LC265" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeIcons</span>[<span class="pl-smi">$mimetype</span>];</span></td>
      </tr>
      <tr>
        <td id="L266" class="blob-num js-line-number" data-line-number="266"></td>
        <td id="LC266" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L267" class="blob-num js-line-number" data-line-number="267"></td>
        <td id="LC267" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// Replace slash and backslash with a minus</span></span></td>
      </tr>
      <tr>
        <td id="L268" class="blob-num js-line-number" data-line-number="268"></td>
        <td id="LC268" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$icon</span> <span class="pl-k">=</span> <span class="pl-c1">str_replace</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>/<span class="pl-pds">&#39;</span></span>, <span class="pl-s"><span class="pl-pds">&#39;</span>-<span class="pl-pds">&#39;</span></span>, <span class="pl-smi">$mimetype</span>);</span></td>
      </tr>
      <tr>
        <td id="L269" class="blob-num js-line-number" data-line-number="269"></td>
        <td id="LC269" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$icon</span> <span class="pl-k">=</span> <span class="pl-c1">str_replace</span>(<span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-cce">\\</span><span class="pl-pds">&#39;</span></span>, <span class="pl-s"><span class="pl-pds">&#39;</span>-<span class="pl-pds">&#39;</span></span>, <span class="pl-smi">$icon</span>);</span></td>
      </tr>
      <tr>
        <td id="L270" class="blob-num js-line-number" data-line-number="270"></td>
        <td id="LC270" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L271" class="blob-num js-line-number" data-line-number="271"></td>
        <td id="LC271" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// Is it a dir?</span></span></td>
      </tr>
      <tr>
        <td id="L272" class="blob-num js-line-number" data-line-number="272"></td>
        <td id="LC272" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$mimetype</span> <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">&#39;</span>dir<span class="pl-pds">&#39;</span></span>) {</span></td>
      </tr>
      <tr>
        <td id="L273" class="blob-num js-line-number" data-line-number="273"></td>
        <td id="LC273" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeIcons</span>[<span class="pl-smi">$mimetype</span>] <span class="pl-k">=</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/folder.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L274" class="blob-num js-line-number" data-line-number="274"></td>
        <td id="LC274" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/folder.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L275" class="blob-num js-line-number" data-line-number="275"></td>
        <td id="LC275" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L276" class="blob-num js-line-number" data-line-number="276"></td>
        <td id="LC276" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$mimetype</span> <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">&#39;</span>dir-shared<span class="pl-pds">&#39;</span></span>) {</span></td>
      </tr>
      <tr>
        <td id="L277" class="blob-num js-line-number" data-line-number="277"></td>
        <td id="LC277" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeIcons</span>[<span class="pl-smi">$mimetype</span>] <span class="pl-k">=</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/folder-shared.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L278" class="blob-num js-line-number" data-line-number="278"></td>
        <td id="LC278" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/folder-shared.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L279" class="blob-num js-line-number" data-line-number="279"></td>
        <td id="LC279" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L280" class="blob-num js-line-number" data-line-number="280"></td>
        <td id="LC280" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$mimetype</span> <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">&#39;</span>dir-external<span class="pl-pds">&#39;</span></span>) {</span></td>
      </tr>
      <tr>
        <td id="L281" class="blob-num js-line-number" data-line-number="281"></td>
        <td id="LC281" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeIcons</span>[<span class="pl-smi">$mimetype</span>] <span class="pl-k">=</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/folder-external.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L282" class="blob-num js-line-number" data-line-number="282"></td>
        <td id="LC282" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/folder-external.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L283" class="blob-num js-line-number" data-line-number="283"></td>
        <td id="LC283" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L284" class="blob-num js-line-number" data-line-number="284"></td>
        <td id="LC284" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L285" class="blob-num js-line-number" data-line-number="285"></td>
        <td id="LC285" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// Icon exists?</span></span></td>
      </tr>
      <tr>
        <td id="L286" class="blob-num js-line-number" data-line-number="286"></td>
        <td id="LC286" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">file_exists</span>(<span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$SERVERROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$icon</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>.png<span class="pl-pds">&#39;</span></span>)) {</span></td>
      </tr>
      <tr>
        <td id="L287" class="blob-num js-line-number" data-line-number="287"></td>
        <td id="LC287" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeIcons</span>[<span class="pl-smi">$mimetype</span>] <span class="pl-k">=</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$icon</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L288" class="blob-num js-line-number" data-line-number="288"></td>
        <td id="LC288" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$icon</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L289" class="blob-num js-line-number" data-line-number="289"></td>
        <td id="LC289" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L290" class="blob-num js-line-number" data-line-number="290"></td>
        <td id="LC290" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L291" class="blob-num js-line-number" data-line-number="291"></td>
        <td id="LC291" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// Try only the first part of the filetype</span></span></td>
      </tr>
      <tr>
        <td id="L292" class="blob-num js-line-number" data-line-number="292"></td>
        <td id="LC292" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$mimePart</span> <span class="pl-k">=</span> <span class="pl-c1">substr</span>(<span class="pl-smi">$icon</span>, <span class="pl-c1">0</span>, <span class="pl-c1">strpos</span>(<span class="pl-smi">$icon</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>-<span class="pl-pds">&#39;</span></span>));</span></td>
      </tr>
      <tr>
        <td id="L293" class="blob-num js-line-number" data-line-number="293"></td>
        <td id="LC293" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">file_exists</span>(<span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$SERVERROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$mimePart</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>.png<span class="pl-pds">&#39;</span></span>)) {</span></td>
      </tr>
      <tr>
        <td id="L294" class="blob-num js-line-number" data-line-number="294"></td>
        <td id="LC294" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeIcons</span>[<span class="pl-smi">$mimetype</span>] <span class="pl-k">=</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$mimePart</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L295" class="blob-num js-line-number" data-line-number="295"></td>
        <td id="LC295" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$mimePart</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L296" class="blob-num js-line-number" data-line-number="296"></td>
        <td id="LC296" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L297" class="blob-num js-line-number" data-line-number="297"></td>
        <td id="LC297" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeIcons</span>[<span class="pl-smi">$mimetype</span>] <span class="pl-k">=</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/file.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L298" class="blob-num js-line-number" data-line-number="298"></td>
        <td id="LC298" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$WEBROOT</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/core/img/filetypes/file.png<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L299" class="blob-num js-line-number" data-line-number="299"></td>
        <td id="LC299" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L300" class="blob-num js-line-number" data-line-number="300"></td>
        <td id="LC300" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L301" class="blob-num js-line-number" data-line-number="301"></td>
        <td id="LC301" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L302" class="blob-num js-line-number" data-line-number="302"></td>
        <td id="LC302" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L303" class="blob-num js-line-number" data-line-number="303"></td>
        <td id="LC303" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * get path to preview of file</span></span></td>
      </tr>
      <tr>
        <td id="L304" class="blob-num js-line-number" data-line-number="304"></td>
        <td id="LC304" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $path path</span></span></td>
      </tr>
      <tr>
        <td id="L305" class="blob-num js-line-number" data-line-number="305"></td>
        <td id="LC305" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the url</span></span></td>
      </tr>
      <tr>
        <td id="L306" class="blob-num js-line-number" data-line-number="306"></td>
        <td id="LC306" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L307" class="blob-num js-line-number" data-line-number="307"></td>
        <td id="LC307" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns the path to the preview of the file.</span></span></td>
      </tr>
      <tr>
        <td id="L308" class="blob-num js-line-number" data-line-number="308"></td>
        <td id="LC308" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L309" class="blob-num js-line-number" data-line-number="309"></td>
        <td id="LC309" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">previewIcon</span>(<span class="pl-smi">$path</span>) {</span></td>
      </tr>
      <tr>
        <td id="L310" class="blob-num js-line-number" data-line-number="310"></td>
        <td id="LC310" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span>linkToRoute( <span class="pl-s"><span class="pl-pds">&#39;</span>core_ajax_preview<span class="pl-pds">&#39;</span></span>, <span class="pl-c1">array</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>x<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">36</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>y<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">36</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>file<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$path</span> ));</span></td>
      </tr>
      <tr>
        <td id="L311" class="blob-num js-line-number" data-line-number="311"></td>
        <td id="LC311" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L312" class="blob-num js-line-number" data-line-number="312"></td>
        <td id="LC312" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L313" class="blob-num js-line-number" data-line-number="313"></td>
        <td id="LC313" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">publicPreviewIcon</span>( <span class="pl-smi">$path</span>, <span class="pl-smi">$token</span> ) {</span></td>
      </tr>
      <tr>
        <td id="L314" class="blob-num js-line-number" data-line-number="314"></td>
        <td id="LC314" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span>linkToRoute( <span class="pl-s"><span class="pl-pds">&#39;</span>core_ajax_public_preview<span class="pl-pds">&#39;</span></span>, <span class="pl-c1">array</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>x<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">36</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>y<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">36</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>file<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$path</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>t<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$token</span>));</span></td>
      </tr>
      <tr>
        <td id="L315" class="blob-num js-line-number" data-line-number="315"></td>
        <td id="LC315" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L316" class="blob-num js-line-number" data-line-number="316"></td>
        <td id="LC316" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L317" class="blob-num js-line-number" data-line-number="317"></td>
        <td id="LC317" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L318" class="blob-num js-line-number" data-line-number="318"></td>
        <td id="LC318" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * shows whether the user has an avatar</span></span></td>
      </tr>
      <tr>
        <td id="L319" class="blob-num js-line-number" data-line-number="319"></td>
        <td id="LC319" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $user username</span></span></td>
      </tr>
      <tr>
        <td id="L320" class="blob-num js-line-number" data-line-number="320"></td>
        <td id="LC320" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> bool avatar set or not</span></span></td>
      </tr>
      <tr>
        <td id="L321" class="blob-num js-line-number" data-line-number="321"></td>
        <td id="LC321" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	**/</span></span></td>
      </tr>
      <tr>
        <td id="L322" class="blob-num js-line-number" data-line-number="322"></td>
        <td id="LC322" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">userAvatarSet</span>(<span class="pl-smi">$user</span>) {</span></td>
      </tr>
      <tr>
        <td id="L323" class="blob-num js-line-number" data-line-number="323"></td>
        <td id="LC323" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$avatar</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">\OC\</span><span class="pl-c1">Avatar</span>(<span class="pl-smi">$user</span>);</span></td>
      </tr>
      <tr>
        <td id="L324" class="blob-num js-line-number" data-line-number="324"></td>
        <td id="LC324" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-smi">$avatar</span><span class="pl-k">-&gt;</span>exists();</span></td>
      </tr>
      <tr>
        <td id="L325" class="blob-num js-line-number" data-line-number="325"></td>
        <td id="LC325" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L326" class="blob-num js-line-number" data-line-number="326"></td>
        <td id="LC326" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L327" class="blob-num js-line-number" data-line-number="327"></td>
        <td id="LC327" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L328" class="blob-num js-line-number" data-line-number="328"></td>
        <td id="LC328" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Make a human file size</span></span></td>
      </tr>
      <tr>
        <td id="L329" class="blob-num js-line-number" data-line-number="329"></td>
        <td id="LC329" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> int $bytes file size in bytes</span></span></td>
      </tr>
      <tr>
        <td id="L330" class="blob-num js-line-number" data-line-number="330"></td>
        <td id="LC330" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string a human readable file size</span></span></td>
      </tr>
      <tr>
        <td id="L331" class="blob-num js-line-number" data-line-number="331"></td>
        <td id="LC331" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L332" class="blob-num js-line-number" data-line-number="332"></td>
        <td id="LC332" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Makes 2048 to 2 kB.</span></span></td>
      </tr>
      <tr>
        <td id="L333" class="blob-num js-line-number" data-line-number="333"></td>
        <td id="LC333" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L334" class="blob-num js-line-number" data-line-number="334"></td>
        <td id="LC334" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">humanFileSize</span>(<span class="pl-smi">$bytes</span>) {</span></td>
      </tr>
      <tr>
        <td id="L335" class="blob-num js-line-number" data-line-number="335"></td>
        <td id="LC335" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$bytes</span> <span class="pl-k">&lt;</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L336" class="blob-num js-line-number" data-line-number="336"></td>
        <td id="LC336" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-s"><span class="pl-pds">&quot;</span>?<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L337" class="blob-num js-line-number" data-line-number="337"></td>
        <td id="LC337" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L338" class="blob-num js-line-number" data-line-number="338"></td>
        <td id="LC338" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$bytes</span> <span class="pl-k">&lt;</span> <span class="pl-c1">1024</span>) {</span></td>
      </tr>
      <tr>
        <td id="L339" class="blob-num js-line-number" data-line-number="339"></td>
        <td id="LC339" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-smi">$bytes</span> B<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L340" class="blob-num js-line-number" data-line-number="340"></td>
        <td id="LC340" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L341" class="blob-num js-line-number" data-line-number="341"></td>
        <td id="LC341" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>(<span class="pl-smi">$bytes</span> <span class="pl-k">/</span> <span class="pl-c1">1024</span>, <span class="pl-c1">0</span>);</span></td>
      </tr>
      <tr>
        <td id="L342" class="blob-num js-line-number" data-line-number="342"></td>
        <td id="LC342" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$bytes</span> <span class="pl-k">&lt;</span> <span class="pl-c1">1024</span>) {</span></td>
      </tr>
      <tr>
        <td id="L343" class="blob-num js-line-number" data-line-number="343"></td>
        <td id="LC343" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-smi">$bytes</span> kB<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L344" class="blob-num js-line-number" data-line-number="344"></td>
        <td id="LC344" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L345" class="blob-num js-line-number" data-line-number="345"></td>
        <td id="LC345" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>(<span class="pl-smi">$bytes</span> <span class="pl-k">/</span> <span class="pl-c1">1024</span>, <span class="pl-c1">1</span>);</span></td>
      </tr>
      <tr>
        <td id="L346" class="blob-num js-line-number" data-line-number="346"></td>
        <td id="LC346" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$bytes</span> <span class="pl-k">&lt;</span> <span class="pl-c1">1024</span>) {</span></td>
      </tr>
      <tr>
        <td id="L347" class="blob-num js-line-number" data-line-number="347"></td>
        <td id="LC347" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-smi">$bytes</span> MB<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L348" class="blob-num js-line-number" data-line-number="348"></td>
        <td id="LC348" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L349" class="blob-num js-line-number" data-line-number="349"></td>
        <td id="LC349" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>(<span class="pl-smi">$bytes</span> <span class="pl-k">/</span> <span class="pl-c1">1024</span>, <span class="pl-c1">1</span>);</span></td>
      </tr>
      <tr>
        <td id="L350" class="blob-num js-line-number" data-line-number="350"></td>
        <td id="LC350" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$bytes</span> <span class="pl-k">&lt;</span> <span class="pl-c1">1024</span>) {</span></td>
      </tr>
      <tr>
        <td id="L351" class="blob-num js-line-number" data-line-number="351"></td>
        <td id="LC351" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-smi">$bytes</span> GB<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L352" class="blob-num js-line-number" data-line-number="352"></td>
        <td id="LC352" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L353" class="blob-num js-line-number" data-line-number="353"></td>
        <td id="LC353" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>(<span class="pl-smi">$bytes</span> <span class="pl-k">/</span> <span class="pl-c1">1024</span>, <span class="pl-c1">1</span>);</span></td>
      </tr>
      <tr>
        <td id="L354" class="blob-num js-line-number" data-line-number="354"></td>
        <td id="LC354" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$bytes</span> <span class="pl-k">&lt;</span> <span class="pl-c1">1024</span>) {</span></td>
      </tr>
      <tr>
        <td id="L355" class="blob-num js-line-number" data-line-number="355"></td>
        <td id="LC355" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-smi">$bytes</span> TB<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L356" class="blob-num js-line-number" data-line-number="356"></td>
        <td id="LC356" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L357" class="blob-num js-line-number" data-line-number="357"></td>
        <td id="LC357" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L358" class="blob-num js-line-number" data-line-number="358"></td>
        <td id="LC358" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>(<span class="pl-smi">$bytes</span> <span class="pl-k">/</span> <span class="pl-c1">1024</span>, <span class="pl-c1">1</span>);</span></td>
      </tr>
      <tr>
        <td id="L359" class="blob-num js-line-number" data-line-number="359"></td>
        <td id="LC359" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-smi">$bytes</span> PB<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L360" class="blob-num js-line-number" data-line-number="360"></td>
        <td id="LC360" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L361" class="blob-num js-line-number" data-line-number="361"></td>
        <td id="LC361" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L362" class="blob-num js-line-number" data-line-number="362"></td>
        <td id="LC362" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L363" class="blob-num js-line-number" data-line-number="363"></td>
        <td id="LC363" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Make a php file size</span></span></td>
      </tr>
      <tr>
        <td id="L364" class="blob-num js-line-number" data-line-number="364"></td>
        <td id="LC364" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> int $bytes file size in bytes</span></span></td>
      </tr>
      <tr>
        <td id="L365" class="blob-num js-line-number" data-line-number="365"></td>
        <td id="LC365" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string a php parseable file size</span></span></td>
      </tr>
      <tr>
        <td id="L366" class="blob-num js-line-number" data-line-number="366"></td>
        <td id="LC366" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L367" class="blob-num js-line-number" data-line-number="367"></td>
        <td id="LC367" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Makes 2048 to 2k and 2^41 to 2048G</span></span></td>
      </tr>
      <tr>
        <td id="L368" class="blob-num js-line-number" data-line-number="368"></td>
        <td id="LC368" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L369" class="blob-num js-line-number" data-line-number="369"></td>
        <td id="LC369" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">phpFileSize</span>(<span class="pl-smi">$bytes</span>) {</span></td>
      </tr>
      <tr>
        <td id="L370" class="blob-num js-line-number" data-line-number="370"></td>
        <td id="LC370" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$bytes</span> <span class="pl-k">&lt;</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L371" class="blob-num js-line-number" data-line-number="371"></td>
        <td id="LC371" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-s"><span class="pl-pds">&quot;</span>?<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L372" class="blob-num js-line-number" data-line-number="372"></td>
        <td id="LC372" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L373" class="blob-num js-line-number" data-line-number="373"></td>
        <td id="LC373" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$bytes</span> <span class="pl-k">&lt;</span> <span class="pl-c1">1024</span>) {</span></td>
      </tr>
      <tr>
        <td id="L374" class="blob-num js-line-number" data-line-number="374"></td>
        <td id="LC374" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-smi">$bytes</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&quot;</span>B<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L375" class="blob-num js-line-number" data-line-number="375"></td>
        <td id="LC375" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L376" class="blob-num js-line-number" data-line-number="376"></td>
        <td id="LC376" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>(<span class="pl-smi">$bytes</span> <span class="pl-k">/</span> <span class="pl-c1">1024</span>, <span class="pl-c1">1</span>);</span></td>
      </tr>
      <tr>
        <td id="L377" class="blob-num js-line-number" data-line-number="377"></td>
        <td id="LC377" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$bytes</span> <span class="pl-k">&lt;</span> <span class="pl-c1">1024</span>) {</span></td>
      </tr>
      <tr>
        <td id="L378" class="blob-num js-line-number" data-line-number="378"></td>
        <td id="LC378" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-smi">$bytes</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&quot;</span>K<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L379" class="blob-num js-line-number" data-line-number="379"></td>
        <td id="LC379" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L380" class="blob-num js-line-number" data-line-number="380"></td>
        <td id="LC380" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>(<span class="pl-smi">$bytes</span> <span class="pl-k">/</span> <span class="pl-c1">1024</span>, <span class="pl-c1">1</span>);</span></td>
      </tr>
      <tr>
        <td id="L381" class="blob-num js-line-number" data-line-number="381"></td>
        <td id="LC381" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$bytes</span> <span class="pl-k">&lt;</span> <span class="pl-c1">1024</span>) {</span></td>
      </tr>
      <tr>
        <td id="L382" class="blob-num js-line-number" data-line-number="382"></td>
        <td id="LC382" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-smi">$bytes</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&quot;</span>M<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L383" class="blob-num js-line-number" data-line-number="383"></td>
        <td id="LC383" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L384" class="blob-num js-line-number" data-line-number="384"></td>
        <td id="LC384" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>(<span class="pl-smi">$bytes</span> <span class="pl-k">/</span> <span class="pl-c1">1024</span>, <span class="pl-c1">1</span>);</span></td>
      </tr>
      <tr>
        <td id="L385" class="blob-num js-line-number" data-line-number="385"></td>
        <td id="LC385" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-smi">$bytes</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&quot;</span>G<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L386" class="blob-num js-line-number" data-line-number="386"></td>
        <td id="LC386" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L387" class="blob-num js-line-number" data-line-number="387"></td>
        <td id="LC387" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L388" class="blob-num js-line-number" data-line-number="388"></td>
        <td id="LC388" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L389" class="blob-num js-line-number" data-line-number="389"></td>
        <td id="LC389" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Make a computer file size</span></span></td>
      </tr>
      <tr>
        <td id="L390" class="blob-num js-line-number" data-line-number="390"></td>
        <td id="LC390" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $str file size in human readable format</span></span></td>
      </tr>
      <tr>
        <td id="L391" class="blob-num js-line-number" data-line-number="391"></td>
        <td id="LC391" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> int a file size in bytes</span></span></td>
      </tr>
      <tr>
        <td id="L392" class="blob-num js-line-number" data-line-number="392"></td>
        <td id="LC392" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L393" class="blob-num js-line-number" data-line-number="393"></td>
        <td id="LC393" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Makes 2kB to 2048.</span></span></td>
      </tr>
      <tr>
        <td id="L394" class="blob-num js-line-number" data-line-number="394"></td>
        <td id="LC394" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L395" class="blob-num js-line-number" data-line-number="395"></td>
        <td id="LC395" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Inspired by: http://www.php.net/manual/en/function.filesize.php#92418</span></span></td>
      </tr>
      <tr>
        <td id="L396" class="blob-num js-line-number" data-line-number="396"></td>
        <td id="LC396" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L397" class="blob-num js-line-number" data-line-number="397"></td>
        <td id="LC397" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">computerFileSize</span>(<span class="pl-smi">$str</span>) {</span></td>
      </tr>
      <tr>
        <td id="L398" class="blob-num js-line-number" data-line-number="398"></td>
        <td id="LC398" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$str</span> <span class="pl-k">=</span> <span class="pl-c1">strtolower</span>(<span class="pl-smi">$str</span>);</span></td>
      </tr>
      <tr>
        <td id="L399" class="blob-num js-line-number" data-line-number="399"></td>
        <td id="LC399" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L400" class="blob-num js-line-number" data-line-number="400"></td>
        <td id="LC400" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes_array</span> <span class="pl-k">=</span> <span class="pl-c1">array</span>(</span></td>
      </tr>
      <tr>
        <td id="L401" class="blob-num js-line-number" data-line-number="401"></td>
        <td id="LC401" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>b<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1</span>,</span></td>
      </tr>
      <tr>
        <td id="L402" class="blob-num js-line-number" data-line-number="402"></td>
        <td id="LC402" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>k<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1024</span>,</span></td>
      </tr>
      <tr>
        <td id="L403" class="blob-num js-line-number" data-line-number="403"></td>
        <td id="LC403" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>kb<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1024</span>,</span></td>
      </tr>
      <tr>
        <td id="L404" class="blob-num js-line-number" data-line-number="404"></td>
        <td id="LC404" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>mb<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span>,</span></td>
      </tr>
      <tr>
        <td id="L405" class="blob-num js-line-number" data-line-number="405"></td>
        <td id="LC405" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>m<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span>,</span></td>
      </tr>
      <tr>
        <td id="L406" class="blob-num js-line-number" data-line-number="406"></td>
        <td id="LC406" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>gb<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span>,</span></td>
      </tr>
      <tr>
        <td id="L407" class="blob-num js-line-number" data-line-number="407"></td>
        <td id="LC407" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>g<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span>,</span></td>
      </tr>
      <tr>
        <td id="L408" class="blob-num js-line-number" data-line-number="408"></td>
        <td id="LC408" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>tb<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span>,</span></td>
      </tr>
      <tr>
        <td id="L409" class="blob-num js-line-number" data-line-number="409"></td>
        <td id="LC409" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>t<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span>,</span></td>
      </tr>
      <tr>
        <td id="L410" class="blob-num js-line-number" data-line-number="410"></td>
        <td id="LC410" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>pb<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span>,</span></td>
      </tr>
      <tr>
        <td id="L411" class="blob-num js-line-number" data-line-number="411"></td>
        <td id="LC411" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-s"><span class="pl-pds">&#39;</span>p<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span> <span class="pl-k">*</span> <span class="pl-c1">1024</span>,</span></td>
      </tr>
      <tr>
        <td id="L412" class="blob-num js-line-number" data-line-number="412"></td>
        <td id="LC412" class="blob-code js-file-line"><span class="pl-s1">		);</span></td>
      </tr>
      <tr>
        <td id="L413" class="blob-num js-line-number" data-line-number="413"></td>
        <td id="LC413" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L414" class="blob-num js-line-number" data-line-number="414"></td>
        <td id="LC414" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes</span> <span class="pl-k">=</span> <span class="pl-c1">floatval</span>(<span class="pl-smi">$str</span>);</span></td>
      </tr>
      <tr>
        <td id="L415" class="blob-num js-line-number" data-line-number="415"></td>
        <td id="LC415" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L416" class="blob-num js-line-number" data-line-number="416"></td>
        <td id="LC416" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">preg_match</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>#([kmgtp]?b?)$#si<span class="pl-pds">&#39;</span></span>, <span class="pl-smi">$str</span>, <span class="pl-smi">$matches</span>) <span class="pl-k">&amp;&amp;</span> <span class="pl-k">!</span><span class="pl-c1">empty</span>(<span class="pl-smi">$bytes_array</span>[<span class="pl-smi">$matches</span>[<span class="pl-c1">1</span>]])) {</span></td>
      </tr>
      <tr>
        <td id="L417" class="blob-num js-line-number" data-line-number="417"></td>
        <td id="LC417" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$bytes</span> <span class="pl-k">*</span><span class="pl-k">=</span> <span class="pl-smi">$bytes_array</span>[<span class="pl-smi">$matches</span>[<span class="pl-c1">1</span>]];</span></td>
      </tr>
      <tr>
        <td id="L418" class="blob-num js-line-number" data-line-number="418"></td>
        <td id="LC418" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L419" class="blob-num js-line-number" data-line-number="419"></td>
        <td id="LC419" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L420" class="blob-num js-line-number" data-line-number="420"></td>
        <td id="LC420" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bytes</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>(<span class="pl-smi">$bytes</span>);</span></td>
      </tr>
      <tr>
        <td id="L421" class="blob-num js-line-number" data-line-number="421"></td>
        <td id="LC421" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L422" class="blob-num js-line-number" data-line-number="422"></td>
        <td id="LC422" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-smi">$bytes</span>;</span></td>
      </tr>
      <tr>
        <td id="L423" class="blob-num js-line-number" data-line-number="423"></td>
        <td id="LC423" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L424" class="blob-num js-line-number" data-line-number="424"></td>
        <td id="LC424" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L425" class="blob-num js-line-number" data-line-number="425"></td>
        <td id="LC425" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L426" class="blob-num js-line-number" data-line-number="426"></td>
        <td id="LC426" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Recursive copying of folders</span></span></td>
      </tr>
      <tr>
        <td id="L427" class="blob-num js-line-number" data-line-number="427"></td>
        <td id="LC427" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $src source folder</span></span></td>
      </tr>
      <tr>
        <td id="L428" class="blob-num js-line-number" data-line-number="428"></td>
        <td id="LC428" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $dest target folder</span></span></td>
      </tr>
      <tr>
        <td id="L429" class="blob-num js-line-number" data-line-number="429"></td>
        <td id="LC429" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L430" class="blob-num js-line-number" data-line-number="430"></td>
        <td id="LC430" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L431" class="blob-num js-line-number" data-line-number="431"></td>
        <td id="LC431" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">copyr</span>(<span class="pl-smi">$src</span>, <span class="pl-smi">$dest</span>) {</span></td>
      </tr>
      <tr>
        <td id="L432" class="blob-num js-line-number" data-line-number="432"></td>
        <td id="LC432" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">is_dir</span>(<span class="pl-smi">$src</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L433" class="blob-num js-line-number" data-line-number="433"></td>
        <td id="LC433" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-c1">is_dir</span>(<span class="pl-smi">$dest</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L434" class="blob-num js-line-number" data-line-number="434"></td>
        <td id="LC434" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c1">mkdir</span>(<span class="pl-smi">$dest</span>);</span></td>
      </tr>
      <tr>
        <td id="L435" class="blob-num js-line-number" data-line-number="435"></td>
        <td id="LC435" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L436" class="blob-num js-line-number" data-line-number="436"></td>
        <td id="LC436" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$files</span> <span class="pl-k">=</span> <span class="pl-c1">scandir</span>(<span class="pl-smi">$src</span>);</span></td>
      </tr>
      <tr>
        <td id="L437" class="blob-num js-line-number" data-line-number="437"></td>
        <td id="LC437" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">foreach</span> (<span class="pl-smi">$files</span> <span class="pl-k">as</span> <span class="pl-smi">$file</span>) {</span></td>
      </tr>
      <tr>
        <td id="L438" class="blob-num js-line-number" data-line-number="438"></td>
        <td id="LC438" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">if</span> (<span class="pl-smi">$file</span> <span class="pl-k">!</span><span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span>.<span class="pl-pds">&quot;</span></span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">$file</span> <span class="pl-k">!</span><span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span>..<span class="pl-pds">&quot;</span></span>) {</span></td>
      </tr>
      <tr>
        <td id="L439" class="blob-num js-line-number" data-line-number="439"></td>
        <td id="LC439" class="blob-code js-file-line"><span class="pl-s1">					<span class="pl-k">self</span><span class="pl-k">::</span>copyr(<span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-smi">$src</span>/<span class="pl-smi">$file</span><span class="pl-pds">&quot;</span></span>, <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-smi">$dest</span>/<span class="pl-smi">$file</span><span class="pl-pds">&quot;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L440" class="blob-num js-line-number" data-line-number="440"></td>
        <td id="LC440" class="blob-code js-file-line"><span class="pl-s1">				}</span></td>
      </tr>
      <tr>
        <td id="L441" class="blob-num js-line-number" data-line-number="441"></td>
        <td id="LC441" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L442" class="blob-num js-line-number" data-line-number="442"></td>
        <td id="LC442" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">elseif</span> (<span class="pl-c1">file_exists</span>(<span class="pl-smi">$src</span>) <span class="pl-k">&amp;&amp;</span> <span class="pl-k">!</span><span class="pl-c1">\OC\Files\</span><span class="pl-c1">Filesystem</span><span class="pl-k">::</span>isFileBlacklisted(<span class="pl-smi">$src</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L443" class="blob-num js-line-number" data-line-number="443"></td>
        <td id="LC443" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-c1">copy</span>(<span class="pl-smi">$src</span>, <span class="pl-smi">$dest</span>);</span></td>
      </tr>
      <tr>
        <td id="L444" class="blob-num js-line-number" data-line-number="444"></td>
        <td id="LC444" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L445" class="blob-num js-line-number" data-line-number="445"></td>
        <td id="LC445" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L446" class="blob-num js-line-number" data-line-number="446"></td>
        <td id="LC446" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L447" class="blob-num js-line-number" data-line-number="447"></td>
        <td id="LC447" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L448" class="blob-num js-line-number" data-line-number="448"></td>
        <td id="LC448" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Recursive deletion of folders</span></span></td>
      </tr>
      <tr>
        <td id="L449" class="blob-num js-line-number" data-line-number="449"></td>
        <td id="LC449" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $dir path to the folder</span></span></td>
      </tr>
      <tr>
        <td id="L450" class="blob-num js-line-number" data-line-number="450"></td>
        <td id="LC450" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> bool $deleteSelf if set to false only the content of the folder will be deleted</span></span></td>
      </tr>
      <tr>
        <td id="L451" class="blob-num js-line-number" data-line-number="451"></td>
        <td id="LC451" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> bool</span></span></td>
      </tr>
      <tr>
        <td id="L452" class="blob-num js-line-number" data-line-number="452"></td>
        <td id="LC452" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L453" class="blob-num js-line-number" data-line-number="453"></td>
        <td id="LC453" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">rmdirr</span>(<span class="pl-smi">$dir</span>, <span class="pl-smi">$deleteSelf</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>) {</span></td>
      </tr>
      <tr>
        <td id="L454" class="blob-num js-line-number" data-line-number="454"></td>
        <td id="LC454" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">is_dir</span>(<span class="pl-smi">$dir</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L455" class="blob-num js-line-number" data-line-number="455"></td>
        <td id="LC455" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$files</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">RecursiveIteratorIterator</span>(</span></td>
      </tr>
      <tr>
        <td id="L456" class="blob-num js-line-number" data-line-number="456"></td>
        <td id="LC456" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">new</span> <span class="pl-c1">RecursiveDirectoryIterator</span>(<span class="pl-smi">$dir</span>, <span class="pl-c1">RecursiveDirectoryIterator</span><span class="pl-k">::</span><span class="pl-c1">SKIP_DOTS</span>),</span></td>
      </tr>
      <tr>
        <td id="L457" class="blob-num js-line-number" data-line-number="457"></td>
        <td id="LC457" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c1">RecursiveIteratorIterator</span><span class="pl-k">::</span><span class="pl-c1">CHILD_FIRST</span></span></td>
      </tr>
      <tr>
        <td id="L458" class="blob-num js-line-number" data-line-number="458"></td>
        <td id="LC458" class="blob-code js-file-line"><span class="pl-s1">			);</span></td>
      </tr>
      <tr>
        <td id="L459" class="blob-num js-line-number" data-line-number="459"></td>
        <td id="LC459" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L460" class="blob-num js-line-number" data-line-number="460"></td>
        <td id="LC460" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">foreach</span> (<span class="pl-smi">$files</span> <span class="pl-k">as</span> <span class="pl-smi">$fileInfo</span>) {</span></td>
      </tr>
      <tr>
        <td id="L461" class="blob-num js-line-number" data-line-number="461"></td>
        <td id="LC461" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c">/** @var SplFileInfo $fileInfo */</span></span></td>
      </tr>
      <tr>
        <td id="L462" class="blob-num js-line-number" data-line-number="462"></td>
        <td id="LC462" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">if</span> (<span class="pl-smi">$fileInfo</span><span class="pl-k">-&gt;</span>isDir()) {</span></td>
      </tr>
      <tr>
        <td id="L463" class="blob-num js-line-number" data-line-number="463"></td>
        <td id="LC463" class="blob-code js-file-line"><span class="pl-s1">					<span class="pl-c1">rmdir</span>(<span class="pl-smi">$fileInfo</span><span class="pl-k">-&gt;</span>getRealPath());</span></td>
      </tr>
      <tr>
        <td id="L464" class="blob-num js-line-number" data-line-number="464"></td>
        <td id="LC464" class="blob-code js-file-line"><span class="pl-s1">				} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L465" class="blob-num js-line-number" data-line-number="465"></td>
        <td id="LC465" class="blob-code js-file-line"><span class="pl-s1">					<span class="pl-c1">unlink</span>(<span class="pl-smi">$fileInfo</span><span class="pl-k">-&gt;</span>getRealPath());</span></td>
      </tr>
      <tr>
        <td id="L466" class="blob-num js-line-number" data-line-number="466"></td>
        <td id="LC466" class="blob-code js-file-line"><span class="pl-s1">				}</span></td>
      </tr>
      <tr>
        <td id="L467" class="blob-num js-line-number" data-line-number="467"></td>
        <td id="LC467" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L468" class="blob-num js-line-number" data-line-number="468"></td>
        <td id="LC468" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-smi">$deleteSelf</span>) {</span></td>
      </tr>
      <tr>
        <td id="L469" class="blob-num js-line-number" data-line-number="469"></td>
        <td id="LC469" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c1">rmdir</span>(<span class="pl-smi">$dir</span>);</span></td>
      </tr>
      <tr>
        <td id="L470" class="blob-num js-line-number" data-line-number="470"></td>
        <td id="LC470" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L471" class="blob-num js-line-number" data-line-number="471"></td>
        <td id="LC471" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">elseif</span> (<span class="pl-c1">file_exists</span>(<span class="pl-smi">$dir</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L472" class="blob-num js-line-number" data-line-number="472"></td>
        <td id="LC472" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-smi">$deleteSelf</span>) {</span></td>
      </tr>
      <tr>
        <td id="L473" class="blob-num js-line-number" data-line-number="473"></td>
        <td id="LC473" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c1">unlink</span>(<span class="pl-smi">$dir</span>);</span></td>
      </tr>
      <tr>
        <td id="L474" class="blob-num js-line-number" data-line-number="474"></td>
        <td id="LC474" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L475" class="blob-num js-line-number" data-line-number="475"></td>
        <td id="LC475" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L476" class="blob-num js-line-number" data-line-number="476"></td>
        <td id="LC476" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">$deleteSelf</span>) {</span></td>
      </tr>
      <tr>
        <td id="L477" class="blob-num js-line-number" data-line-number="477"></td>
        <td id="LC477" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">true</span>;</span></td>
      </tr>
      <tr>
        <td id="L478" class="blob-num js-line-number" data-line-number="478"></td>
        <td id="LC478" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L479" class="blob-num js-line-number" data-line-number="479"></td>
        <td id="LC479" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L480" class="blob-num js-line-number" data-line-number="480"></td>
        <td id="LC480" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">!</span><span class="pl-c1">file_exists</span>(<span class="pl-smi">$dir</span>);</span></td>
      </tr>
      <tr>
        <td id="L481" class="blob-num js-line-number" data-line-number="481"></td>
        <td id="LC481" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L482" class="blob-num js-line-number" data-line-number="482"></td>
        <td id="LC482" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L483" class="blob-num js-line-number" data-line-number="483"></td>
        <td id="LC483" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L484" class="blob-num js-line-number" data-line-number="484"></td>
        <td id="LC484" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> \OC\Files\Type\Detection</span></span></td>
      </tr>
      <tr>
        <td id="L485" class="blob-num js-line-number" data-line-number="485"></td>
        <td id="LC485" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L486" class="blob-num js-line-number" data-line-number="486"></td>
        <td id="LC486" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">static</span> <span class="pl-k">public</span> <span class="pl-k">function</span> <span class="pl-en">getMimetypeDetector</span>() {</span></td>
      </tr>
      <tr>
        <td id="L487" class="blob-num js-line-number" data-line-number="487"></td>
        <td id="LC487" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeDetector</span>) {</span></td>
      </tr>
      <tr>
        <td id="L488" class="blob-num js-line-number" data-line-number="488"></td>
        <td id="LC488" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeDetector</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">\OC\Files\Type\</span><span class="pl-c1">Detection</span>();</span></td>
      </tr>
      <tr>
        <td id="L489" class="blob-num js-line-number" data-line-number="489"></td>
        <td id="LC489" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeDetector</span><span class="pl-k">-&gt;</span>registerTypeArray(<span class="pl-k">include</span> <span class="pl-s"><span class="pl-pds">&#39;</span>mimetypes.list.php<span class="pl-pds">&#39;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L490" class="blob-num js-line-number" data-line-number="490"></td>
        <td id="LC490" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L491" class="blob-num js-line-number" data-line-number="491"></td>
        <td id="LC491" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$mimetypeDetector</span>;</span></td>
      </tr>
      <tr>
        <td id="L492" class="blob-num js-line-number" data-line-number="492"></td>
        <td id="LC492" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L493" class="blob-num js-line-number" data-line-number="493"></td>
        <td id="LC493" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L494" class="blob-num js-line-number" data-line-number="494"></td>
        <td id="LC494" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L495" class="blob-num js-line-number" data-line-number="495"></td>
        <td id="LC495" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> \OC\Files\Type\TemplateManager</span></span></td>
      </tr>
      <tr>
        <td id="L496" class="blob-num js-line-number" data-line-number="496"></td>
        <td id="LC496" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L497" class="blob-num js-line-number" data-line-number="497"></td>
        <td id="LC497" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">static</span> <span class="pl-k">public</span> <span class="pl-k">function</span> <span class="pl-en">getFileTemplateManager</span>() {</span></td>
      </tr>
      <tr>
        <td id="L498" class="blob-num js-line-number" data-line-number="498"></td>
        <td id="LC498" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$templateManager</span>) {</span></td>
      </tr>
      <tr>
        <td id="L499" class="blob-num js-line-number" data-line-number="499"></td>
        <td id="LC499" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$templateManager</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">\OC\Files\Type\</span><span class="pl-c1">TemplateManager</span>();</span></td>
      </tr>
      <tr>
        <td id="L500" class="blob-num js-line-number" data-line-number="500"></td>
        <td id="LC500" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L501" class="blob-num js-line-number" data-line-number="501"></td>
        <td id="LC501" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span><span class="pl-smi">$templateManager</span>;</span></td>
      </tr>
      <tr>
        <td id="L502" class="blob-num js-line-number" data-line-number="502"></td>
        <td id="LC502" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L503" class="blob-num js-line-number" data-line-number="503"></td>
        <td id="LC503" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L504" class="blob-num js-line-number" data-line-number="504"></td>
        <td id="LC504" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L505" class="blob-num js-line-number" data-line-number="505"></td>
        <td id="LC505" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Try to guess the mimetype based on filename</span></span></td>
      </tr>
      <tr>
        <td id="L506" class="blob-num js-line-number" data-line-number="506"></td>
        <td id="LC506" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L507" class="blob-num js-line-number" data-line-number="507"></td>
        <td id="LC507" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $path</span></span></td>
      </tr>
      <tr>
        <td id="L508" class="blob-num js-line-number" data-line-number="508"></td>
        <td id="LC508" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string</span></span></td>
      </tr>
      <tr>
        <td id="L509" class="blob-num js-line-number" data-line-number="509"></td>
        <td id="LC509" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L510" class="blob-num js-line-number" data-line-number="510"></td>
        <td id="LC510" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">static</span> <span class="pl-k">public</span> <span class="pl-k">function</span> <span class="pl-en">getFileNameMimeType</span>(<span class="pl-smi">$path</span>) {</span></td>
      </tr>
      <tr>
        <td id="L511" class="blob-num js-line-number" data-line-number="511"></td>
        <td id="LC511" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span>getMimetypeDetector()<span class="pl-k">-&gt;</span>detectPath(<span class="pl-smi">$path</span>);</span></td>
      </tr>
      <tr>
        <td id="L512" class="blob-num js-line-number" data-line-number="512"></td>
        <td id="LC512" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L513" class="blob-num js-line-number" data-line-number="513"></td>
        <td id="LC513" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L514" class="blob-num js-line-number" data-line-number="514"></td>
        <td id="LC514" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L515" class="blob-num js-line-number" data-line-number="515"></td>
        <td id="LC515" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * get the mimetype form a local file</span></span></td>
      </tr>
      <tr>
        <td id="L516" class="blob-num js-line-number" data-line-number="516"></td>
        <td id="LC516" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L517" class="blob-num js-line-number" data-line-number="517"></td>
        <td id="LC517" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $path</span></span></td>
      </tr>
      <tr>
        <td id="L518" class="blob-num js-line-number" data-line-number="518"></td>
        <td id="LC518" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string</span></span></td>
      </tr>
      <tr>
        <td id="L519" class="blob-num js-line-number" data-line-number="519"></td>
        <td id="LC519" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * does NOT work for ownClouds filesystem, use OC_FileSystem::getMimeType instead</span></span></td>
      </tr>
      <tr>
        <td id="L520" class="blob-num js-line-number" data-line-number="520"></td>
        <td id="LC520" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L521" class="blob-num js-line-number" data-line-number="521"></td>
        <td id="LC521" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">getMimeType</span>(<span class="pl-smi">$path</span>) {</span></td>
      </tr>
      <tr>
        <td id="L522" class="blob-num js-line-number" data-line-number="522"></td>
        <td id="LC522" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span>getMimetypeDetector()<span class="pl-k">-&gt;</span>detect(<span class="pl-smi">$path</span>);</span></td>
      </tr>
      <tr>
        <td id="L523" class="blob-num js-line-number" data-line-number="523"></td>
        <td id="LC523" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L524" class="blob-num js-line-number" data-line-number="524"></td>
        <td id="LC524" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L525" class="blob-num js-line-number" data-line-number="525"></td>
        <td id="LC525" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L526" class="blob-num js-line-number" data-line-number="526"></td>
        <td id="LC526" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Get a secure mimetype that won&#39;t expose potential XSS.</span></span></td>
      </tr>
      <tr>
        <td id="L527" class="blob-num js-line-number" data-line-number="527"></td>
        <td id="LC527" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L528" class="blob-num js-line-number" data-line-number="528"></td>
        <td id="LC528" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $mimeType</span></span></td>
      </tr>
      <tr>
        <td id="L529" class="blob-num js-line-number" data-line-number="529"></td>
        <td id="LC529" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string</span></span></td>
      </tr>
      <tr>
        <td id="L530" class="blob-num js-line-number" data-line-number="530"></td>
        <td id="LC530" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L531" class="blob-num js-line-number" data-line-number="531"></td>
        <td id="LC531" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">getSecureMimeType</span>(<span class="pl-smi">$mimeType</span>) {</span></td>
      </tr>
      <tr>
        <td id="L532" class="blob-num js-line-number" data-line-number="532"></td>
        <td id="LC532" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span>getMimetypeDetector()<span class="pl-k">-&gt;</span>getSecureMimeType(<span class="pl-smi">$mimeType</span>);</span></td>
      </tr>
      <tr>
        <td id="L533" class="blob-num js-line-number" data-line-number="533"></td>
        <td id="LC533" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L534" class="blob-num js-line-number" data-line-number="534"></td>
        <td id="LC534" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L535" class="blob-num js-line-number" data-line-number="535"></td>
        <td id="LC535" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L536" class="blob-num js-line-number" data-line-number="536"></td>
        <td id="LC536" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * get the mimetype form a data string</span></span></td>
      </tr>
      <tr>
        <td id="L537" class="blob-num js-line-number" data-line-number="537"></td>
        <td id="LC537" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L538" class="blob-num js-line-number" data-line-number="538"></td>
        <td id="LC538" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $data</span></span></td>
      </tr>
      <tr>
        <td id="L539" class="blob-num js-line-number" data-line-number="539"></td>
        <td id="LC539" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string</span></span></td>
      </tr>
      <tr>
        <td id="L540" class="blob-num js-line-number" data-line-number="540"></td>
        <td id="LC540" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L541" class="blob-num js-line-number" data-line-number="541"></td>
        <td id="LC541" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">getStringMimeType</span>(<span class="pl-smi">$data</span>) {</span></td>
      </tr>
      <tr>
        <td id="L542" class="blob-num js-line-number" data-line-number="542"></td>
        <td id="LC542" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span>getMimetypeDetector()<span class="pl-k">-&gt;</span>detectString(<span class="pl-smi">$data</span>);</span></td>
      </tr>
      <tr>
        <td id="L543" class="blob-num js-line-number" data-line-number="543"></td>
        <td id="LC543" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L544" class="blob-num js-line-number" data-line-number="544"></td>
        <td id="LC544" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L545" class="blob-num js-line-number" data-line-number="545"></td>
        <td id="LC545" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L546" class="blob-num js-line-number" data-line-number="546"></td>
        <td id="LC546" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Checks $_REQUEST contains a var for the $s key. If so, returns the html-escaped value of this var; otherwise returns the default value provided by $d.</span></span></td>
      </tr>
      <tr>
        <td id="L547" class="blob-num js-line-number" data-line-number="547"></td>
        <td id="LC547" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $s name of the var to escape, if set.</span></span></td>
      </tr>
      <tr>
        <td id="L548" class="blob-num js-line-number" data-line-number="548"></td>
        <td id="LC548" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $d default value.</span></span></td>
      </tr>
      <tr>
        <td id="L549" class="blob-num js-line-number" data-line-number="549"></td>
        <td id="LC549" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string the print-safe value.</span></span></td>
      </tr>
      <tr>
        <td id="L550" class="blob-num js-line-number" data-line-number="550"></td>
        <td id="LC550" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L551" class="blob-num js-line-number" data-line-number="551"></td>
        <td id="LC551" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L552" class="blob-num js-line-number" data-line-number="552"></td>
        <td id="LC552" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L553" class="blob-num js-line-number" data-line-number="553"></td>
        <td id="LC553" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L554" class="blob-num js-line-number" data-line-number="554"></td>
        <td id="LC554" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * detect if a given program is found in the search PATH</span></span></td>
      </tr>
      <tr>
        <td id="L555" class="blob-num js-line-number" data-line-number="555"></td>
        <td id="LC555" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L556" class="blob-num js-line-number" data-line-number="556"></td>
        <td id="LC556" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $name</span></span></td>
      </tr>
      <tr>
        <td id="L557" class="blob-num js-line-number" data-line-number="557"></td>
        <td id="LC557" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> bool $path</span></span></td>
      </tr>
      <tr>
        <td id="L558" class="blob-num js-line-number" data-line-number="558"></td>
        <td id="LC558" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@internal</span> param string $program name</span></span></td>
      </tr>
      <tr>
        <td id="L559" class="blob-num js-line-number" data-line-number="559"></td>
        <td id="LC559" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@internal</span> param string $optional search path, defaults to $PATH</span></span></td>
      </tr>
      <tr>
        <td id="L560" class="blob-num js-line-number" data-line-number="560"></td>
        <td id="LC560" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> bool    true if executable program found in path</span></span></td>
      </tr>
      <tr>
        <td id="L561" class="blob-num js-line-number" data-line-number="561"></td>
        <td id="LC561" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L562" class="blob-num js-line-number" data-line-number="562"></td>
        <td id="LC562" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">canExecute</span>(<span class="pl-smi">$name</span>, <span class="pl-smi">$path</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>) {</span></td>
      </tr>
      <tr>
        <td id="L563" class="blob-num js-line-number" data-line-number="563"></td>
        <td id="LC563" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// path defaults to PATH from environment if not set</span></span></td>
      </tr>
      <tr>
        <td id="L564" class="blob-num js-line-number" data-line-number="564"></td>
        <td id="LC564" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$path</span> <span class="pl-k">===</span> <span class="pl-c1">false</span>) {</span></td>
      </tr>
      <tr>
        <td id="L565" class="blob-num js-line-number" data-line-number="565"></td>
        <td id="LC565" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$path</span> <span class="pl-k">=</span> <span class="pl-c1">getenv</span>(<span class="pl-s"><span class="pl-pds">&quot;</span>PATH<span class="pl-pds">&quot;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L566" class="blob-num js-line-number" data-line-number="566"></td>
        <td id="LC566" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L567" class="blob-num js-line-number" data-line-number="567"></td>
        <td id="LC567" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// check method depends on operating system</span></span></td>
      </tr>
      <tr>
        <td id="L568" class="blob-num js-line-number" data-line-number="568"></td>
        <td id="LC568" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-c1">strncmp</span>(<span class="pl-c1">PHP_OS</span>, <span class="pl-s"><span class="pl-pds">&quot;</span>WIN<span class="pl-pds">&quot;</span></span>, <span class="pl-c1">3</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L569" class="blob-num js-line-number" data-line-number="569"></td>
        <td id="LC569" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-c">// on Windows an appropriate COM or EXE file needs to exist</span></span></td>
      </tr>
      <tr>
        <td id="L570" class="blob-num js-line-number" data-line-number="570"></td>
        <td id="LC570" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$exts</span> <span class="pl-k">=</span> <span class="pl-c1">array</span>(<span class="pl-s"><span class="pl-pds">&quot;</span>.exe<span class="pl-pds">&quot;</span></span>, <span class="pl-s"><span class="pl-pds">&quot;</span>.com<span class="pl-pds">&quot;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L571" class="blob-num js-line-number" data-line-number="571"></td>
        <td id="LC571" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$check_fn</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span>file_exists<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L572" class="blob-num js-line-number" data-line-number="572"></td>
        <td id="LC572" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L573" class="blob-num js-line-number" data-line-number="573"></td>
        <td id="LC573" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-c">// anywhere else we look for an executable file of that name</span></span></td>
      </tr>
      <tr>
        <td id="L574" class="blob-num js-line-number" data-line-number="574"></td>
        <td id="LC574" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$exts</span> <span class="pl-k">=</span> <span class="pl-c1">array</span>(<span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-pds">&quot;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L575" class="blob-num js-line-number" data-line-number="575"></td>
        <td id="LC575" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$check_fn</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span>is_executable<span class="pl-pds">&quot;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L576" class="blob-num js-line-number" data-line-number="576"></td>
        <td id="LC576" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L577" class="blob-num js-line-number" data-line-number="577"></td>
        <td id="LC577" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// Default check will be done with $path directories :</span></span></td>
      </tr>
      <tr>
        <td id="L578" class="blob-num js-line-number" data-line-number="578"></td>
        <td id="LC578" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$dirs</span> <span class="pl-k">=</span> <span class="pl-c1">explode</span>(<span class="pl-c1">PATH_SEPARATOR</span>, <span class="pl-smi">$path</span>);</span></td>
      </tr>
      <tr>
        <td id="L579" class="blob-num js-line-number" data-line-number="579"></td>
        <td id="LC579" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// WARNING : We have to check if open_basedir is enabled :</span></span></td>
      </tr>
      <tr>
        <td id="L580" class="blob-num js-line-number" data-line-number="580"></td>
        <td id="LC580" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$obd</span> <span class="pl-k">=</span> <span class="pl-c1">ini_get</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>open_basedir<span class="pl-pds">&#39;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L581" class="blob-num js-line-number" data-line-number="581"></td>
        <td id="LC581" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$obd</span> <span class="pl-k">!</span><span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span>none<span class="pl-pds">&quot;</span></span>) {</span></td>
      </tr>
      <tr>
        <td id="L582" class="blob-num js-line-number" data-line-number="582"></td>
        <td id="LC582" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$obd_values</span> <span class="pl-k">=</span> <span class="pl-c1">explode</span>(<span class="pl-c1">PATH_SEPARATOR</span>, <span class="pl-smi">$obd</span>);</span></td>
      </tr>
      <tr>
        <td id="L583" class="blob-num js-line-number" data-line-number="583"></td>
        <td id="LC583" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-c1">count</span>(<span class="pl-smi">$obd_values</span>) <span class="pl-k">&gt;</span> <span class="pl-c1">0</span> <span class="pl-k">and</span> <span class="pl-smi">$obd_values</span>[<span class="pl-c1">0</span>]) {</span></td>
      </tr>
      <tr>
        <td id="L584" class="blob-num js-line-number" data-line-number="584"></td>
        <td id="LC584" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c">// open_basedir is in effect !</span></span></td>
      </tr>
      <tr>
        <td id="L585" class="blob-num js-line-number" data-line-number="585"></td>
        <td id="LC585" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c">// We need to check if the program is in one of these dirs :</span></span></td>
      </tr>
      <tr>
        <td id="L586" class="blob-num js-line-number" data-line-number="586"></td>
        <td id="LC586" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$dirs</span> <span class="pl-k">=</span> <span class="pl-smi">$obd_values</span>;</span></td>
      </tr>
      <tr>
        <td id="L587" class="blob-num js-line-number" data-line-number="587"></td>
        <td id="LC587" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L588" class="blob-num js-line-number" data-line-number="588"></td>
        <td id="LC588" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L589" class="blob-num js-line-number" data-line-number="589"></td>
        <td id="LC589" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">foreach</span> (<span class="pl-smi">$dirs</span> <span class="pl-k">as</span> <span class="pl-smi">$dir</span>) {</span></td>
      </tr>
      <tr>
        <td id="L590" class="blob-num js-line-number" data-line-number="590"></td>
        <td id="LC590" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">foreach</span> (<span class="pl-smi">$exts</span> <span class="pl-k">as</span> <span class="pl-smi">$ext</span>) {</span></td>
      </tr>
      <tr>
        <td id="L591" class="blob-num js-line-number" data-line-number="591"></td>
        <td id="LC591" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">if</span> ($<span class="pl-smi">check_fn</span>(<span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-smi">$dir</span>/<span class="pl-smi">$name</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$ext</span>))</span></td>
      </tr>
      <tr>
        <td id="L592" class="blob-num js-line-number" data-line-number="592"></td>
        <td id="LC592" class="blob-code js-file-line"><span class="pl-s1">					<span class="pl-k">return</span> <span class="pl-c1">true</span>;</span></td>
      </tr>
      <tr>
        <td id="L593" class="blob-num js-line-number" data-line-number="593"></td>
        <td id="LC593" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L594" class="blob-num js-line-number" data-line-number="594"></td>
        <td id="LC594" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L595" class="blob-num js-line-number" data-line-number="595"></td>
        <td id="LC595" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">false</span>;</span></td>
      </tr>
      <tr>
        <td id="L596" class="blob-num js-line-number" data-line-number="596"></td>
        <td id="LC596" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L597" class="blob-num js-line-number" data-line-number="597"></td>
        <td id="LC597" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L598" class="blob-num js-line-number" data-line-number="598"></td>
        <td id="LC598" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L599" class="blob-num js-line-number" data-line-number="599"></td>
        <td id="LC599" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * copy the contents of one stream to another</span></span></td>
      </tr>
      <tr>
        <td id="L600" class="blob-num js-line-number" data-line-number="600"></td>
        <td id="LC600" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L601" class="blob-num js-line-number" data-line-number="601"></td>
        <td id="LC601" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> resource $source</span></span></td>
      </tr>
      <tr>
        <td id="L602" class="blob-num js-line-number" data-line-number="602"></td>
        <td id="LC602" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> resource $target</span></span></td>
      </tr>
      <tr>
        <td id="L603" class="blob-num js-line-number" data-line-number="603"></td>
        <td id="LC603" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> array the number of bytes copied and result</span></span></td>
      </tr>
      <tr>
        <td id="L604" class="blob-num js-line-number" data-line-number="604"></td>
        <td id="LC604" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L605" class="blob-num js-line-number" data-line-number="605"></td>
        <td id="LC605" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">streamCopy</span>(<span class="pl-smi">$source</span>, <span class="pl-smi">$target</span>) {</span></td>
      </tr>
      <tr>
        <td id="L606" class="blob-num js-line-number" data-line-number="606"></td>
        <td id="LC606" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">$source</span> <span class="pl-k">or</span> <span class="pl-k">!</span><span class="pl-smi">$target</span>) {</span></td>
      </tr>
      <tr>
        <td id="L607" class="blob-num js-line-number" data-line-number="607"></td>
        <td id="LC607" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">array</span>(<span class="pl-c1">0</span>, <span class="pl-c1">false</span>);</span></td>
      </tr>
      <tr>
        <td id="L608" class="blob-num js-line-number" data-line-number="608"></td>
        <td id="LC608" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L609" class="blob-num js-line-number" data-line-number="609"></td>
        <td id="LC609" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$bufSize</span> <span class="pl-k">=</span> <span class="pl-c1">8192</span>;</span></td>
      </tr>
      <tr>
        <td id="L610" class="blob-num js-line-number" data-line-number="610"></td>
        <td id="LC610" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$result</span> <span class="pl-k">=</span> <span class="pl-c1">true</span>;</span></td>
      </tr>
      <tr>
        <td id="L611" class="blob-num js-line-number" data-line-number="611"></td>
        <td id="LC611" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$count</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;</span></td>
      </tr>
      <tr>
        <td id="L612" class="blob-num js-line-number" data-line-number="612"></td>
        <td id="LC612" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">while</span> (<span class="pl-k">!</span><span class="pl-c1">feof</span>(<span class="pl-smi">$source</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L613" class="blob-num js-line-number" data-line-number="613"></td>
        <td id="LC613" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$buf</span> <span class="pl-k">=</span> <span class="pl-c1">fread</span>(<span class="pl-smi">$source</span>, <span class="pl-smi">$bufSize</span>);</span></td>
      </tr>
      <tr>
        <td id="L614" class="blob-num js-line-number" data-line-number="614"></td>
        <td id="LC614" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$bytesWritten</span> <span class="pl-k">=</span> <span class="pl-c1">fwrite</span>(<span class="pl-smi">$target</span>, <span class="pl-smi">$buf</span>);</span></td>
      </tr>
      <tr>
        <td id="L615" class="blob-num js-line-number" data-line-number="615"></td>
        <td id="LC615" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-smi">$bytesWritten</span> <span class="pl-k">!</span><span class="pl-k">==</span> <span class="pl-c1">false</span>) {</span></td>
      </tr>
      <tr>
        <td id="L616" class="blob-num js-line-number" data-line-number="616"></td>
        <td id="LC616" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$count</span> <span class="pl-k">+</span><span class="pl-k">=</span> <span class="pl-smi">$bytesWritten</span>;</span></td>
      </tr>
      <tr>
        <td id="L617" class="blob-num js-line-number" data-line-number="617"></td>
        <td id="LC617" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L618" class="blob-num js-line-number" data-line-number="618"></td>
        <td id="LC618" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-c">// note: strlen is expensive so only use it when necessary,</span></span></td>
      </tr>
      <tr>
        <td id="L619" class="blob-num js-line-number" data-line-number="619"></td>
        <td id="LC619" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-c">// on the last block</span></span></td>
      </tr>
      <tr>
        <td id="L620" class="blob-num js-line-number" data-line-number="620"></td>
        <td id="LC620" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-smi">$bytesWritten</span> <span class="pl-k">===</span> <span class="pl-c1">false</span></span></td>
      </tr>
      <tr>
        <td id="L621" class="blob-num js-line-number" data-line-number="621"></td>
        <td id="LC621" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">||</span> (<span class="pl-smi">$bytesWritten</span> <span class="pl-k">&lt;</span> <span class="pl-smi">$bufSize</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">$bytesWritten</span> <span class="pl-k">&lt;</span> <span class="pl-c1">strlen</span>(<span class="pl-smi">$buf</span>))</span></td>
      </tr>
      <tr>
        <td id="L622" class="blob-num js-line-number" data-line-number="622"></td>
        <td id="LC622" class="blob-code js-file-line"><span class="pl-s1">			) {</span></td>
      </tr>
      <tr>
        <td id="L623" class="blob-num js-line-number" data-line-number="623"></td>
        <td id="LC623" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c">// write error, could be disk full ?</span></span></td>
      </tr>
      <tr>
        <td id="L624" class="blob-num js-line-number" data-line-number="624"></td>
        <td id="LC624" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$result</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;</span></td>
      </tr>
      <tr>
        <td id="L625" class="blob-num js-line-number" data-line-number="625"></td>
        <td id="LC625" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">break</span>;</span></td>
      </tr>
      <tr>
        <td id="L626" class="blob-num js-line-number" data-line-number="626"></td>
        <td id="LC626" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L627" class="blob-num js-line-number" data-line-number="627"></td>
        <td id="LC627" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L628" class="blob-num js-line-number" data-line-number="628"></td>
        <td id="LC628" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">array</span>(<span class="pl-smi">$count</span>, <span class="pl-smi">$result</span>);</span></td>
      </tr>
      <tr>
        <td id="L629" class="blob-num js-line-number" data-line-number="629"></td>
        <td id="LC629" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L630" class="blob-num js-line-number" data-line-number="630"></td>
        <td id="LC630" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L631" class="blob-num js-line-number" data-line-number="631"></td>
        <td id="LC631" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L632" class="blob-num js-line-number" data-line-number="632"></td>
        <td id="LC632" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * create a temporary file with an unique filename</span></span></td>
      </tr>
      <tr>
        <td id="L633" class="blob-num js-line-number" data-line-number="633"></td>
        <td id="LC633" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L634" class="blob-num js-line-number" data-line-number="634"></td>
        <td id="LC634" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $postfix</span></span></td>
      </tr>
      <tr>
        <td id="L635" class="blob-num js-line-number" data-line-number="635"></td>
        <td id="LC635" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string</span></span></td>
      </tr>
      <tr>
        <td id="L636" class="blob-num js-line-number" data-line-number="636"></td>
        <td id="LC636" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@deprecated</span> Use the TempManager instead</span></span></td>
      </tr>
      <tr>
        <td id="L637" class="blob-num js-line-number" data-line-number="637"></td>
        <td id="LC637" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L638" class="blob-num js-line-number" data-line-number="638"></td>
        <td id="LC638" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * temporary files are automatically cleaned up after the script is finished</span></span></td>
      </tr>
      <tr>
        <td id="L639" class="blob-num js-line-number" data-line-number="639"></td>
        <td id="LC639" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L640" class="blob-num js-line-number" data-line-number="640"></td>
        <td id="LC640" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">tmpFile</span>(<span class="pl-smi">$postfix</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-pds">&#39;</span></span>) {</span></td>
      </tr>
      <tr>
        <td id="L641" class="blob-num js-line-number" data-line-number="641"></td>
        <td id="LC641" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">\</span><span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getTempManager()<span class="pl-k">-&gt;</span>getTemporaryFile(<span class="pl-smi">$postfix</span>);</span></td>
      </tr>
      <tr>
        <td id="L642" class="blob-num js-line-number" data-line-number="642"></td>
        <td id="LC642" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L643" class="blob-num js-line-number" data-line-number="643"></td>
        <td id="LC643" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L644" class="blob-num js-line-number" data-line-number="644"></td>
        <td id="LC644" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L645" class="blob-num js-line-number" data-line-number="645"></td>
        <td id="LC645" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * create a temporary folder with an unique filename</span></span></td>
      </tr>
      <tr>
        <td id="L646" class="blob-num js-line-number" data-line-number="646"></td>
        <td id="LC646" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L647" class="blob-num js-line-number" data-line-number="647"></td>
        <td id="LC647" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string</span></span></td>
      </tr>
      <tr>
        <td id="L648" class="blob-num js-line-number" data-line-number="648"></td>
        <td id="LC648" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@deprecated</span> Use the TempManager instead</span></span></td>
      </tr>
      <tr>
        <td id="L649" class="blob-num js-line-number" data-line-number="649"></td>
        <td id="LC649" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L650" class="blob-num js-line-number" data-line-number="650"></td>
        <td id="LC650" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * temporary files are automatically cleaned up after the script is finished</span></span></td>
      </tr>
      <tr>
        <td id="L651" class="blob-num js-line-number" data-line-number="651"></td>
        <td id="LC651" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L652" class="blob-num js-line-number" data-line-number="652"></td>
        <td id="LC652" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">tmpFolder</span>() {</span></td>
      </tr>
      <tr>
        <td id="L653" class="blob-num js-line-number" data-line-number="653"></td>
        <td id="LC653" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">\</span><span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getTempManager()<span class="pl-k">-&gt;</span>getTemporaryFolder();</span></td>
      </tr>
      <tr>
        <td id="L654" class="blob-num js-line-number" data-line-number="654"></td>
        <td id="LC654" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L655" class="blob-num js-line-number" data-line-number="655"></td>
        <td id="LC655" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L656" class="blob-num js-line-number" data-line-number="656"></td>
        <td id="LC656" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L657" class="blob-num js-line-number" data-line-number="657"></td>
        <td id="LC657" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Adds a suffix to the name in case the file exists</span></span></td>
      </tr>
      <tr>
        <td id="L658" class="blob-num js-line-number" data-line-number="658"></td>
        <td id="LC658" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L659" class="blob-num js-line-number" data-line-number="659"></td>
        <td id="LC659" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $path</span></span></td>
      </tr>
      <tr>
        <td id="L660" class="blob-num js-line-number" data-line-number="660"></td>
        <td id="LC660" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $filename</span></span></td>
      </tr>
      <tr>
        <td id="L661" class="blob-num js-line-number" data-line-number="661"></td>
        <td id="LC661" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string</span></span></td>
      </tr>
      <tr>
        <td id="L662" class="blob-num js-line-number" data-line-number="662"></td>
        <td id="LC662" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L663" class="blob-num js-line-number" data-line-number="663"></td>
        <td id="LC663" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">buildNotExistingFileName</span>(<span class="pl-smi">$path</span>, <span class="pl-smi">$filename</span>) {</span></td>
      </tr>
      <tr>
        <td id="L664" class="blob-num js-line-number" data-line-number="664"></td>
        <td id="LC664" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$view</span> <span class="pl-k">=</span> <span class="pl-c1">\OC\Files\</span><span class="pl-c1">Filesystem</span><span class="pl-k">::</span>getView();</span></td>
      </tr>
      <tr>
        <td id="L665" class="blob-num js-line-number" data-line-number="665"></td>
        <td id="LC665" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span>buildNotExistingFileNameForView(<span class="pl-smi">$path</span>, <span class="pl-smi">$filename</span>, <span class="pl-smi">$view</span>);</span></td>
      </tr>
      <tr>
        <td id="L666" class="blob-num js-line-number" data-line-number="666"></td>
        <td id="LC666" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L667" class="blob-num js-line-number" data-line-number="667"></td>
        <td id="LC667" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L668" class="blob-num js-line-number" data-line-number="668"></td>
        <td id="LC668" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L669" class="blob-num js-line-number" data-line-number="669"></td>
        <td id="LC669" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Adds a suffix to the name in case the file exists</span></span></td>
      </tr>
      <tr>
        <td id="L670" class="blob-num js-line-number" data-line-number="670"></td>
        <td id="LC670" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L671" class="blob-num js-line-number" data-line-number="671"></td>
        <td id="LC671" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $path</span></span></td>
      </tr>
      <tr>
        <td id="L672" class="blob-num js-line-number" data-line-number="672"></td>
        <td id="LC672" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $filename</span></span></td>
      </tr>
      <tr>
        <td id="L673" class="blob-num js-line-number" data-line-number="673"></td>
        <td id="LC673" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string</span></span></td>
      </tr>
      <tr>
        <td id="L674" class="blob-num js-line-number" data-line-number="674"></td>
        <td id="LC674" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L675" class="blob-num js-line-number" data-line-number="675"></td>
        <td id="LC675" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">buildNotExistingFileNameForView</span>(<span class="pl-smi">$path</span>, <span class="pl-smi">$filename</span>, <span class="pl-c1">\OC\Files\</span><span class="pl-c1">View</span> <span class="pl-smi">$view</span>) {</span></td>
      </tr>
      <tr>
        <td id="L676" class="blob-num js-line-number" data-line-number="676"></td>
        <td id="LC676" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$path</span> <span class="pl-k">===</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/<span class="pl-pds">&#39;</span></span>) {</span></td>
      </tr>
      <tr>
        <td id="L677" class="blob-num js-line-number" data-line-number="677"></td>
        <td id="LC677" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$path</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L678" class="blob-num js-line-number" data-line-number="678"></td>
        <td id="LC678" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L679" class="blob-num js-line-number" data-line-number="679"></td>
        <td id="LC679" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$pos</span> <span class="pl-k">=</span> <span class="pl-c1">strrpos</span>(<span class="pl-smi">$filename</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>.<span class="pl-pds">&#39;</span></span>)) {</span></td>
      </tr>
      <tr>
        <td id="L680" class="blob-num js-line-number" data-line-number="680"></td>
        <td id="LC680" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$name</span> <span class="pl-k">=</span> <span class="pl-c1">substr</span>(<span class="pl-smi">$filename</span>, <span class="pl-c1">0</span>, <span class="pl-smi">$pos</span>);</span></td>
      </tr>
      <tr>
        <td id="L681" class="blob-num js-line-number" data-line-number="681"></td>
        <td id="LC681" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$ext</span> <span class="pl-k">=</span> <span class="pl-c1">substr</span>(<span class="pl-smi">$filename</span>, <span class="pl-smi">$pos</span>);</span></td>
      </tr>
      <tr>
        <td id="L682" class="blob-num js-line-number" data-line-number="682"></td>
        <td id="LC682" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L683" class="blob-num js-line-number" data-line-number="683"></td>
        <td id="LC683" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$name</span> <span class="pl-k">=</span> <span class="pl-smi">$filename</span>;</span></td>
      </tr>
      <tr>
        <td id="L684" class="blob-num js-line-number" data-line-number="684"></td>
        <td id="LC684" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$ext</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L685" class="blob-num js-line-number" data-line-number="685"></td>
        <td id="LC685" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L686" class="blob-num js-line-number" data-line-number="686"></td>
        <td id="LC686" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L687" class="blob-num js-line-number" data-line-number="687"></td>
        <td id="LC687" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$newpath</span> <span class="pl-k">=</span> <span class="pl-smi">$path</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$filename</span>;</span></td>
      </tr>
      <tr>
        <td id="L688" class="blob-num js-line-number" data-line-number="688"></td>
        <td id="LC688" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$view</span><span class="pl-k">-&gt;</span>file_exists(<span class="pl-smi">$newpath</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L689" class="blob-num js-line-number" data-line-number="689"></td>
        <td id="LC689" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-c1">preg_match_all</span>(<span class="pl-sr"><span class="pl-pds">&#39;/</span><span class="pl-cce">\(</span>(<span class="pl-cce">\d</span><span class="pl-k">+</span>)<span class="pl-cce">\)</span><span class="pl-pds">/&#39;</span></span>, <span class="pl-smi">$name</span>, <span class="pl-smi">$matches</span>, <span class="pl-c1">PREG_OFFSET_CAPTURE</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L690" class="blob-num js-line-number" data-line-number="690"></td>
        <td id="LC690" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c">//Replace the last &quot;(number)&quot; with &quot;(number+1)&quot;</span></span></td>
      </tr>
      <tr>
        <td id="L691" class="blob-num js-line-number" data-line-number="691"></td>
        <td id="LC691" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$last_match</span> <span class="pl-k">=</span> <span class="pl-c1">count</span>(<span class="pl-smi">$matches</span>[<span class="pl-c1">0</span>]) <span class="pl-k">-</span> <span class="pl-c1">1</span>;</span></td>
      </tr>
      <tr>
        <td id="L692" class="blob-num js-line-number" data-line-number="692"></td>
        <td id="LC692" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$counter</span> <span class="pl-k">=</span> <span class="pl-smi">$matches</span>[<span class="pl-c1">1</span>][<span class="pl-smi">$last_match</span>][<span class="pl-c1">0</span>] <span class="pl-k">+</span> <span class="pl-c1">1</span>;</span></td>
      </tr>
      <tr>
        <td id="L693" class="blob-num js-line-number" data-line-number="693"></td>
        <td id="LC693" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$offset</span> <span class="pl-k">=</span> <span class="pl-smi">$matches</span>[<span class="pl-c1">0</span>][<span class="pl-smi">$last_match</span>][<span class="pl-c1">1</span>];</span></td>
      </tr>
      <tr>
        <td id="L694" class="blob-num js-line-number" data-line-number="694"></td>
        <td id="LC694" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$match_length</span> <span class="pl-k">=</span> <span class="pl-c1">strlen</span>(<span class="pl-smi">$matches</span>[<span class="pl-c1">0</span>][<span class="pl-smi">$last_match</span>][<span class="pl-c1">0</span>]);</span></td>
      </tr>
      <tr>
        <td id="L695" class="blob-num js-line-number" data-line-number="695"></td>
        <td id="LC695" class="blob-code js-file-line"><span class="pl-s1">			} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L696" class="blob-num js-line-number" data-line-number="696"></td>
        <td id="LC696" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$counter</span> <span class="pl-k">=</span> <span class="pl-c1">2</span>;</span></td>
      </tr>
      <tr>
        <td id="L697" class="blob-num js-line-number" data-line-number="697"></td>
        <td id="LC697" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$offset</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;</span></td>
      </tr>
      <tr>
        <td id="L698" class="blob-num js-line-number" data-line-number="698"></td>
        <td id="LC698" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L699" class="blob-num js-line-number" data-line-number="699"></td>
        <td id="LC699" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">do</span> {</span></td>
      </tr>
      <tr>
        <td id="L700" class="blob-num js-line-number" data-line-number="700"></td>
        <td id="LC700" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">if</span> (<span class="pl-smi">$offset</span>) {</span></td>
      </tr>
      <tr>
        <td id="L701" class="blob-num js-line-number" data-line-number="701"></td>
        <td id="LC701" class="blob-code js-file-line"><span class="pl-s1">					<span class="pl-c">//Replace the last &quot;(number)&quot; with &quot;(number+1)&quot;</span></span></td>
      </tr>
      <tr>
        <td id="L702" class="blob-num js-line-number" data-line-number="702"></td>
        <td id="LC702" class="blob-code js-file-line"><span class="pl-s1">					<span class="pl-smi">$newname</span> <span class="pl-k">=</span> <span class="pl-c1">substr_replace</span>(<span class="pl-smi">$name</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>(<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$counter</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>)<span class="pl-pds">&#39;</span></span>, <span class="pl-smi">$offset</span>, <span class="pl-smi">$match_length</span>);</span></td>
      </tr>
      <tr>
        <td id="L703" class="blob-num js-line-number" data-line-number="703"></td>
        <td id="LC703" class="blob-code js-file-line"><span class="pl-s1">				} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L704" class="blob-num js-line-number" data-line-number="704"></td>
        <td id="LC704" class="blob-code js-file-line"><span class="pl-s1">					<span class="pl-smi">$newname</span> <span class="pl-k">=</span> <span class="pl-smi">$name</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span> (<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$counter</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>)<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L705" class="blob-num js-line-number" data-line-number="705"></td>
        <td id="LC705" class="blob-code js-file-line"><span class="pl-s1">				}</span></td>
      </tr>
      <tr>
        <td id="L706" class="blob-num js-line-number" data-line-number="706"></td>
        <td id="LC706" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$newpath</span> <span class="pl-k">=</span> <span class="pl-smi">$path</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$newname</span> <span class="pl-k">.</span> <span class="pl-smi">$ext</span>;</span></td>
      </tr>
      <tr>
        <td id="L707" class="blob-num js-line-number" data-line-number="707"></td>
        <td id="LC707" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$counter</span><span class="pl-k">++</span>;</span></td>
      </tr>
      <tr>
        <td id="L708" class="blob-num js-line-number" data-line-number="708"></td>
        <td id="LC708" class="blob-code js-file-line"><span class="pl-s1">			} <span class="pl-k">while</span> (<span class="pl-smi">$view</span><span class="pl-k">-&gt;</span>file_exists(<span class="pl-smi">$newpath</span>));</span></td>
      </tr>
      <tr>
        <td id="L709" class="blob-num js-line-number" data-line-number="709"></td>
        <td id="LC709" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L710" class="blob-num js-line-number" data-line-number="710"></td>
        <td id="LC710" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L711" class="blob-num js-line-number" data-line-number="711"></td>
        <td id="LC711" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-smi">$newpath</span>;</span></td>
      </tr>
      <tr>
        <td id="L712" class="blob-num js-line-number" data-line-number="712"></td>
        <td id="LC712" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L713" class="blob-num js-line-number" data-line-number="713"></td>
        <td id="LC713" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L714" class="blob-num js-line-number" data-line-number="714"></td>
        <td id="LC714" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L715" class="blob-num js-line-number" data-line-number="715"></td>
        <td id="LC715" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Checks if $sub is a subdirectory of $parent</span></span></td>
      </tr>
      <tr>
        <td id="L716" class="blob-num js-line-number" data-line-number="716"></td>
        <td id="LC716" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L717" class="blob-num js-line-number" data-line-number="717"></td>
        <td id="LC717" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $sub</span></span></td>
      </tr>
      <tr>
        <td id="L718" class="blob-num js-line-number" data-line-number="718"></td>
        <td id="LC718" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $parent</span></span></td>
      </tr>
      <tr>
        <td id="L719" class="blob-num js-line-number" data-line-number="719"></td>
        <td id="LC719" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> bool</span></span></td>
      </tr>
      <tr>
        <td id="L720" class="blob-num js-line-number" data-line-number="720"></td>
        <td id="LC720" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L721" class="blob-num js-line-number" data-line-number="721"></td>
        <td id="LC721" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">isSubDirectory</span>(<span class="pl-smi">$sub</span>, <span class="pl-smi">$parent</span>) {</span></td>
      </tr>
      <tr>
        <td id="L722" class="blob-num js-line-number" data-line-number="722"></td>
        <td id="LC722" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$realpathSub</span> <span class="pl-k">=</span> <span class="pl-c1">realpath</span>(<span class="pl-smi">$sub</span>);</span></td>
      </tr>
      <tr>
        <td id="L723" class="blob-num js-line-number" data-line-number="723"></td>
        <td id="LC723" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$realpathParent</span> <span class="pl-k">=</span> <span class="pl-c1">realpath</span>(<span class="pl-smi">$parent</span>);</span></td>
      </tr>
      <tr>
        <td id="L724" class="blob-num js-line-number" data-line-number="724"></td>
        <td id="LC724" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L725" class="blob-num js-line-number" data-line-number="725"></td>
        <td id="LC725" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// realpath() may return false in case the directory does not exist</span></span></td>
      </tr>
      <tr>
        <td id="L726" class="blob-num js-line-number" data-line-number="726"></td>
        <td id="LC726" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// since we can not be sure how different PHP versions may behave here</span></span></td>
      </tr>
      <tr>
        <td id="L727" class="blob-num js-line-number" data-line-number="727"></td>
        <td id="LC727" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// we do an additional check whether realpath returned false</span></span></td>
      </tr>
      <tr>
        <td id="L728" class="blob-num js-line-number" data-line-number="728"></td>
        <td id="LC728" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span>(<span class="pl-smi">$realpathSub</span> <span class="pl-k">===</span> <span class="pl-c1">false</span> <span class="pl-k">||</span>  <span class="pl-smi">$realpathParent</span> <span class="pl-k">===</span> <span class="pl-c1">false</span>) {</span></td>
      </tr>
      <tr>
        <td id="L729" class="blob-num js-line-number" data-line-number="729"></td>
        <td id="LC729" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">false</span>;</span></td>
      </tr>
      <tr>
        <td id="L730" class="blob-num js-line-number" data-line-number="730"></td>
        <td id="LC730" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L731" class="blob-num js-line-number" data-line-number="731"></td>
        <td id="LC731" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L732" class="blob-num js-line-number" data-line-number="732"></td>
        <td id="LC732" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// Check whether $sub is a subdirectory of $parent</span></span></td>
      </tr>
      <tr>
        <td id="L733" class="blob-num js-line-number" data-line-number="733"></td>
        <td id="LC733" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">strpos</span>(<span class="pl-smi">$realpathSub</span>, <span class="pl-smi">$realpathParent</span>) <span class="pl-k">===</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L734" class="blob-num js-line-number" data-line-number="734"></td>
        <td id="LC734" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">true</span>;</span></td>
      </tr>
      <tr>
        <td id="L735" class="blob-num js-line-number" data-line-number="735"></td>
        <td id="LC735" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L736" class="blob-num js-line-number" data-line-number="736"></td>
        <td id="LC736" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L737" class="blob-num js-line-number" data-line-number="737"></td>
        <td id="LC737" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">false</span>;</span></td>
      </tr>
      <tr>
        <td id="L738" class="blob-num js-line-number" data-line-number="738"></td>
        <td id="LC738" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L739" class="blob-num js-line-number" data-line-number="739"></td>
        <td id="LC739" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L740" class="blob-num js-line-number" data-line-number="740"></td>
        <td id="LC740" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L741" class="blob-num js-line-number" data-line-number="741"></td>
        <td id="LC741" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns an array with all keys from input lowercased or uppercased. Numbered indices are left as is.</span></span></td>
      </tr>
      <tr>
        <td id="L742" class="blob-num js-line-number" data-line-number="742"></td>
        <td id="LC742" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L743" class="blob-num js-line-number" data-line-number="743"></td>
        <td id="LC743" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> array $input The array to work on</span></span></td>
      </tr>
      <tr>
        <td id="L744" class="blob-num js-line-number" data-line-number="744"></td>
        <td id="LC744" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> int $case Either MB_CASE_UPPER or MB_CASE_LOWER (default)</span></span></td>
      </tr>
      <tr>
        <td id="L745" class="blob-num js-line-number" data-line-number="745"></td>
        <td id="LC745" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $encoding The encoding parameter is the character encoding. Defaults to UTF-8</span></span></td>
      </tr>
      <tr>
        <td id="L746" class="blob-num js-line-number" data-line-number="746"></td>
        <td id="LC746" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> array</span></span></td>
      </tr>
      <tr>
        <td id="L747" class="blob-num js-line-number" data-line-number="747"></td>
        <td id="LC747" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L748" class="blob-num js-line-number" data-line-number="748"></td>
        <td id="LC748" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns an array with all keys from input lowercased or uppercased. Numbered indices are left as is.</span></span></td>
      </tr>
      <tr>
        <td id="L749" class="blob-num js-line-number" data-line-number="749"></td>
        <td id="LC749" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * based on http://www.php.net/manual/en/function.array-change-key-case.php#107715</span></span></td>
      </tr>
      <tr>
        <td id="L750" class="blob-num js-line-number" data-line-number="750"></td>
        <td id="LC750" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L751" class="blob-num js-line-number" data-line-number="751"></td>
        <td id="LC751" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L752" class="blob-num js-line-number" data-line-number="752"></td>
        <td id="LC752" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">mb_array_change_key_case</span>(<span class="pl-smi">$input</span>, <span class="pl-smi">$case</span> <span class="pl-k">=</span> <span class="pl-c1">MB_CASE_LOWER</span>, <span class="pl-smi">$encoding</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span>UTF-8<span class="pl-pds">&#39;</span></span>) {</span></td>
      </tr>
      <tr>
        <td id="L753" class="blob-num js-line-number" data-line-number="753"></td>
        <td id="LC753" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$case</span> <span class="pl-k">=</span> (<span class="pl-smi">$case</span> <span class="pl-k">!</span><span class="pl-k">=</span> <span class="pl-c1">MB_CASE_UPPER</span>) ? <span class="pl-c1">MB_CASE_LOWER</span> : <span class="pl-c1">MB_CASE_UPPER</span>;</span></td>
      </tr>
      <tr>
        <td id="L754" class="blob-num js-line-number" data-line-number="754"></td>
        <td id="LC754" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$ret</span> <span class="pl-k">=</span> <span class="pl-c1">array</span>();</span></td>
      </tr>
      <tr>
        <td id="L755" class="blob-num js-line-number" data-line-number="755"></td>
        <td id="LC755" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">foreach</span> (<span class="pl-smi">$input</span> <span class="pl-k">as</span> <span class="pl-smi">$k</span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$v</span>) {</span></td>
      </tr>
      <tr>
        <td id="L756" class="blob-num js-line-number" data-line-number="756"></td>
        <td id="LC756" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$ret</span>[<span class="pl-c1">mb_convert_case</span>(<span class="pl-smi">$k</span>, <span class="pl-smi">$case</span>, <span class="pl-smi">$encoding</span>)] <span class="pl-k">=</span> <span class="pl-smi">$v</span>;</span></td>
      </tr>
      <tr>
        <td id="L757" class="blob-num js-line-number" data-line-number="757"></td>
        <td id="LC757" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L758" class="blob-num js-line-number" data-line-number="758"></td>
        <td id="LC758" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-smi">$ret</span>;</span></td>
      </tr>
      <tr>
        <td id="L759" class="blob-num js-line-number" data-line-number="759"></td>
        <td id="LC759" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L760" class="blob-num js-line-number" data-line-number="760"></td>
        <td id="LC760" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L761" class="blob-num js-line-number" data-line-number="761"></td>
        <td id="LC761" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L762" class="blob-num js-line-number" data-line-number="762"></td>
        <td id="LC762" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * replaces a copy of string delimited by the start and (optionally) length parameters with the string given in replacement.</span></span></td>
      </tr>
      <tr>
        <td id="L763" class="blob-num js-line-number" data-line-number="763"></td>
        <td id="LC763" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L764" class="blob-num js-line-number" data-line-number="764"></td>
        <td id="LC764" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $string</span></span></td>
      </tr>
      <tr>
        <td id="L765" class="blob-num js-line-number" data-line-number="765"></td>
        <td id="LC765" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $replacement The replacement string.</span></span></td>
      </tr>
      <tr>
        <td id="L766" class="blob-num js-line-number" data-line-number="766"></td>
        <td id="LC766" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> int $start If start is positive, the replacing will begin at the start&#39;th offset into string. If start is negative, the replacing will begin at the start&#39;th character from the end of string.</span></span></td>
      </tr>
      <tr>
        <td id="L767" class="blob-num js-line-number" data-line-number="767"></td>
        <td id="LC767" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> int $length Length of the part to be replaced</span></span></td>
      </tr>
      <tr>
        <td id="L768" class="blob-num js-line-number" data-line-number="768"></td>
        <td id="LC768" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $encoding The encoding parameter is the character encoding. Defaults to UTF-8</span></span></td>
      </tr>
      <tr>
        <td id="L769" class="blob-num js-line-number" data-line-number="769"></td>
        <td id="LC769" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@internal</span> param string $input The input string. .Opposite to the PHP build-in function does not accept an array.</span></span></td>
      </tr>
      <tr>
        <td id="L770" class="blob-num js-line-number" data-line-number="770"></td>
        <td id="LC770" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string</span></span></td>
      </tr>
      <tr>
        <td id="L771" class="blob-num js-line-number" data-line-number="771"></td>
        <td id="LC771" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L772" class="blob-num js-line-number" data-line-number="772"></td>
        <td id="LC772" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">mb_substr_replace</span>(<span class="pl-smi">$string</span>, <span class="pl-smi">$replacement</span>, <span class="pl-smi">$start</span>, <span class="pl-smi">$length</span> <span class="pl-k">=</span> <span class="pl-c1">null</span>, <span class="pl-smi">$encoding</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span>UTF-8<span class="pl-pds">&#39;</span></span>) {</span></td>
      </tr>
      <tr>
        <td id="L773" class="blob-num js-line-number" data-line-number="773"></td>
        <td id="LC773" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$start</span> <span class="pl-k">=</span> <span class="pl-c1">intval</span>(<span class="pl-smi">$start</span>);</span></td>
      </tr>
      <tr>
        <td id="L774" class="blob-num js-line-number" data-line-number="774"></td>
        <td id="LC774" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$length</span> <span class="pl-k">=</span> <span class="pl-c1">intval</span>(<span class="pl-smi">$length</span>);</span></td>
      </tr>
      <tr>
        <td id="L775" class="blob-num js-line-number" data-line-number="775"></td>
        <td id="LC775" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$string</span> <span class="pl-k">=</span> <span class="pl-c1">mb_substr</span>(<span class="pl-smi">$string</span>, <span class="pl-c1">0</span>, <span class="pl-smi">$start</span>, <span class="pl-smi">$encoding</span>) <span class="pl-k">.</span></span></td>
      </tr>
      <tr>
        <td id="L776" class="blob-num js-line-number" data-line-number="776"></td>
        <td id="LC776" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$replacement</span> <span class="pl-k">.</span></span></td>
      </tr>
      <tr>
        <td id="L777" class="blob-num js-line-number" data-line-number="777"></td>
        <td id="LC777" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-c1">mb_substr</span>(<span class="pl-smi">$string</span>, <span class="pl-smi">$start</span> <span class="pl-k">+</span> <span class="pl-smi">$length</span>, <span class="pl-c1">mb_strlen</span>(<span class="pl-smi">$string</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>UTF-8<span class="pl-pds">&#39;</span></span>) <span class="pl-k">-</span> <span class="pl-smi">$start</span>, <span class="pl-smi">$encoding</span>);</span></td>
      </tr>
      <tr>
        <td id="L778" class="blob-num js-line-number" data-line-number="778"></td>
        <td id="LC778" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L779" class="blob-num js-line-number" data-line-number="779"></td>
        <td id="LC779" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-smi">$string</span>;</span></td>
      </tr>
      <tr>
        <td id="L780" class="blob-num js-line-number" data-line-number="780"></td>
        <td id="LC780" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L781" class="blob-num js-line-number" data-line-number="781"></td>
        <td id="LC781" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L782" class="blob-num js-line-number" data-line-number="782"></td>
        <td id="LC782" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L783" class="blob-num js-line-number" data-line-number="783"></td>
        <td id="LC783" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Replace all occurrences of the search string with the replacement string</span></span></td>
      </tr>
      <tr>
        <td id="L784" class="blob-num js-line-number" data-line-number="784"></td>
        <td id="LC784" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L785" class="blob-num js-line-number" data-line-number="785"></td>
        <td id="LC785" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $search The value being searched for, otherwise known as the needle.</span></span></td>
      </tr>
      <tr>
        <td id="L786" class="blob-num js-line-number" data-line-number="786"></td>
        <td id="LC786" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $replace The replacement</span></span></td>
      </tr>
      <tr>
        <td id="L787" class="blob-num js-line-number" data-line-number="787"></td>
        <td id="LC787" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $subject The string or array being searched and replaced on, otherwise known as the haystack.</span></span></td>
      </tr>
      <tr>
        <td id="L788" class="blob-num js-line-number" data-line-number="788"></td>
        <td id="LC788" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $encoding The encoding parameter is the character encoding. Defaults to UTF-8</span></span></td>
      </tr>
      <tr>
        <td id="L789" class="blob-num js-line-number" data-line-number="789"></td>
        <td id="LC789" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> int $count If passed, this will be set to the number of replacements performed.</span></span></td>
      </tr>
      <tr>
        <td id="L790" class="blob-num js-line-number" data-line-number="790"></td>
        <td id="LC790" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string</span></span></td>
      </tr>
      <tr>
        <td id="L791" class="blob-num js-line-number" data-line-number="791"></td>
        <td id="LC791" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L792" class="blob-num js-line-number" data-line-number="792"></td>
        <td id="LC792" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L793" class="blob-num js-line-number" data-line-number="793"></td>
        <td id="LC793" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">mb_str_replace</span>(<span class="pl-smi">$search</span>, <span class="pl-smi">$replace</span>, <span class="pl-smi">$subject</span>, <span class="pl-smi">$encoding</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span>UTF-8<span class="pl-pds">&#39;</span></span>, <span class="pl-k">&amp;</span><span class="pl-smi">$count</span> <span class="pl-k">=</span> <span class="pl-c1">null</span>) {</span></td>
      </tr>
      <tr>
        <td id="L794" class="blob-num js-line-number" data-line-number="794"></td>
        <td id="LC794" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$offset</span> <span class="pl-k">=</span> <span class="pl-k">-</span><span class="pl-c1">1</span>;</span></td>
      </tr>
      <tr>
        <td id="L795" class="blob-num js-line-number" data-line-number="795"></td>
        <td id="LC795" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$length</span> <span class="pl-k">=</span> <span class="pl-c1">mb_strlen</span>(<span class="pl-smi">$search</span>, <span class="pl-smi">$encoding</span>);</span></td>
      </tr>
      <tr>
        <td id="L796" class="blob-num js-line-number" data-line-number="796"></td>
        <td id="LC796" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">while</span> ((<span class="pl-smi">$i</span> <span class="pl-k">=</span> <span class="pl-c1">mb_strrpos</span>(<span class="pl-smi">$subject</span>, <span class="pl-smi">$search</span>, <span class="pl-smi">$offset</span>, <span class="pl-smi">$encoding</span>)) <span class="pl-k">!</span><span class="pl-k">==</span> <span class="pl-c1">false</span>) {</span></td>
      </tr>
      <tr>
        <td id="L797" class="blob-num js-line-number" data-line-number="797"></td>
        <td id="LC797" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$subject</span> <span class="pl-k">=</span> <span class="pl-c1">OC_Helper</span><span class="pl-k">::</span>mb_substr_replace(<span class="pl-smi">$subject</span>, <span class="pl-smi">$replace</span>, <span class="pl-smi">$i</span>, <span class="pl-smi">$length</span>);</span></td>
      </tr>
      <tr>
        <td id="L798" class="blob-num js-line-number" data-line-number="798"></td>
        <td id="LC798" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$offset</span> <span class="pl-k">=</span> <span class="pl-smi">$i</span> <span class="pl-k">-</span> <span class="pl-c1">mb_strlen</span>(<span class="pl-smi">$subject</span>, <span class="pl-smi">$encoding</span>);</span></td>
      </tr>
      <tr>
        <td id="L799" class="blob-num js-line-number" data-line-number="799"></td>
        <td id="LC799" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$count</span><span class="pl-k">++</span>;</span></td>
      </tr>
      <tr>
        <td id="L800" class="blob-num js-line-number" data-line-number="800"></td>
        <td id="LC800" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L801" class="blob-num js-line-number" data-line-number="801"></td>
        <td id="LC801" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-smi">$subject</span>;</span></td>
      </tr>
      <tr>
        <td id="L802" class="blob-num js-line-number" data-line-number="802"></td>
        <td id="LC802" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L803" class="blob-num js-line-number" data-line-number="803"></td>
        <td id="LC803" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L804" class="blob-num js-line-number" data-line-number="804"></td>
        <td id="LC804" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L805" class="blob-num js-line-number" data-line-number="805"></td>
        <td id="LC805" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * performs a search in a nested array</span></span></td>
      </tr>
      <tr>
        <td id="L806" class="blob-num js-line-number" data-line-number="806"></td>
        <td id="LC806" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> array $haystack the array to be searched</span></span></td>
      </tr>
      <tr>
        <td id="L807" class="blob-num js-line-number" data-line-number="807"></td>
        <td id="LC807" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $needle the search string</span></span></td>
      </tr>
      <tr>
        <td id="L808" class="blob-num js-line-number" data-line-number="808"></td>
        <td id="LC808" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $index optional, only search this key name</span></span></td>
      </tr>
      <tr>
        <td id="L809" class="blob-num js-line-number" data-line-number="809"></td>
        <td id="LC809" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> mixed the key of the matching field, otherwise false</span></span></td>
      </tr>
      <tr>
        <td id="L810" class="blob-num js-line-number" data-line-number="810"></td>
        <td id="LC810" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L811" class="blob-num js-line-number" data-line-number="811"></td>
        <td id="LC811" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * performs a search in a nested array</span></span></td>
      </tr>
      <tr>
        <td id="L812" class="blob-num js-line-number" data-line-number="812"></td>
        <td id="LC812" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L813" class="blob-num js-line-number" data-line-number="813"></td>
        <td id="LC813" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * taken from http://www.php.net/manual/en/function.array-search.php#97645</span></span></td>
      </tr>
      <tr>
        <td id="L814" class="blob-num js-line-number" data-line-number="814"></td>
        <td id="LC814" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L815" class="blob-num js-line-number" data-line-number="815"></td>
        <td id="LC815" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">recursiveArraySearch</span>(<span class="pl-smi">$haystack</span>, <span class="pl-smi">$needle</span>, <span class="pl-smi">$index</span> <span class="pl-k">=</span> <span class="pl-c1">null</span>) {</span></td>
      </tr>
      <tr>
        <td id="L816" class="blob-num js-line-number" data-line-number="816"></td>
        <td id="LC816" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$aIt</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">RecursiveArrayIterator</span>(<span class="pl-smi">$haystack</span>);</span></td>
      </tr>
      <tr>
        <td id="L817" class="blob-num js-line-number" data-line-number="817"></td>
        <td id="LC817" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$it</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">RecursiveIteratorIterator</span>(<span class="pl-smi">$aIt</span>);</span></td>
      </tr>
      <tr>
        <td id="L818" class="blob-num js-line-number" data-line-number="818"></td>
        <td id="LC818" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L819" class="blob-num js-line-number" data-line-number="819"></td>
        <td id="LC819" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">while</span> (<span class="pl-smi">$it</span><span class="pl-k">-&gt;</span>valid()) {</span></td>
      </tr>
      <tr>
        <td id="L820" class="blob-num js-line-number" data-line-number="820"></td>
        <td id="LC820" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (((<span class="pl-c1">isset</span>(<span class="pl-smi">$index</span>) <span class="pl-k">AND</span> (<span class="pl-smi">$it</span><span class="pl-k">-&gt;</span>key() <span class="pl-k">==</span> <span class="pl-smi">$index</span>)) <span class="pl-k">OR</span> (<span class="pl-k">!</span><span class="pl-c1">isset</span>(<span class="pl-smi">$index</span>))) <span class="pl-k">AND</span> (<span class="pl-smi">$it</span><span class="pl-k">-&gt;</span>current() <span class="pl-k">==</span> <span class="pl-smi">$needle</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L821" class="blob-num js-line-number" data-line-number="821"></td>
        <td id="LC821" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">return</span> <span class="pl-smi">$aIt</span><span class="pl-k">-&gt;</span>key();</span></td>
      </tr>
      <tr>
        <td id="L822" class="blob-num js-line-number" data-line-number="822"></td>
        <td id="LC822" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L823" class="blob-num js-line-number" data-line-number="823"></td>
        <td id="LC823" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L824" class="blob-num js-line-number" data-line-number="824"></td>
        <td id="LC824" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$it</span><span class="pl-k">-&gt;</span>next();</span></td>
      </tr>
      <tr>
        <td id="L825" class="blob-num js-line-number" data-line-number="825"></td>
        <td id="LC825" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L826" class="blob-num js-line-number" data-line-number="826"></td>
        <td id="LC826" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L827" class="blob-num js-line-number" data-line-number="827"></td>
        <td id="LC827" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">false</span>;</span></td>
      </tr>
      <tr>
        <td id="L828" class="blob-num js-line-number" data-line-number="828"></td>
        <td id="LC828" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L829" class="blob-num js-line-number" data-line-number="829"></td>
        <td id="LC829" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L830" class="blob-num js-line-number" data-line-number="830"></td>
        <td id="LC830" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L831" class="blob-num js-line-number" data-line-number="831"></td>
        <td id="LC831" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Shortens str to maxlen by replacing characters in the middle with &#39;...&#39;, eg.</span></span></td>
      </tr>
      <tr>
        <td id="L832" class="blob-num js-line-number" data-line-number="832"></td>
        <td id="LC832" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * ellipsis(&#39;a very long string with lots of useless info to make a better example&#39;, 14) becomes &#39;a very ...example&#39;</span></span></td>
      </tr>
      <tr>
        <td id="L833" class="blob-num js-line-number" data-line-number="833"></td>
        <td id="LC833" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L834" class="blob-num js-line-number" data-line-number="834"></td>
        <td id="LC834" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $str the string</span></span></td>
      </tr>
      <tr>
        <td id="L835" class="blob-num js-line-number" data-line-number="835"></td>
        <td id="LC835" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $maxlen the maximum length of the result</span></span></td>
      </tr>
      <tr>
        <td id="L836" class="blob-num js-line-number" data-line-number="836"></td>
        <td id="LC836" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> string with at most maxlen characters</span></span></td>
      </tr>
      <tr>
        <td id="L837" class="blob-num js-line-number" data-line-number="837"></td>
        <td id="LC837" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L838" class="blob-num js-line-number" data-line-number="838"></td>
        <td id="LC838" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">ellipsis</span>(<span class="pl-smi">$str</span>, <span class="pl-smi">$maxlen</span>) {</span></td>
      </tr>
      <tr>
        <td id="L839" class="blob-num js-line-number" data-line-number="839"></td>
        <td id="LC839" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">strlen</span>(<span class="pl-smi">$str</span>) <span class="pl-k">&gt;</span> <span class="pl-smi">$maxlen</span>) {</span></td>
      </tr>
      <tr>
        <td id="L840" class="blob-num js-line-number" data-line-number="840"></td>
        <td id="LC840" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$characters</span> <span class="pl-k">=</span> <span class="pl-c1">floor</span>(<span class="pl-smi">$maxlen</span> <span class="pl-k">/</span> <span class="pl-c1">2</span>);</span></td>
      </tr>
      <tr>
        <td id="L841" class="blob-num js-line-number" data-line-number="841"></td>
        <td id="LC841" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">substr</span>(<span class="pl-smi">$str</span>, <span class="pl-c1">0</span>, <span class="pl-smi">$characters</span>) <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span>...<span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-c1">substr</span>(<span class="pl-smi">$str</span>, <span class="pl-k">-</span><span class="pl-c1">1</span> <span class="pl-k">*</span> <span class="pl-smi">$characters</span>);</span></td>
      </tr>
      <tr>
        <td id="L842" class="blob-num js-line-number" data-line-number="842"></td>
        <td id="LC842" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L843" class="blob-num js-line-number" data-line-number="843"></td>
        <td id="LC843" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-smi">$str</span>;</span></td>
      </tr>
      <tr>
        <td id="L844" class="blob-num js-line-number" data-line-number="844"></td>
        <td id="LC844" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L845" class="blob-num js-line-number" data-line-number="845"></td>
        <td id="LC845" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L846" class="blob-num js-line-number" data-line-number="846"></td>
        <td id="LC846" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L847" class="blob-num js-line-number" data-line-number="847"></td>
        <td id="LC847" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * calculates the maximum upload size respecting system settings, free space and user quota</span></span></td>
      </tr>
      <tr>
        <td id="L848" class="blob-num js-line-number" data-line-number="848"></td>
        <td id="LC848" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L849" class="blob-num js-line-number" data-line-number="849"></td>
        <td id="LC849" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $dir the current folder where the user currently operates</span></span></td>
      </tr>
      <tr>
        <td id="L850" class="blob-num js-line-number" data-line-number="850"></td>
        <td id="LC850" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> int $freeSpace the number of bytes free on the storage holding $dir, if not set this will be received from the storage directly</span></span></td>
      </tr>
      <tr>
        <td id="L851" class="blob-num js-line-number" data-line-number="851"></td>
        <td id="LC851" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> int number of bytes representing</span></span></td>
      </tr>
      <tr>
        <td id="L852" class="blob-num js-line-number" data-line-number="852"></td>
        <td id="LC852" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L853" class="blob-num js-line-number" data-line-number="853"></td>
        <td id="LC853" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">maxUploadFilesize</span>(<span class="pl-smi">$dir</span>, <span class="pl-smi">$freeSpace</span> <span class="pl-k">=</span> <span class="pl-c1">null</span>) {</span></td>
      </tr>
      <tr>
        <td id="L854" class="blob-num js-line-number" data-line-number="854"></td>
        <td id="LC854" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">is_null</span>(<span class="pl-smi">$freeSpace</span>) <span class="pl-k">||</span> <span class="pl-smi">$freeSpace</span> <span class="pl-k">&lt;</span> <span class="pl-c1">0</span>){</span></td>
      </tr>
      <tr>
        <td id="L855" class="blob-num js-line-number" data-line-number="855"></td>
        <td id="LC855" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$freeSpace</span> <span class="pl-k">=</span> <span class="pl-k">self</span><span class="pl-k">::</span>freeSpace(<span class="pl-smi">$dir</span>);</span></td>
      </tr>
      <tr>
        <td id="L856" class="blob-num js-line-number" data-line-number="856"></td>
        <td id="LC856" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L857" class="blob-num js-line-number" data-line-number="857"></td>
        <td id="LC857" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">min</span>(<span class="pl-smi">$freeSpace</span>, <span class="pl-k">self</span><span class="pl-k">::</span>uploadLimit());</span></td>
      </tr>
      <tr>
        <td id="L858" class="blob-num js-line-number" data-line-number="858"></td>
        <td id="LC858" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L859" class="blob-num js-line-number" data-line-number="859"></td>
        <td id="LC859" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L860" class="blob-num js-line-number" data-line-number="860"></td>
        <td id="LC860" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L861" class="blob-num js-line-number" data-line-number="861"></td>
        <td id="LC861" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Calculate free space left within user quota</span></span></td>
      </tr>
      <tr>
        <td id="L862" class="blob-num js-line-number" data-line-number="862"></td>
        <td id="LC862" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L863" class="blob-num js-line-number" data-line-number="863"></td>
        <td id="LC863" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $dir the current folder where the user currently operates</span></span></td>
      </tr>
      <tr>
        <td id="L864" class="blob-num js-line-number" data-line-number="864"></td>
        <td id="LC864" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> int number of bytes representing</span></span></td>
      </tr>
      <tr>
        <td id="L865" class="blob-num js-line-number" data-line-number="865"></td>
        <td id="LC865" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L866" class="blob-num js-line-number" data-line-number="866"></td>
        <td id="LC866" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">freeSpace</span>(<span class="pl-smi">$dir</span>) {</span></td>
      </tr>
      <tr>
        <td id="L867" class="blob-num js-line-number" data-line-number="867"></td>
        <td id="LC867" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$freeSpace</span> <span class="pl-k">=</span> <span class="pl-c1">\OC\Files\</span><span class="pl-c1">Filesystem</span><span class="pl-k">::</span>free_space(<span class="pl-smi">$dir</span>);</span></td>
      </tr>
      <tr>
        <td id="L868" class="blob-num js-line-number" data-line-number="868"></td>
        <td id="LC868" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$freeSpace</span> <span class="pl-k">!</span><span class="pl-k">==</span> <span class="pl-c1">\OCP\Files\</span><span class="pl-c1">FileInfo</span><span class="pl-k">::</span><span class="pl-c1">SPACE_UNKNOWN</span>) {</span></td>
      </tr>
      <tr>
        <td id="L869" class="blob-num js-line-number" data-line-number="869"></td>
        <td id="LC869" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$freeSpace</span> <span class="pl-k">=</span> <span class="pl-c1">max</span>(<span class="pl-smi">$freeSpace</span>, <span class="pl-c1">0</span>);</span></td>
      </tr>
      <tr>
        <td id="L870" class="blob-num js-line-number" data-line-number="870"></td>
        <td id="LC870" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-smi">$freeSpace</span>;</span></td>
      </tr>
      <tr>
        <td id="L871" class="blob-num js-line-number" data-line-number="871"></td>
        <td id="LC871" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L872" class="blob-num js-line-number" data-line-number="872"></td>
        <td id="LC872" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">INF</span>;</span></td>
      </tr>
      <tr>
        <td id="L873" class="blob-num js-line-number" data-line-number="873"></td>
        <td id="LC873" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L874" class="blob-num js-line-number" data-line-number="874"></td>
        <td id="LC874" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L875" class="blob-num js-line-number" data-line-number="875"></td>
        <td id="LC875" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L876" class="blob-num js-line-number" data-line-number="876"></td>
        <td id="LC876" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L877" class="blob-num js-line-number" data-line-number="877"></td>
        <td id="LC877" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Calculate PHP upload limit</span></span></td>
      </tr>
      <tr>
        <td id="L878" class="blob-num js-line-number" data-line-number="878"></td>
        <td id="LC878" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L879" class="blob-num js-line-number" data-line-number="879"></td>
        <td id="LC879" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> PHP upload file size limit</span></span></td>
      </tr>
      <tr>
        <td id="L880" class="blob-num js-line-number" data-line-number="880"></td>
        <td id="LC880" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L881" class="blob-num js-line-number" data-line-number="881"></td>
        <td id="LC881" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">uploadLimit</span>() {</span></td>
      </tr>
      <tr>
        <td id="L882" class="blob-num js-line-number" data-line-number="882"></td>
        <td id="LC882" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$upload_max_filesize</span> <span class="pl-k">=</span> <span class="pl-c1">OCP\</span><span class="pl-c1">Util</span><span class="pl-k">::</span>computerFileSize(<span class="pl-c1">ini_get</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>upload_max_filesize<span class="pl-pds">&#39;</span></span>));</span></td>
      </tr>
      <tr>
        <td id="L883" class="blob-num js-line-number" data-line-number="883"></td>
        <td id="LC883" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$post_max_size</span> <span class="pl-k">=</span> <span class="pl-c1">OCP\</span><span class="pl-c1">Util</span><span class="pl-k">::</span>computerFileSize(<span class="pl-c1">ini_get</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>post_max_size<span class="pl-pds">&#39;</span></span>));</span></td>
      </tr>
      <tr>
        <td id="L884" class="blob-num js-line-number" data-line-number="884"></td>
        <td id="LC884" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> ((<span class="pl-k">int</span>)<span class="pl-smi">$upload_max_filesize</span> <span class="pl-k">===</span> <span class="pl-c1">0</span> <span class="pl-k">and</span> (<span class="pl-k">int</span>)<span class="pl-smi">$post_max_size</span> <span class="pl-k">===</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L885" class="blob-num js-line-number" data-line-number="885"></td>
        <td id="LC885" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">INF</span>;</span></td>
      </tr>
      <tr>
        <td id="L886" class="blob-num js-line-number" data-line-number="886"></td>
        <td id="LC886" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">elseif</span> ((<span class="pl-k">int</span>)<span class="pl-smi">$upload_max_filesize</span> <span class="pl-k">===</span> <span class="pl-c1">0</span> <span class="pl-k">or</span> (<span class="pl-k">int</span>)<span class="pl-smi">$post_max_size</span> <span class="pl-k">===</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L887" class="blob-num js-line-number" data-line-number="887"></td>
        <td id="LC887" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">max</span>(<span class="pl-smi">$upload_max_filesize</span>, <span class="pl-smi">$post_max_size</span>); <span class="pl-c">//only the non 0 value counts</span></span></td>
      </tr>
      <tr>
        <td id="L888" class="blob-num js-line-number" data-line-number="888"></td>
        <td id="LC888" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L889" class="blob-num js-line-number" data-line-number="889"></td>
        <td id="LC889" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">min</span>(<span class="pl-smi">$upload_max_filesize</span>, <span class="pl-smi">$post_max_size</span>);</span></td>
      </tr>
      <tr>
        <td id="L890" class="blob-num js-line-number" data-line-number="890"></td>
        <td id="LC890" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L891" class="blob-num js-line-number" data-line-number="891"></td>
        <td id="LC891" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L892" class="blob-num js-line-number" data-line-number="892"></td>
        <td id="LC892" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L893" class="blob-num js-line-number" data-line-number="893"></td>
        <td id="LC893" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L894" class="blob-num js-line-number" data-line-number="894"></td>
        <td id="LC894" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Checks if a function is available</span></span></td>
      </tr>
      <tr>
        <td id="L895" class="blob-num js-line-number" data-line-number="895"></td>
        <td id="LC895" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L896" class="blob-num js-line-number" data-line-number="896"></td>
        <td id="LC896" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $function_name</span></span></td>
      </tr>
      <tr>
        <td id="L897" class="blob-num js-line-number" data-line-number="897"></td>
        <td id="LC897" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> bool</span></span></td>
      </tr>
      <tr>
        <td id="L898" class="blob-num js-line-number" data-line-number="898"></td>
        <td id="LC898" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L899" class="blob-num js-line-number" data-line-number="899"></td>
        <td id="LC899" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">is_function_enabled</span>(<span class="pl-smi">$function_name</span>) {</span></td>
      </tr>
      <tr>
        <td id="L900" class="blob-num js-line-number" data-line-number="900"></td>
        <td id="LC900" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-c1">function_exists</span>(<span class="pl-smi">$function_name</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L901" class="blob-num js-line-number" data-line-number="901"></td>
        <td id="LC901" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">false</span>;</span></td>
      </tr>
      <tr>
        <td id="L902" class="blob-num js-line-number" data-line-number="902"></td>
        <td id="LC902" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L903" class="blob-num js-line-number" data-line-number="903"></td>
        <td id="LC903" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$disabled</span> <span class="pl-k">=</span> <span class="pl-c1">explode</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>,<span class="pl-pds">&#39;</span></span>, <span class="pl-c1">ini_get</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>disable_functions<span class="pl-pds">&#39;</span></span>));</span></td>
      </tr>
      <tr>
        <td id="L904" class="blob-num js-line-number" data-line-number="904"></td>
        <td id="LC904" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$disabled</span> <span class="pl-k">=</span> <span class="pl-c1">array_map</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>trim<span class="pl-pds">&#39;</span></span>, <span class="pl-smi">$disabled</span>);</span></td>
      </tr>
      <tr>
        <td id="L905" class="blob-num js-line-number" data-line-number="905"></td>
        <td id="LC905" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">in_array</span>(<span class="pl-smi">$function_name</span>, <span class="pl-smi">$disabled</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L906" class="blob-num js-line-number" data-line-number="906"></td>
        <td id="LC906" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">false</span>;</span></td>
      </tr>
      <tr>
        <td id="L907" class="blob-num js-line-number" data-line-number="907"></td>
        <td id="LC907" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L908" class="blob-num js-line-number" data-line-number="908"></td>
        <td id="LC908" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$disabled</span> <span class="pl-k">=</span> <span class="pl-c1">explode</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>,<span class="pl-pds">&#39;</span></span>, <span class="pl-c1">ini_get</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>suhosin.executor.func.blacklist<span class="pl-pds">&#39;</span></span>));</span></td>
      </tr>
      <tr>
        <td id="L909" class="blob-num js-line-number" data-line-number="909"></td>
        <td id="LC909" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$disabled</span> <span class="pl-k">=</span> <span class="pl-c1">array_map</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>trim<span class="pl-pds">&#39;</span></span>, <span class="pl-smi">$disabled</span>);</span></td>
      </tr>
      <tr>
        <td id="L910" class="blob-num js-line-number" data-line-number="910"></td>
        <td id="LC910" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-c1">in_array</span>(<span class="pl-smi">$function_name</span>, <span class="pl-smi">$disabled</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L911" class="blob-num js-line-number" data-line-number="911"></td>
        <td id="LC911" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-c1">false</span>;</span></td>
      </tr>
      <tr>
        <td id="L912" class="blob-num js-line-number" data-line-number="912"></td>
        <td id="LC912" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L913" class="blob-num js-line-number" data-line-number="913"></td>
        <td id="LC913" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">true</span>;</span></td>
      </tr>
      <tr>
        <td id="L914" class="blob-num js-line-number" data-line-number="914"></td>
        <td id="LC914" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L915" class="blob-num js-line-number" data-line-number="915"></td>
        <td id="LC915" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L916" class="blob-num js-line-number" data-line-number="916"></td>
        <td id="LC916" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L917" class="blob-num js-line-number" data-line-number="917"></td>
        <td id="LC917" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Try to find a program</span></span></td>
      </tr>
      <tr>
        <td id="L918" class="blob-num js-line-number" data-line-number="918"></td>
        <td id="LC918" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Note: currently windows is not supported</span></span></td>
      </tr>
      <tr>
        <td id="L919" class="blob-num js-line-number" data-line-number="919"></td>
        <td id="LC919" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L920" class="blob-num js-line-number" data-line-number="920"></td>
        <td id="LC920" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $program</span></span></td>
      </tr>
      <tr>
        <td id="L921" class="blob-num js-line-number" data-line-number="921"></td>
        <td id="LC921" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> null|string</span></span></td>
      </tr>
      <tr>
        <td id="L922" class="blob-num js-line-number" data-line-number="922"></td>
        <td id="LC922" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L923" class="blob-num js-line-number" data-line-number="923"></td>
        <td id="LC923" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">findBinaryPath</span>(<span class="pl-smi">$program</span>) {</span></td>
      </tr>
      <tr>
        <td id="L924" class="blob-num js-line-number" data-line-number="924"></td>
        <td id="LC924" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$memcache</span> <span class="pl-k">=</span> <span class="pl-c1">\</span><span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getMemCacheFactory()<span class="pl-k">-&gt;</span>create(<span class="pl-s"><span class="pl-pds">&#39;</span>findBinaryPath<span class="pl-pds">&#39;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L925" class="blob-num js-line-number" data-line-number="925"></td>
        <td id="LC925" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$memcache</span><span class="pl-k">-&gt;</span>hasKey(<span class="pl-smi">$program</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L926" class="blob-num js-line-number" data-line-number="926"></td>
        <td id="LC926" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">return</span> <span class="pl-smi">$memcache</span><span class="pl-k">-&gt;</span>get(<span class="pl-smi">$program</span>);</span></td>
      </tr>
      <tr>
        <td id="L927" class="blob-num js-line-number" data-line-number="927"></td>
        <td id="LC927" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L928" class="blob-num js-line-number" data-line-number="928"></td>
        <td id="LC928" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$result</span> <span class="pl-k">=</span> <span class="pl-c1">null</span>;</span></td>
      </tr>
      <tr>
        <td id="L929" class="blob-num js-line-number" data-line-number="929"></td>
        <td id="LC929" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-c1">\</span><span class="pl-c1">OC_Util</span><span class="pl-k">::</span>runningOnWindows() <span class="pl-k">&amp;&amp;</span> <span class="pl-k">self</span><span class="pl-k">::</span>is_function_enabled(<span class="pl-s"><span class="pl-pds">&#39;</span>exec<span class="pl-pds">&#39;</span></span>)) {</span></td>
      </tr>
      <tr>
        <td id="L930" class="blob-num js-line-number" data-line-number="930"></td>
        <td id="LC930" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$exeSniffer</span> <span class="pl-k">=</span> <span class="pl-k">new</span> <span class="pl-c1">ExecutableFinder</span>();</span></td>
      </tr>
      <tr>
        <td id="L931" class="blob-num js-line-number" data-line-number="931"></td>
        <td id="LC931" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-c">// Returns null if nothing is found</span></span></td>
      </tr>
      <tr>
        <td id="L932" class="blob-num js-line-number" data-line-number="932"></td>
        <td id="LC932" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$result</span> <span class="pl-k">=</span> <span class="pl-smi">$exeSniffer</span><span class="pl-k">-&gt;</span>find(<span class="pl-smi">$program</span>);</span></td>
      </tr>
      <tr>
        <td id="L933" class="blob-num js-line-number" data-line-number="933"></td>
        <td id="LC933" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-c1">empty</span>(<span class="pl-smi">$result</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L934" class="blob-num js-line-number" data-line-number="934"></td>
        <td id="LC934" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$paths</span> <span class="pl-k">=</span> <span class="pl-c1">getenv</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>PATH<span class="pl-pds">&#39;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L935" class="blob-num js-line-number" data-line-number="935"></td>
        <td id="LC935" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">if</span> (<span class="pl-c1">empty</span>(<span class="pl-smi">$paths</span>)) {</span></td>
      </tr>
      <tr>
        <td id="L936" class="blob-num js-line-number" data-line-number="936"></td>
        <td id="LC936" class="blob-code js-file-line"><span class="pl-s1">					<span class="pl-smi">$paths</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span>/usr/local/bin /usr/bin /opt/bin /bin<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L937" class="blob-num js-line-number" data-line-number="937"></td>
        <td id="LC937" class="blob-code js-file-line"><span class="pl-s1">				} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L938" class="blob-num js-line-number" data-line-number="938"></td>
        <td id="LC938" class="blob-code js-file-line"><span class="pl-s1">					<span class="pl-smi">$paths</span> <span class="pl-k">=</span> <span class="pl-c1">str_replace</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>:<span class="pl-pds">&#39;</span></span>,<span class="pl-s"><span class="pl-pds">&#39;</span> <span class="pl-pds">&#39;</span></span>,<span class="pl-c1">getenv</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>PATH<span class="pl-pds">&#39;</span></span>));</span></td>
      </tr>
      <tr>
        <td id="L939" class="blob-num js-line-number" data-line-number="939"></td>
        <td id="LC939" class="blob-code js-file-line"><span class="pl-s1">				}</span></td>
      </tr>
      <tr>
        <td id="L940" class="blob-num js-line-number" data-line-number="940"></td>
        <td id="LC940" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$command</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span>find <span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-smi">$paths</span> <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span> -name <span class="pl-pds">&#39;</span></span> <span class="pl-k">.</span> <span class="pl-c1">escapeshellarg</span>(<span class="pl-smi">$program</span>) <span class="pl-k">.</span> <span class="pl-s"><span class="pl-pds">&#39;</span> 2&gt; /dev/null<span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L941" class="blob-num js-line-number" data-line-number="941"></td>
        <td id="LC941" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c1">exec</span>(<span class="pl-smi">$command</span>, <span class="pl-smi">$output</span>, <span class="pl-smi">$returnCode</span>);</span></td>
      </tr>
      <tr>
        <td id="L942" class="blob-num js-line-number" data-line-number="942"></td>
        <td id="LC942" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">if</span> (<span class="pl-c1">count</span>(<span class="pl-smi">$output</span>) <span class="pl-k">&gt;</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L943" class="blob-num js-line-number" data-line-number="943"></td>
        <td id="LC943" class="blob-code js-file-line"><span class="pl-s1">					<span class="pl-smi">$result</span> <span class="pl-k">=</span> <span class="pl-c1">escapeshellcmd</span>(<span class="pl-smi">$output</span>[<span class="pl-c1">0</span>]);</span></td>
      </tr>
      <tr>
        <td id="L944" class="blob-num js-line-number" data-line-number="944"></td>
        <td id="LC944" class="blob-code js-file-line"><span class="pl-s1">				}</span></td>
      </tr>
      <tr>
        <td id="L945" class="blob-num js-line-number" data-line-number="945"></td>
        <td id="LC945" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L946" class="blob-num js-line-number" data-line-number="946"></td>
        <td id="LC946" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L947" class="blob-num js-line-number" data-line-number="947"></td>
        <td id="LC947" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$memcache</span><span class="pl-k">-&gt;</span>set(<span class="pl-smi">$program</span>, <span class="pl-smi">$result</span>, <span class="pl-c1">3600</span>);</span></td>
      </tr>
      <tr>
        <td id="L948" class="blob-num js-line-number" data-line-number="948"></td>
        <td id="LC948" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-smi">$result</span>;</span></td>
      </tr>
      <tr>
        <td id="L949" class="blob-num js-line-number" data-line-number="949"></td>
        <td id="LC949" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L950" class="blob-num js-line-number" data-line-number="950"></td>
        <td id="LC950" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L951" class="blob-num js-line-number" data-line-number="951"></td>
        <td id="LC951" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L952" class="blob-num js-line-number" data-line-number="952"></td>
        <td id="LC952" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Calculate the disc space for the given path</span></span></td>
      </tr>
      <tr>
        <td id="L953" class="blob-num js-line-number" data-line-number="953"></td>
        <td id="LC953" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L954" class="blob-num js-line-number" data-line-number="954"></td>
        <td id="LC954" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> string $path</span></span></td>
      </tr>
      <tr>
        <td id="L955" class="blob-num js-line-number" data-line-number="955"></td>
        <td id="LC955" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@param</span> \OCP\Files\FileInfo $rootInfo (optional)</span></span></td>
      </tr>
      <tr>
        <td id="L956" class="blob-num js-line-number" data-line-number="956"></td>
        <td id="LC956" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> array</span></span></td>
      </tr>
      <tr>
        <td id="L957" class="blob-num js-line-number" data-line-number="957"></td>
        <td id="LC957" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L958" class="blob-num js-line-number" data-line-number="958"></td>
        <td id="LC958" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">getStorageInfo</span>(<span class="pl-smi">$path</span>, <span class="pl-smi">$rootInfo</span> <span class="pl-k">=</span> <span class="pl-c1">null</span>) {</span></td>
      </tr>
      <tr>
        <td id="L959" class="blob-num js-line-number" data-line-number="959"></td>
        <td id="LC959" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// return storage info without adding mount points</span></span></td>
      </tr>
      <tr>
        <td id="L960" class="blob-num js-line-number" data-line-number="960"></td>
        <td id="LC960" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$includeExtStorage</span> <span class="pl-k">=</span> <span class="pl-c1">\</span><span class="pl-c1">OC_Config</span><span class="pl-k">::</span>getValue(<span class="pl-s"><span class="pl-pds">&#39;</span>quota_include_external_storage<span class="pl-pds">&#39;</span></span>, <span class="pl-c1">false</span>);</span></td>
      </tr>
      <tr>
        <td id="L961" class="blob-num js-line-number" data-line-number="961"></td>
        <td id="LC961" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L962" class="blob-num js-line-number" data-line-number="962"></td>
        <td id="LC962" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">$rootInfo</span>) {</span></td>
      </tr>
      <tr>
        <td id="L963" class="blob-num js-line-number" data-line-number="963"></td>
        <td id="LC963" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$rootInfo</span> <span class="pl-k">=</span> <span class="pl-c1">\OC\Files\</span><span class="pl-c1">Filesystem</span><span class="pl-k">::</span>getFileInfo(<span class="pl-smi">$path</span>, <span class="pl-c1">false</span>);</span></td>
      </tr>
      <tr>
        <td id="L964" class="blob-num js-line-number" data-line-number="964"></td>
        <td id="LC964" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L965" class="blob-num js-line-number" data-line-number="965"></td>
        <td id="LC965" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-k">!</span><span class="pl-smi">$rootInfo</span> <span class="pl-k">instanceof</span> <span class="pl-c1">\OCP\Files\</span><span class="pl-c1">FileInfo</span>) {</span></td>
      </tr>
      <tr>
        <td id="L966" class="blob-num js-line-number" data-line-number="966"></td>
        <td id="LC966" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">throw</span> <span class="pl-k">new</span> <span class="pl-c1">\OCP\Files\</span><span class="pl-c1">NotFoundException</span>();</span></td>
      </tr>
      <tr>
        <td id="L967" class="blob-num js-line-number" data-line-number="967"></td>
        <td id="LC967" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L968" class="blob-num js-line-number" data-line-number="968"></td>
        <td id="LC968" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$used</span> <span class="pl-k">=</span> <span class="pl-smi">$rootInfo</span><span class="pl-k">-&gt;</span>getSize();</span></td>
      </tr>
      <tr>
        <td id="L969" class="blob-num js-line-number" data-line-number="969"></td>
        <td id="LC969" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$used</span> <span class="pl-k">&lt;</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L970" class="blob-num js-line-number" data-line-number="970"></td>
        <td id="LC970" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$used</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;</span></td>
      </tr>
      <tr>
        <td id="L971" class="blob-num js-line-number" data-line-number="971"></td>
        <td id="LC971" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L972" class="blob-num js-line-number" data-line-number="972"></td>
        <td id="LC972" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$quota</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;</span></td>
      </tr>
      <tr>
        <td id="L973" class="blob-num js-line-number" data-line-number="973"></td>
        <td id="LC973" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$storage</span> <span class="pl-k">=</span> <span class="pl-smi">$rootInfo</span><span class="pl-k">-&gt;</span>getStorage();</span></td>
      </tr>
      <tr>
        <td id="L974" class="blob-num js-line-number" data-line-number="974"></td>
        <td id="LC974" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$includeExtStorage</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">$storage</span><span class="pl-k">-&gt;</span>instanceOfStorage(<span class="pl-s"><span class="pl-pds">&#39;</span>\OC\Files\Storage\Shared<span class="pl-pds">&#39;</span></span>)) {</span></td>
      </tr>
      <tr>
        <td id="L975" class="blob-num js-line-number" data-line-number="975"></td>
        <td id="LC975" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$includeExtStorage</span> <span class="pl-k">=</span> <span class="pl-c1">false</span>;</span></td>
      </tr>
      <tr>
        <td id="L976" class="blob-num js-line-number" data-line-number="976"></td>
        <td id="LC976" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L977" class="blob-num js-line-number" data-line-number="977"></td>
        <td id="LC977" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$includeExtStorage</span>) {</span></td>
      </tr>
      <tr>
        <td id="L978" class="blob-num js-line-number" data-line-number="978"></td>
        <td id="LC978" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$quota</span> <span class="pl-k">=</span> <span class="pl-c1">OC_Util</span><span class="pl-k">::</span>getUserQuota(<span class="pl-c1">\OCP\</span><span class="pl-c1">User</span><span class="pl-k">::</span>getUser());</span></td>
      </tr>
      <tr>
        <td id="L979" class="blob-num js-line-number" data-line-number="979"></td>
        <td id="LC979" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-smi">$quota</span> <span class="pl-k">!</span><span class="pl-k">==</span> <span class="pl-c1">\OCP\Files\</span><span class="pl-c1">FileInfo</span><span class="pl-k">::</span><span class="pl-c1">SPACE_UNLIMITED</span>) {</span></td>
      </tr>
      <tr>
        <td id="L980" class="blob-num js-line-number" data-line-number="980"></td>
        <td id="LC980" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-c">// always get free space / total space from root + mount points</span></span></td>
      </tr>
      <tr>
        <td id="L981" class="blob-num js-line-number" data-line-number="981"></td>
        <td id="LC981" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$path</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-pds">&#39;</span></span>;</span></td>
      </tr>
      <tr>
        <td id="L982" class="blob-num js-line-number" data-line-number="982"></td>
        <td id="LC982" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-k">return</span> <span class="pl-k">self</span><span class="pl-k">::</span>getGlobalStorageInfo();</span></td>
      </tr>
      <tr>
        <td id="L983" class="blob-num js-line-number" data-line-number="983"></td>
        <td id="LC983" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L984" class="blob-num js-line-number" data-line-number="984"></td>
        <td id="LC984" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L985" class="blob-num js-line-number" data-line-number="985"></td>
        <td id="LC985" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L986" class="blob-num js-line-number" data-line-number="986"></td>
        <td id="LC986" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-c">// TODO: need a better way to get total space from storage</span></span></td>
      </tr>
      <tr>
        <td id="L987" class="blob-num js-line-number" data-line-number="987"></td>
        <td id="LC987" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$storage</span><span class="pl-k">-&gt;</span>instanceOfStorage(<span class="pl-s"><span class="pl-pds">&#39;</span>\OC\Files\Storage\Wrapper\Quota<span class="pl-pds">&#39;</span></span>)) {</span></td>
      </tr>
      <tr>
        <td id="L988" class="blob-num js-line-number" data-line-number="988"></td>
        <td id="LC988" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$quota</span> <span class="pl-k">=</span> <span class="pl-smi">$storage</span><span class="pl-k">-&gt;</span>getQuota();</span></td>
      </tr>
      <tr>
        <td id="L989" class="blob-num js-line-number" data-line-number="989"></td>
        <td id="LC989" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L990" class="blob-num js-line-number" data-line-number="990"></td>
        <td id="LC990" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$free</span> <span class="pl-k">=</span> <span class="pl-smi">$storage</span><span class="pl-k">-&gt;</span>free_space(<span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-pds">&#39;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L991" class="blob-num js-line-number" data-line-number="991"></td>
        <td id="LC991" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$free</span> <span class="pl-k">&gt;=</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L992" class="blob-num js-line-number" data-line-number="992"></td>
        <td id="LC992" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$total</span> <span class="pl-k">=</span> <span class="pl-smi">$free</span> <span class="pl-k">+</span> <span class="pl-smi">$used</span>;</span></td>
      </tr>
      <tr>
        <td id="L993" class="blob-num js-line-number" data-line-number="993"></td>
        <td id="LC993" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L994" class="blob-num js-line-number" data-line-number="994"></td>
        <td id="LC994" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$total</span> <span class="pl-k">=</span> <span class="pl-smi">$free</span>; <span class="pl-c">//either unknown or unlimited</span></span></td>
      </tr>
      <tr>
        <td id="L995" class="blob-num js-line-number" data-line-number="995"></td>
        <td id="LC995" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L996" class="blob-num js-line-number" data-line-number="996"></td>
        <td id="LC996" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$total</span> <span class="pl-k">&gt;</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L997" class="blob-num js-line-number" data-line-number="997"></td>
        <td id="LC997" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-smi">$quota</span> <span class="pl-k">&gt;</span> <span class="pl-c1">0</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">$total</span> <span class="pl-k">&gt;</span> <span class="pl-smi">$quota</span>) {</span></td>
      </tr>
      <tr>
        <td id="L998" class="blob-num js-line-number" data-line-number="998"></td>
        <td id="LC998" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$total</span> <span class="pl-k">=</span> <span class="pl-smi">$quota</span>;</span></td>
      </tr>
      <tr>
        <td id="L999" class="blob-num js-line-number" data-line-number="999"></td>
        <td id="LC999" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L1000" class="blob-num js-line-number" data-line-number="1000"></td>
        <td id="LC1000" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-c">// prevent division by zero or error codes (negative values)</span></span></td>
      </tr>
      <tr>
        <td id="L1001" class="blob-num js-line-number" data-line-number="1001"></td>
        <td id="LC1001" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$relative</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>((<span class="pl-smi">$used</span> <span class="pl-k">/</span> <span class="pl-smi">$total</span>) <span class="pl-k">*</span> <span class="pl-c1">10000</span>) <span class="pl-k">/</span> <span class="pl-c1">100</span>;</span></td>
      </tr>
      <tr>
        <td id="L1002" class="blob-num js-line-number" data-line-number="1002"></td>
        <td id="LC1002" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L1003" class="blob-num js-line-number" data-line-number="1003"></td>
        <td id="LC1003" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$relative</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;</span></td>
      </tr>
      <tr>
        <td id="L1004" class="blob-num js-line-number" data-line-number="1004"></td>
        <td id="LC1004" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L1005" class="blob-num js-line-number" data-line-number="1005"></td>
        <td id="LC1005" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L1006" class="blob-num js-line-number" data-line-number="1006"></td>
        <td id="LC1006" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">array</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>free<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$free</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>used<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$used</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>total<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$total</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>relative<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$relative</span>);</span></td>
      </tr>
      <tr>
        <td id="L1007" class="blob-num js-line-number" data-line-number="1007"></td>
        <td id="LC1007" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L1008" class="blob-num js-line-number" data-line-number="1008"></td>
        <td id="LC1008" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L1009" class="blob-num js-line-number" data-line-number="1009"></td>
        <td id="LC1009" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L1010" class="blob-num js-line-number" data-line-number="1010"></td>
        <td id="LC1010" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Get storage info including all mount points and quota</span></span></td>
      </tr>
      <tr>
        <td id="L1011" class="blob-num js-line-number" data-line-number="1011"></td>
        <td id="LC1011" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 *</span></span></td>
      </tr>
      <tr>
        <td id="L1012" class="blob-num js-line-number" data-line-number="1012"></td>
        <td id="LC1012" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> array</span></span></td>
      </tr>
      <tr>
        <td id="L1013" class="blob-num js-line-number" data-line-number="1013"></td>
        <td id="LC1013" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L1014" class="blob-num js-line-number" data-line-number="1014"></td>
        <td id="LC1014" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">private</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">getGlobalStorageInfo</span>() {</span></td>
      </tr>
      <tr>
        <td id="L1015" class="blob-num js-line-number" data-line-number="1015"></td>
        <td id="LC1015" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$quota</span> <span class="pl-k">=</span> <span class="pl-c1">OC_Util</span><span class="pl-k">::</span>getUserQuota(<span class="pl-c1">\OCP\</span><span class="pl-c1">User</span><span class="pl-k">::</span>getUser());</span></td>
      </tr>
      <tr>
        <td id="L1016" class="blob-num js-line-number" data-line-number="1016"></td>
        <td id="LC1016" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L1017" class="blob-num js-line-number" data-line-number="1017"></td>
        <td id="LC1017" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$rootInfo</span> <span class="pl-k">=</span> <span class="pl-c1">\OC\Files\</span><span class="pl-c1">Filesystem</span><span class="pl-k">::</span>getFileInfo(<span class="pl-s"><span class="pl-pds">&#39;</span><span class="pl-pds">&#39;</span></span>, <span class="pl-s"><span class="pl-pds">&#39;</span>ext<span class="pl-pds">&#39;</span></span>);</span></td>
      </tr>
      <tr>
        <td id="L1018" class="blob-num js-line-number" data-line-number="1018"></td>
        <td id="LC1018" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$used</span> <span class="pl-k">=</span> <span class="pl-smi">$rootInfo</span>[<span class="pl-s"><span class="pl-pds">&#39;</span>size<span class="pl-pds">&#39;</span></span>];</span></td>
      </tr>
      <tr>
        <td id="L1019" class="blob-num js-line-number" data-line-number="1019"></td>
        <td id="LC1019" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$used</span> <span class="pl-k">&lt;</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L1020" class="blob-num js-line-number" data-line-number="1020"></td>
        <td id="LC1020" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$used</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;</span></td>
      </tr>
      <tr>
        <td id="L1021" class="blob-num js-line-number" data-line-number="1021"></td>
        <td id="LC1021" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L1022" class="blob-num js-line-number" data-line-number="1022"></td>
        <td id="LC1022" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L1023" class="blob-num js-line-number" data-line-number="1023"></td>
        <td id="LC1023" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$total</span> <span class="pl-k">=</span> <span class="pl-smi">$quota</span>;</span></td>
      </tr>
      <tr>
        <td id="L1024" class="blob-num js-line-number" data-line-number="1024"></td>
        <td id="LC1024" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-smi">$free</span> <span class="pl-k">=</span> <span class="pl-smi">$quota</span> <span class="pl-k">-</span> <span class="pl-smi">$used</span>;</span></td>
      </tr>
      <tr>
        <td id="L1025" class="blob-num js-line-number" data-line-number="1025"></td>
        <td id="LC1025" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L1026" class="blob-num js-line-number" data-line-number="1026"></td>
        <td id="LC1026" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">if</span> (<span class="pl-smi">$total</span> <span class="pl-k">&gt;</span> <span class="pl-c1">0</span>) {</span></td>
      </tr>
      <tr>
        <td id="L1027" class="blob-num js-line-number" data-line-number="1027"></td>
        <td id="LC1027" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-k">if</span> (<span class="pl-smi">$quota</span> <span class="pl-k">&gt;</span> <span class="pl-c1">0</span> <span class="pl-k">&amp;&amp;</span> <span class="pl-smi">$total</span> <span class="pl-k">&gt;</span> <span class="pl-smi">$quota</span>) {</span></td>
      </tr>
      <tr>
        <td id="L1028" class="blob-num js-line-number" data-line-number="1028"></td>
        <td id="LC1028" class="blob-code js-file-line"><span class="pl-s1">				<span class="pl-smi">$total</span> <span class="pl-k">=</span> <span class="pl-smi">$quota</span>;</span></td>
      </tr>
      <tr>
        <td id="L1029" class="blob-num js-line-number" data-line-number="1029"></td>
        <td id="LC1029" class="blob-code js-file-line"><span class="pl-s1">			}</span></td>
      </tr>
      <tr>
        <td id="L1030" class="blob-num js-line-number" data-line-number="1030"></td>
        <td id="LC1030" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-c">// prevent division by zero or error codes (negative values)</span></span></td>
      </tr>
      <tr>
        <td id="L1031" class="blob-num js-line-number" data-line-number="1031"></td>
        <td id="LC1031" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$relative</span> <span class="pl-k">=</span> <span class="pl-c1">round</span>((<span class="pl-smi">$used</span> <span class="pl-k">/</span> <span class="pl-smi">$total</span>) <span class="pl-k">*</span> <span class="pl-c1">10000</span>) <span class="pl-k">/</span> <span class="pl-c1">100</span>;</span></td>
      </tr>
      <tr>
        <td id="L1032" class="blob-num js-line-number" data-line-number="1032"></td>
        <td id="LC1032" class="blob-code js-file-line"><span class="pl-s1">		} <span class="pl-k">else</span> {</span></td>
      </tr>
      <tr>
        <td id="L1033" class="blob-num js-line-number" data-line-number="1033"></td>
        <td id="LC1033" class="blob-code js-file-line"><span class="pl-s1">			<span class="pl-smi">$relative</span> <span class="pl-k">=</span> <span class="pl-c1">0</span>;</span></td>
      </tr>
      <tr>
        <td id="L1034" class="blob-num js-line-number" data-line-number="1034"></td>
        <td id="LC1034" class="blob-code js-file-line"><span class="pl-s1">		}</span></td>
      </tr>
      <tr>
        <td id="L1035" class="blob-num js-line-number" data-line-number="1035"></td>
        <td id="LC1035" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L1036" class="blob-num js-line-number" data-line-number="1036"></td>
        <td id="LC1036" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">array</span>(<span class="pl-s"><span class="pl-pds">&#39;</span>free<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$free</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>used<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$used</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>total<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$total</span>, <span class="pl-s"><span class="pl-pds">&#39;</span>relative<span class="pl-pds">&#39;</span></span> <span class="pl-k">=&gt;</span> <span class="pl-smi">$relative</span>);</span></td>
      </tr>
      <tr>
        <td id="L1037" class="blob-num js-line-number" data-line-number="1037"></td>
        <td id="LC1037" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L1038" class="blob-num js-line-number" data-line-number="1038"></td>
        <td id="LC1038" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L1039" class="blob-num js-line-number" data-line-number="1039"></td>
        <td id="LC1039" class="blob-code js-file-line"><span class="pl-s1"></span></td>
      </tr>
      <tr>
        <td id="L1040" class="blob-num js-line-number" data-line-number="1040"></td>
        <td id="LC1040" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-c">/**</span></span></td>
      </tr>
      <tr>
        <td id="L1041" class="blob-num js-line-number" data-line-number="1041"></td>
        <td id="LC1041" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * Returns whether the config file is set manually to read-only</span></span></td>
      </tr>
      <tr>
        <td id="L1042" class="blob-num js-line-number" data-line-number="1042"></td>
        <td id="LC1042" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 * <span class="pl-k">@return</span> bool</span></span></td>
      </tr>
      <tr>
        <td id="L1043" class="blob-num js-line-number" data-line-number="1043"></td>
        <td id="LC1043" class="blob-code js-file-line"><span class="pl-s1"><span class="pl-c">	 */</span></span></td>
      </tr>
      <tr>
        <td id="L1044" class="blob-num js-line-number" data-line-number="1044"></td>
        <td id="LC1044" class="blob-code js-file-line"><span class="pl-s1">	<span class="pl-k">public</span> <span class="pl-k">static</span> <span class="pl-k">function</span> <span class="pl-en">isReadOnlyConfigEnabled</span>() {</span></td>
      </tr>
      <tr>
        <td id="L1045" class="blob-num js-line-number" data-line-number="1045"></td>
        <td id="LC1045" class="blob-code js-file-line"><span class="pl-s1">		<span class="pl-k">return</span> <span class="pl-c1">\</span><span class="pl-c1">OC</span><span class="pl-k">::</span><span class="pl-smi">$server</span><span class="pl-k">-&gt;</span>getConfig()<span class="pl-k">-&gt;</span>getSystemValue(<span class="pl-s"><span class="pl-pds">&#39;</span>config_is_read_only<span class="pl-pds">&#39;</span></span>, <span class="pl-c1">false</span>);</span></td>
      </tr>
      <tr>
        <td id="L1046" class="blob-num js-line-number" data-line-number="1046"></td>
        <td id="LC1046" class="blob-code js-file-line"><span class="pl-s1">	}</span></td>
      </tr>
      <tr>
        <td id="L1047" class="blob-num js-line-number" data-line-number="1047"></td>
        <td id="LC1047" class="blob-code js-file-line"><span class="pl-s1">}</span></td>
      </tr>
</table>

  </div>

</div>

<a href="#jump-to-line" rel="facebox[.linejump]" data-hotkey="l" style="display:none">Jump to Line</a>
<div id="jump-to-line" style="display:none">
  <form accept-charset="UTF-8" action="" class="js-jump-to-line-form" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <input class="linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" autofocus>
    <button type="submit" class="btn">Go</button>
</form></div>

        </div>

      </div><!-- /.repo-container -->
      <div class="modal-backdrop"></div>
    </div><!-- /.container -->
  </div><!-- /.site -->


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer" role="contentinfo">
    <ul class="site-footer-links right">
        <li><a href="https://status.github.com/" data-ga-click="Footer, go to status, text:status">Status</a></li>
      <li><a href="https://developer.github.com" data-ga-click="Footer, go to api, text:api">API</a></li>
      <li><a href="https://training.github.com" data-ga-click="Footer, go to training, text:training">Training</a></li>
      <li><a href="https://shop.github.com" data-ga-click="Footer, go to shop, text:shop">Shop</a></li>
        <li><a href="https://github.com/blog" data-ga-click="Footer, go to blog, text:blog">Blog</a></li>
        <li><a href="https://github.com/about" data-ga-click="Footer, go to about, text:about">About</a></li>

    </ul>

    <a href="https://github.com" aria-label="Homepage">
      <span class="mega-octicon octicon-mark-github" title="GitHub"></span>
</a>
    <ul class="site-footer-links">
      <li>&copy; 2015 <span title="0.05844s from github-fe120-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="https://github.com/site/terms" data-ga-click="Footer, go to terms, text:terms">Terms</a></li>
        <li><a href="https://github.com/site/privacy" data-ga-click="Footer, go to privacy, text:privacy">Privacy</a></li>
        <li><a href="https://github.com/security" data-ga-click="Footer, go to security, text:security">Security</a></li>
        <li><a href="https://github.com/contact" data-ga-click="Footer, go to contact, text:contact">Contact</a></li>
    </ul>
  </div>
</div>


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-suggester-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="fullscreen-contents js-fullscreen-contents" placeholder=""></textarea>
      <div class="suggester-container">
        <div class="suggester fullscreen-suggester js-suggester js-navigation-container"></div>
      </div>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped tooltipped-w" aria-label="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped tooltipped-w"
      aria-label="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    
    

    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-x flash-close js-ajax-error-dismiss" aria-label="Dismiss error"></a>
      Something went wrong with that request. Please try again.
    </div>


      <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/frameworks-2c8ae50712a47d2b83d740cb875d55cdbbb3fdbccf303951cc6b7e63731e0c38.js"></script>
      <script async="async" crossorigin="anonymous" src="https://assets-cdn.github.com/assets/github-a612914f18d72984765c3aa8bf9dc71ff1da096692c5e14976f9910b2ec2fbda.js"></script>
      
      


  </body>
</html>

